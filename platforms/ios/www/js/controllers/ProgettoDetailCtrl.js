angular.module('LiveEngine.ProgettoDetail.Controller', [])

.filter('getUrl', function() {

   return function(url) {
       	return url ? url : './img/imgLoader.gif';
   }

})

.controller('ProgettoDetailCtrl', function(GlobalVariables, SupportServices,
$scope, $rootScope, $state, $ionicModal, $http, LazyLoader, HelperService, $ionicPopup,
LoggerService, FileSystemService, WikiService_principale, WikiService_poi, $ionicLoading, $timeout, $sce,
SocketService, GeoLocationService, LanguageService, AlbumServiceHelper) {

  var self = this;

  $scope.data = {
      testata: null,
      progetto: null,
      layout: null,
      labels: {
        title: LanguageService.getLabel('LISTA_CONTENUTI_AUMENTATI'),
        btnAvvia: LanguageService.getLabel('BTN_START'),
        btnScegliComeProseguire: LanguageService.getLabel('BTN_SCEGLI_COME_PROSEGUIRE'),
        btnHoAlbum: LanguageService.getLabel('BTN_HO_ALBUM_CON_ME'),
        btnNonHoAlbum: LanguageService.getLabel('BTN_NON_HO_ALBUM')
      },
      toDownload: [],
      timer: null,
      momentsInfo: null,
      audioSrc: "",
      audio: null,
      audioState: 0 //nessun file audio caricato
  };


  $scope.$on("$ionicView.beforeEnter", function(event, data) {

      $timeout(function() {
        console.log("before Enter");

        $ionicLoading.hide();
        
        $scope.data.testata = GlobalVariables.application.currentTestata;
        $scope.data.layout = GlobalVariables.application.currrentLayoutTestata;

        $scope.data.progetto = GlobalVariables.application.currentProgetto;
        $scope.data.progetto.numUtenti=0;

        //alert($scope.data.progetto.tipologia);
        setTimeout($scope.getUtenti4Project, 1000); //prima richiesta


        $scope.data.timer = setTimeout($scope.getUtenti4Project, 10000);
        //$scope.showSplash();

        $scope.data.carrelloItems=$rootScope.variable_header_items["moments_foto_carrello_key"];

        console.log($scope.data.carrelloItems);

        //info moments
        if (GlobalVariables.application.currentProgetto.detail.momentsInfo!=null)
        {
          $scope.data.momentsInfo=JSON.parse(GlobalVariables.application.currentProgetto.detail.momentsInfo);
        }

        console.log("moments Info:");
        console.log($scope.data.momentsInfo);

        //musica di fondo
        if ($scope.data.momentsInfo!=null)
        {
          if ($scope.data.momentsInfo.musicType=="LOCAL") $scope.data.audioSrc = GlobalVariables.baseUrl + "/" + decodeURIComponent($scope.data.momentsInfo.musicPath);
          else $scope.data.audioSrc = decodeURIComponent($scope.data.momentsInfo.musicPath);
          console.log($scope.data.audioSrc);

          $scope.caricaAudio();


/*
          $timeout(function() {
            $scope.data.audio.pause();
          },4000);

          $timeout(function() {
            $scope.data.audio.play();
          },6000);
*/


        }


      },100);

      /*
      console.log("+++GEO+++");
      console.log(GeoLocationService);
      console.log(GeoLocationService.getCurrentPosition().coords);
      console.log(GlobalVariables.application.currentProgetto);
*/

      //console.log($rootScope.variable_header_items);



  });

  $scope.trustSrc = function(src) {
      return $sce.trustAsResourceUrl(src);
  }

  $scope.caricaAudio = function() {
    $scope.data.audio = new Audio();
    $scope.data.audio.src = $scope.data.audioSrc;
    $scope.data.audio.load();
    $scope.data.audio.play();
    $scope.data.audioState=1; //audio in play
  }
  

  $scope.pauseAudio = function() {
    console.log("pauseAudio");
    $scope.data.audio.pause();
    //dovrei mettere in audiostate=2, ma se lo metto subito, mi fa vdere per un istante le 2 icone, perciò metto a 0 e dopo 100ms metto a 2
    $timeout(function() {
      $scope.data.audioState=2; //audio in pausa
    },400);
    $scope.data.audioState=0; //icona audio temporenameamete nascosta
  }

  $scope.restartAudio = function() {
    console.log("restartAudio");
    if ($scope.data.audio==null)
    {
      //caso anomalo, tipo che ha chiesto di andare in RA ma non c'è riuscito, e comunque ha eliminato l'audio
      $scope.caricaAudio();
    }
    else
    {
      $scope.data.audio.play();
      //dovrei mettere in audiostate=2, ma se lo metto subito, mi fa vdere per un istante le 2 icone, perciò metto a 0 e dopo 100ms metto a 2
      $timeout(function() {
        $scope.data.audioState=1; //audio in play
      },400);
      $scope.data.audioState=0; //icona audio temporenameamete nascosta
    }
  }

  $scope.getUtenti4Project = function() {
      
      console.log("getUtenti4Project");
      SupportServices.getUsersByPrjId({
          onComplete: function(err, json_prj) {
            
            //console.log("Presi gli utenti per progetto");
            //console.log(JSON.stringify(json_prj));
            if (json_prj==null) return;

            var arrayPrjId=[];
            if(err) {
                                      
            }
            else
            {
              arrayPrjId=json_prj;
            }
            
            for (var j=0;j<arrayPrjId.prjId_list.length;j++)
            {
              if (arrayPrjId.prjId_list[j].name==GlobalVariables.application.currentProgetto.idProgetto)
                {
                  $scope.data.progetto.numUtenti=arrayPrjId.prjId_list[j].totale;
                }
            }
            if ($scope.data.progetto.numUtenti==0) $scope.data.progetto.numUtenti=1; //almeno lui....
            console.log(GlobalVariables.application.currentProgetto);

          }

      });


  }


  $scope.startCarrello = function() {
    console.log("start carrello");
    $scope.data.carrelloItems.callback();
  }

  $scope.goBackToListaProgetti = function() {
      console.log("goBackToListaProgetti");
      clearInterval($scope.data.timer);
      
      if($scope.data.audio) {
        $scope.data.audio.pause();
        $scope.data.audio = null;
        $scope.data.audioState=0; //nessun audio
      }

      //per adesso lo porto direttamente alla home

      SocketService.closeSocket();
      $state.go('home');



      /*
      if (GlobalVariables.application.currentTestata.riconoscimento==0) //è una testata senza riconoscimento copertina, ritorno alla home
      {
        $ionicLoading.show({
            template: LanguageService.getLabel('ATTENDERE')
        });

        SupportServices.getListaCopertine({

            onComplete: function(err, jsonData) {

                $ionicLoading.hide();

                if(err) {

                    console.log("pre-error 6: " + err);
                    HelperService.showUnrecoverableError(err, false);

                } else {

                    GlobalVariables.application.listaProgetti = jsonData.progetti;
                    SocketService.closeSocket();
                    $state.go('home');

                }

            }

        });
      }
      else
      {
        $state.go('navigazione_progetti');
      }
      */

  }


  $scope.avviaSferaFoto = function() {


    $ionicLoading.show({
        template: '<div style="padding:10px 5px;">'+LanguageService.getLabel('AVVIO_REALTA_AUMENTATA')+'</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
        duration: 10000
    });

    SupportServices.getFotosferaData({
        idProgetto: GlobalVariables.application.currentProgetto.idProgetto,
        onComplete: function(err, json) {

            console.log("on complete: " + err + " - " + JSON.stringify(json));
            if(err) {

                HelperService.showUnrecoverableError(err, true);
                $ionicLoading.hide();

            } else {

                $timeout(function() {
                  if (json.errore)
                  {
                    HelperService.showUnrecoverableError(json.errore, true);
                    $ionicLoading.hide();
                    return;                      
                  }

                  if ($scope.data.audioState!=0) //esiste un audio
                  {
                    $scope.data.audio.pause();
                    $scope.data.audio=null;
                    $scope.data.audioState=2; //pausa audio
                  }

                  var localPath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;
                  var localDynamicARPath = FileSystemService.getLocalPath() + GlobalVariables.directory_main + "/dinamic_classes" + GlobalVariables.directory_trail;
                  var idPrj = GlobalVariables.application.currentProgetto.idProgetto;
                  var baseUrl = GlobalVariables.baseUrl;
                  //var parameter = { "path": "www/clientRecognition/index.html", "requiredFeatures": ["image_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "localDynamicARPath": localDynamicARPath, "idPrj": idPrj, "isApiRest": false, "baseUrl": baseUrl, "deviceId": GlobalVariables.deviceUUID, "multiTarget": GlobalVariables.application.currentTestata.multitarget};
                  var parameter = { "path": "www/clientRecognition/indexFotosfera.html", "requiredFeatures": ["geo"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "localDynamicARPath": localDynamicARPath, "idPrj": idPrj, "baseUrl": baseUrl};
                  console.log(JSON.stringify(parameter));
                  clearInterval($scope.data.timer);
                  
                  WikiService_poi.loadARchitectWorld(parameter);

                });

            }

        }

    });
    






  };


  $scope.avviaRealtaAumentataProgetto = function() {

    //realtà aumentata o visualizzazione slideshow?
    
    $scope.data.popupScelta = $ionicPopup.show({
      template: '',
      title: $scope.data.labels.btnScegliComeProseguire,
      //subTitle: 'Please use normal things',
      scope: $scope,
      buttons: [
        { text: $scope.data.labels.btnHoAlbum,
          onTap: function(e) {
            //alert("RA");
            $scope.avviaSiRA();
          }
        },
        {
          text: $scope.data.labels.btnNonHoAlbum,
          type: 'button-positive',
          onTap: function(e) {
            //alert("NON RA");
            $scope.avviaNoRA();
          }
        }
      ]
    });  

  };

  $scope.avviaNoRA = function() {  

    if ($scope.data.popupScelta) $scope.data.popupScelta.close();

    //senza album
    console.log(GlobalVariables.application.currentProgetto);

    SupportServices.getLikes({
        idProgetto: GlobalVariables.application.currentProgetto.idProgetto,
        onComplete: function(err, json) {

            console.log("on complete: " + err + " - " + JSON.stringify(json));
            if(err) {

                HelperService.showUnrecoverableError(err, true);
                $ionicLoading.hide();

            } else {

                $timeout(function() {
                  if (json.errore)
                  {
                    HelperService.showUnrecoverableError(json.errore, true);
                    $ionicLoading.hide();                      
                  }

                  GlobalVariables.application.currentProgetto.moments_likes = json.moments_likes;                  
                  GlobalVariables.application.currentProgetto.moments_comments = json.moments_comments;                  
                  
                  console.log(GlobalVariables.application.currentProgetto);

                  AlbumServiceHelper.showAlbum($scope.data);

                });

            }

        }

    });

  };


  $scope.avviaSiRA = function() {  
   
    if ($scope.data.popupScelta) $scope.data.popupScelta.close();

    //con album
    $ionicLoading.show({
        template: '<div style="padding:10px 5px;">'+LanguageService.getLabel('AVVIO_REALTA_AUMENTATA')+'</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
        duration: 10000
    });

    if ($scope.data.audioState!=0) //esiste un audio
    {
      $scope.data.audio.pause();
      $scope.data.audio=null;
      $scope.data.audioState=2; //pausa audio
    }

    //alert("Sono qui");
    //devo scaricare un certo numero di files, prima di partire
    $scope.data.toDownload = new Array();
    var basePath=GlobalVariables.baseUrl + "/";
    var targets=GlobalVariables.application.currentProgetto.detail.target;
    for (var index=0;index<targets.length;index++)
    {
      //per ogni target vedo gli hotspot
      var punti=targets[index].punti;
      for (var i=0;i<punti.length;i++)
      {
        //ha un hotspot custom vecchio stile?
        var pathIndicatore=punti[i].pathIndicatore;
        if (typeof pathIndicatore != 'undefined')
        {
          console.log("Trovato hotspot da scaricare: " + pathIndicatore);
          $scope.data.toDownload.push(basePath + pathIndicatore);
        }
        //ha un hotspot custom nuovo stile?
        var hsc_tipo=punti[i].hsc_tipo;
        if (typeof hsc_tipo != 'undefined')
        {
          console.log("Trovato hotspot nuovo stile da scaricare: " + punti[i].hsc_path);
          $scope.data.toDownload.push(basePath + punti[i].hsc_path);
        }

      }
    }

    //wtc del progetto
    var path = GlobalVariables.application.currentProgetto.detail.wtcPath;
    if (path==null)
    {
      console.log("Attenzione, i contenuti per questo numero non sono stati pubblicati. Riprovare in seguito, grazie");
      HelperService.showUnrecoverableError("Attenzione, i contenuti per questo numero non sono stati pubblicati.<br>Riprovare in seguito, grazie", true);

    }
    $scope.data.toDownload.push(basePath + path);

    console.log($scope.data.toDownload);

    $scope.downloadFilesAndStart(0); //elemento iniziale

    

  };


  $scope.downloadFilesAndStart = function(index) {

      console.log("sono in downloadFilesAndStart, con index: " + index);

      path = $scope.data.toDownload[index];
      var pathSplit= path.split("/");
      //il nome è l'ultimo elemento
      var name=pathSplit[pathSplit.length-1];
      //se il nome termina con wtc, lo cambio in mac
      var estensione=name.substring(name.length-4);
      console.log("estensione: " + estensione);
      if (estensione==".wtc") name = name.replace(".wtc", ".mac");
      if (estensione==".wt3") name = name.replace(".wt3", ".mao");

      //e la dir il penultimo
      dir=pathSplit[pathSplit.length-2];

      var uri = encodeURI(path);

      //var localpath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;
      var localpath = SupportServices.BUFFER_DIRECTORY;

      //di base vedo se il file è già presente, per non doverlo scaricare
      console.log("Controllo se il file " + name + " esiste già, altrimenti provo a scaricarlo");
      FileSystemService.fileExists({
          directory: SupportServices.BUFFER_DIRECTORY + '/' + dir,
          fileName: name,
          onComplete: function(err, isAvailable) {

              var esiste=false;
              if(err)
              {
                  //errore, ma non faccio niente
                  console.log("Errore in fileExist, continuo e lo scaricherò");
              }
              else
              {
                  if(!isAvailable) {
                      //non esiste
                      console.log("Non Esiste, continuo e lo scaricherò");
                  } else {
                      //esiste, vado avanti
                      console.log("Esiste, vado avanti");
                      esiste=true;
                  }
              }
              if (esiste)
              {
                $scope.downloadNextFile(index);
              }
              else
              {
                //è presente la connessione?
                console.log("Check connessione: " + HelperService.isNetworkAvailable());
                if (HelperService.isNetworkAvailable()==false)
                {
                  console.log("Nessuna connessione. Ho già verificato che questo file non è presente...");
                  HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);
                }
                else
                {
                  console.log("Provo a scaricare " + uri);

                  FileSystemService.downloadFile({
                      url: uri,
                      directory: SupportServices.BUFFER_DIRECTORY + '/' + dir,
                      fileName: name,
                      onComplete: function(err) {

                          if(err) {
                            console.log("download error: " + err);
                            console.log("Ho già verificato che questo file non è presente...");
                            //il file non trovato, su ios, mi genera comunque un file con lo stesso nome ma con un contenuto non coerente (è la risposta del server che dice che il file non esiste)
                            //devo perciò esplicitamente cancellare questo file...

                            FileSystemService.fileDelete({
                                directory: SupportServices.BUFFER_DIRECTORY + '/' + dir,
                                fileName: name,
                                onComplete: function(err) {

                                    if(err) {
                                      console.log("filedelete error: " + err);
                                    } else {
                                      console.log("filedelete completato");
                                    }
                                    HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);
                                }
                            });

                          } else {
                            console.log("download completato");
                            //console.log("index: " + index);
                            //console.log("toDownload: " + toDownload);
                            $scope.downloadNextFile(index);

                          }
                      }
                  });
                }                
              }

          }
      });      





  }

  $scope.downloadNextFile = function(index) {

      index++;
      if (index<$scope.data.toDownload.length)
      {
        //console.log("index: " + index);
        $scope.downloadFilesAndStart(index);
      }
      else
      {

        console.log("Finito");

      




        var localPath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;
        var localDynamicARPath = FileSystemService.getLocalPath() + GlobalVariables.directory_main + "/dinamic_classes" + GlobalVariables.directory_trail;
        var idPrj = GlobalVariables.application.currentProgetto.idProgetto;
        var baseUrl = GlobalVariables.baseUrl;
        //var parameter = { "path": "www/clientRecognition/index.html", "requiredFeatures": ["2d_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "idPrj": idPrj, "isApiRest": false, "baseUrl": baseUrl, "deviceId": GlobalVariables.deviceUUID};
        var parameter = { "path": "www/clientRecognition/index.html", "requiredFeatures": ["image_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "localDynamicARPath": localDynamicARPath, "idPrj": idPrj, "isApiRest": false, "baseUrl": baseUrl, "deviceId": GlobalVariables.deviceUUID, "multiTarget": GlobalVariables.application.currentTestata.multitarget};

        console.log(JSON.stringify(parameter));
        
        clearInterval($scope.data.timer);
        WikiService_principale.loadARchitectWorld(parameter);

      }

  }





  $scope.$on("$ionicView.enter", function(event, data) {

      /*
        tweak per ios. quando esco dalla realtà aumentata di wikitude con avvenuta
        rotazione dello schermo l'ambientre ionic non percepisce il ricalcolo della larghezza
      */
      try {
        if(ionic.Platform.isIOS()) {
          StatusBar.show();
          StatusBar.hide();
        }  
      } catch(e) {}

  });


  $scope.$on("$ionicView.afterEnter", function(event, data) {

      $rootScope.labelReload = $scope.labelReload;

      LoggerService.triggerAction({
          action: LoggerService.ACTION.ENTER,
          state: LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO,
          data: null
      });


  });



  $scope.$on("$ionicView.beforeLeave", function(event, data) {

      LoggerService.triggerAction({
          action: LoggerService.ACTION.LEAVE,
          state: LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO,
          data: null
      });

      if($scope.data.audio) {
        $scope.data.audio.pause();
        $scope.data.audio = null;
        $scope.data.audioState=0; //nessun audio
      }

  });

  
  $scope.labelReload = function() {
      console.log("Ricarico le label - Dettaglio Progetto");

      $scope.data.labels = {
        title: LanguageService.getLabel('LISTA_CONTENUTI_AUMENTATI'),
        btnAvvia: LanguageService.getLabel('BTN_START'),
        btnScegliComeProseguire: LanguageService.getLabel('BTN_SCEGLI_COME_PROSEGUIRE'),
        btnHoAlbum: LanguageService.getLabel('BTN_HO_ALBUM_CON_ME'),
        btnNonHoAlbum: LanguageService.getLabel('BTN_NON_HO_ALBUM')
      };
      
  };

});
