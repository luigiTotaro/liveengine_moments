function ContentManager(contenutiUrl,contenutiPath,coloreGenerale,sender){
	this.arrayImagesSwiper=null; //nel vecchio codice era usato per memorizzare le immagini per il passaggio fullscreen/non full... da rivedere in caso di implementazione del fullscreen
	this.contenutiUrl=contenutiUrl;
	this.contenutiPath=contenutiPath;
	this.coloreGenerale=coloreGenerale; //colore usato per il bordo delle varie finestre dei video, dello slideshow, ecc
	this.sender=sender;
	this.timeoutCaricamento=null;
}


ContentManager.prototype.setVideo = function (oggettoVideo)
{
	console.log("sono in setVideo, con oggettoVideo: " + JSON.stringify(oggettoVideo));
	var url=oggettoVideo.path;
	url=decodeURIComponent(url);
	var tipo=oggettoVideo.type;
	if ((tipo=="YT") || (tipo=="VM") || (tipo=="FB"))
	{
		//aspect ratio
		var ar=parseInt(oggettoVideo.larghezza)/parseInt(oggettoVideo.altezza);
		this.creaVideo(url, tipo, ar);
	}
	else if (tipo=="LINK")
	{
		this.creaVideo(url, tipo, 0);
	}
	else
	{
		this.creaVideo(this.contenutiUrl + "/" + url, "LOCAL", 0);
	}
}


ContentManager.prototype.creaVideo = function (urlVideo, tipo, aspectRatio)
{
	nasconditutto();
	var self=this;
	console.log("sono in creaVideo, con url: " + urlVideo + " e tipo: " + tipo);

	$("#backButton").hide();

	
	var dimensioni = this.getViewDimensions();

	//quanto lo devo fare grande il video?
    $("#videoContainer").css("border", "0px").css("position", "fixed").css("right", "0").css("bottom", "0").css("top", "0").css("min-width", "100%").css("min-height", "100%").css("width", dimensioni.larghezza+"px").css("height", "auto").css("z-index", "999");
	var larghezzaDiv=$("#videoContainer").width();

	//videoYT o normale?
	var htmlToAdd="";
	if ((tipo=="YT") || (tipo=="VM") || (tipo=="FB"))
	{
		aspectRationPlayerYT=aspectRatio;
		var larghezzaYT=larghezzaDiv*1;
		var altezzaYT=larghezzaYT/aspectRatio;

		if (tipo=="YT")
		{
			htmlToAdd += "<iframe id='playerYT' type='text/html' width='"+larghezzaYT+"' height='"+altezzaYT+"' style='padding-top:0px;padding-bottom:0px;' ";
			htmlToAdd += "src='http://www.youtube.com/embed/"+urlVideo+"?enablejsapi=1&controls=2&autoplay=1&modestbranding=1&rel=0&showinfo=0&origin=http://livengine.mediasoftonline.com' frameborder='0'></iframe>";
		}
		else if (tipo=="VM")
		{
			htmlToAdd += "<iframe id='playerYT' type='text/html' width='"+larghezzaYT+"' height='"+altezzaYT+"' style='padding-top:0px;padding-bottom:0px;' ";
			htmlToAdd += "src='https://player.vimeo.com/video/"+urlVideo+"' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
		}
		else if (tipo=="FB")
		{
			htmlToAdd += "<iframe id='playerYT' type='text/html' width='"+larghezzaYT+"' height='"+altezzaYT+"' ";
			htmlToAdd += "src='"+urlVideo+"' style='padding-top:0px;padding-bottom:0px;border:none;overflow:hidden' scrolling='no' frameborder='0' allowTransparency='true'></iframe>";
		}

		htmlToAdd += "<image id='btnCloseVideo' src='assets/btn_close.png' onclick='back();' style='height:60px;border: 0px;background: none;box-shadow: none;margin: 0px;padding: 0px;right: -10px;top: -6px;position:absolute;' />";
		//htmlToAdd += "<image src='assets/fullscreen.png' onclick='toggleFullscreen();' style='height:30px;border: 0px;background: none;box-shadow: none;margin: 0px;padding: 0px;right: 0px;bottom: 0px;position:absolute;' />";

		console.log(htmlToAdd);

		$("#videoContainer").html(htmlToAdd);

	    var larghezzaContenitore=$("#videoContainer").width();
	    var altezzaContenitore=larghezzaContenitore/aspectRatio;

		var posizione=(dimensioni.altezza-dimensioni.altezzaHeader-altezzaContenitore-dimensioni.altezzaFooter)/2 + dimensioni.altezzaHeader;

		console.log("altezzaContenitore: " + altezzaContenitore);
		console.log("posizione: " + posizione);

		$("#videoContainer").css("top",posizione+"px");
		$("#videoContainer").show();
		resizeBasedOnResolution(self.sender);
	}
	else
	{
		$("#loader").show();
		htmlToAdd += "<video id='videoInterno' autoplay controls width='"+larghezzaDiv+"' style='padding-top:0px;padding-bottom:0px;'>";
		htmlToAdd += "<source src='"+ urlVideo +"' type='video/mp4' />";
		htmlToAdd += "</video>";
		htmlToAdd += "<image id='btnCloseVideo' src='assets/btn_close.png' onclick='back();' style='height:60px;border: 0px;background: none;box-shadow: none;margin: 0px;padding: 0px;right: -10px;top: -6px;position:absolute;' />";
		//htmlToAdd += "<image src='assets/fullscreen.png' onclick='toggleFullscreen();' style='height:30px;border: 0px;background: none;box-shadow: none;margin: 0px;padding: 0px;right: 0px;bottom: 0px;position:absolute;' />";

		console.log(htmlToAdd);

		$("#videoContainer").html(htmlToAdd);

		//se il video non riesce a caricarlo, si blocca, percè il resto del codice è in loadeddata
		//perciò metto un loader, e un timer, e se in 5 secondi non ha almeno iniziato, do bandiera bianca

		var timeoutCaricamento = setTimeout(function() {
			//alert("sono nel timeout");
			if ($("#loader").is(':visible'))
			{
				//non ha caricato il video... alzo bandiera bianca
				//rimuovo il listener dall'elemento... casomai riuscisse...
				var el = document.getElementById('videoInterno'), elClone = el.cloneNode(true);
				el.parentNode.replaceChild(elClone, el);
				// e metto un messaggio
				$("#messaggioGenerico p").html(getLabel("ERRORE_CARICAMENTO_VIDEO","Errore nel caricamento del video"));
				$("#messaggioGenerico").show();
				$("#loader").hide();
				back();

				setTimeout(function() {
					$("#messaggioGenerico").hide();
				}, 4000);
			}
		}, 15000);

		var video = document.getElementById('videoInterno');
		video.addEventListener('loadeddata', function() {
		    $("#loader").hide();
		    clearTimeout(timeoutCaricamento);
		    console.log("Loaded the video's data!");

		    //calcolo l'altezza del contenitore
		    var altezzaVideo = $("#videoInterno")[0].videoHeight;
		    var larghezzaVideo = $("#videoInterno")[0].videoWidth;
		    var larghezzaContenitore=$("#videoContainer").width();
	    	var rapportoVideo=altezzaVideo/larghezzaVideo;
		    
		    //var altezzaContenitore=$("#videoContainer").height();
		    var altezzaContenitore=altezzaVideo/larghezzaVideo*larghezzaContenitore;

		    //var altezzaContenitore=$("#videoContainer").height();
			var altezzaUtile=(dimensioni.altezza-dimensioni.altezzaHeader)*0.80;
			//override per fuillscreen
			altezzaUtile=dimensioni.altezza;
			var posizione=(dimensioni.altezza-dimensioni.altezzaHeader-altezzaContenitore-dimensioni.altezzaFooter)/2 + dimensioni.altezzaHeader;
			console.log("altezzaContenitore: " + altezzaContenitore);
			console.log("altezza utile: " + altezzaUtile);
			console.log("posizione: " + posizione);
			
			if (altezzaContenitore>altezzaUtile)
			{
				//è un video molto alto.. deve comandare l'altezza, non la larghezza
				var newAltezzaVideo=altezzaUtile;
				var newLarghezzaVideo=newAltezzaVideo/rapportoVideo;
				console.log("newAltezzaVideo: " + newAltezzaVideo);
				console.log("newLarghezzaVideo: " + newLarghezzaVideo);
				$("#videoContainer").css("height",newAltezzaVideo+"px").css("width",newLarghezzaVideo+"px").css("right",((dimensioni.larghezza-newLarghezzaVideo)/2)+"px");
				$("#videoInterno").css("height",(newAltezzaVideo*0.95)+"px").css("width",(newLarghezzaVideo*0.95)+"px");
				posizione=(dimensioni.altezza-dimensioni.altezzaHeader-newAltezzaVideo)/2;
			}

			$("#videoContainer").css("top",posizione+"px");
			$("#videoContainer").show();
			resizeBasedOnResolution(self.sender);

			$("#videoInterno")[0].webkitExitFullScreen();

		}, false);
	}
}


ContentManager.prototype.setSlideshow = function (arrayOggettoImmagine, connectionPresent, idSlideshow)
{
	var self=this;

	console.log("sono in setSlideshow, con connessione: "+connectionPresent+" e arrayOggettoImmagine: " + JSON.stringify(arrayOggettoImmagine) );

	var arrayImg=[];
	var arrayTitoli=[];

	if (connectionPresent)
	{
		//il primo valore di questo array è l'id del punto, che mi serve per memorizzare le immagini in caso di successivo offline
		arrayImg.push(idSlideshow);
		for (i=0;i<arrayOggettoImmagine.length;i++)
		{
			var oggettoImmagine=arrayOggettoImmagine[i];
			var url=decodeURIComponent(oggettoImmagine.path);

			var tipo=oggettoImmagine.type;
			if (tipo=="LINK")
			{
				arrayImg.push(url);
			}
			else
			{
				arrayImg.push(this.contenutiUrl + "/" + url);
			}
		}
	}
	else
	{
		//il primo valore di questo array è l'id del punto, lo metto = "" in moda da far sapere di non dover salvare le immagini... sono senza connessione
		arrayImg.push("");
		for (i=0;i<arrayOggettoImmagine.length;i++)
		{
			arrayImg.push(this.contenutiPath + "slideshow/" + idSlideshow + "_" + i);
		}
	}
	this.arrayImagesSwiper=null;
	this.creaSlideshowNew(arrayImg, true); //di default in fullscreen
}

ContentManager.prototype.creaSlideshowNew = function (arrayImg, fullscreen)
{
	var self=this;

	console.log("sono in creaSlideshowNew, con arrayImg: " + JSON.stringify(arrayImg) );

	// se sta solo ridisegnando lo slideshow, non salvo le immagini
	if (this.arrayImagesSwiper == null)
	{
		//ci sta connessione, perciò, oltre a mostre le immagini, le comunico fuori da wikitude, per scaricarle e trenerle disponibili per l'offline
		//ma solo se ho un id valido in prima posizione in questo array
		if (arrayImg[0]!="")
		{
			//creo un array con gli stessi dati ma con gli url encodati
			var arrayTemp=new Array();
			for (var i=0;i<arrayImg.length;i++)
			{
				if (i==0) arrayTemp.push(arrayImg[i]);
				else arrayTemp.push(encodeURIComponent(arrayImg[i]));
			}
			AR.platform.sendJSONObject({
				topic: "scaricaSlideshow",
				array: arrayTemp
			}); 

			//document.location = 'architectsdk://scaricaSlideshow_' + encodeURIComponent(JSON.stringify(arrayImg));
		}
	}

	this.arrayImagesSwiper=arrayImg; //se dovesse distruggere lo swiper e rifarlo per un resize...

	$("#backButton").hide();

	nasconditutto();

	var dimensioni = this.getViewDimensions();

	var larghezzaSlider;
	var altezzaSlider;
	var top;
	var left;

	if (fullscreen)
	{
		if (dimensioni.larghezza>dimensioni.altezza) //landscape
		{
			larghezzaSlider=dimensioni.larghezza-4;
			altezzaSlider=dimensioni.altezza-4;
			top=0;
			left=0;
		}
		else //portrait
		{
			larghezzaSlider=dimensioni.larghezza-4;
			altezzaSlider=dimensioni.altezza-4;
			top=0;
			left=0;
		}

	}
	else
	{
		if (dimensioni.larghezza>dimensioni.altezza) //landscape
		{
			larghezzaSlider=dimensioni.larghezza*0.8;
			altezzaSlider=(dimensioni.altezza-dimensioni.altezzaHeader)*0.8;
			top=(dimensioni.altezza-altezzaSlider + dimensioni.altezzaHeader)/2;
			left=dimensioni.larghezza*0.1;
		}
		else //portrait
		{
			larghezzaSlider=dimensioni.larghezza*0.8;
			altezzaSlider=(dimensioni.altezza-dimensioni.altezzaHeader)*0.4;
			top=(dimensioni.altezza-altezzaSlider + dimensioni.altezzaHeader)/2;
			left=dimensioni.larghezza*0.1;
		}
	}

	console.log( "larghezzaSlider:" + larghezzaSlider );
	console.log( "altezzaSlider:" + altezzaSlider );

	//fullscreenSwiper=fullscreen;

	$( ".swiper-container" ).css("position","absolute").css("top",top+"px").css("left",left+"px").css("width",larghezzaSlider+"px").css("height",altezzaSlider+"px").css("border","2px solid "+this.coloreGenerale).css("margin","0px");
	//$( ".swiper-container" ).show();
	$("#swiperDiv" ).show();
	$("#closeSwiper").css("top",(top-5)+"px").css("right",(left-11)+"px").css("z-index","9999");
	//$("#fullscreenSwiper").css("top",(top+altezzaSlider-28)+"px").css("right",(left-2)+"px").css("z-index","9999");

	swiper = new Swiper('.swiper-container', {
	    pagination: '.swiper-pagination',
	    paginationClickable: true,
	    spaceBetween: 0,
	    preloadImages: true,
	    updateOnImagesReady: true
	});

	swiper.on('touchStart', function (swiper, event) {
	    console.log('slide touchStart');
	    clearInterval(myTimer);

		/*
		$( ".swiper-wrapper div img" ).each(function( index ) {
          //dettagliato tutto bene nel resizeBasedOnResolution
          var widthOriginal=$( this )[0].naturalWidth;
          var heightOriginal=$( this )[0].naturalHeight;
          var rapporto=altezzaSlider/heightOriginal;
          if ((widthOriginal*rapporto)<=larghezzaSlider)
          {
            $( this ).css("width","initial").css("height","100%");
          }
          else
          {
            $( this ).css("width","100%").css("height","initial");
          }
		});
*/

	});


	for (i=1;i<arrayImg.length;i++) //parto da 1 perchè in prima posizione ci sta l'id del punto
	{
		swiper.appendSlide('<div class="swiper-slide" ><img src="'+arrayImg[i]+'" style="width:100%;"/></div>');
	}

	//swiper.update();

	$( ".swiper-wrapper div img" ).each(function( index ) {
      //dettagliato tutto bene nel resizeBasedOnResolution
      var widthOriginal=$( this )[0].naturalWidth;
      var heightOriginal=$( this )[0].naturalHeight;
      var rapporto=altezzaSlider/heightOriginal;
      if ((widthOriginal*rapporto)<=larghezzaSlider)
      {
        $( this ).css("width","initial").css("height","100%");
      }
      else
      {
        $( this ).css("width","100%").css("height","initial");
      }
	});

	//timer per aggiustare le immagini.. non so quando le ha finite di caricare. Lo stoppo appena inizia a swipare
	var myTimer = setInterval(function() {
			//console.log("timer");
			$( ".swiper-wrapper div img" ).each(function( index ) {
				//dettagliato tutto bene nel resizeBasedOnResolution
				var widthOriginal=$( this )[0].naturalWidth;
				var heightOriginal=$( this )[0].naturalHeight;
				var rapporto=altezzaSlider/heightOriginal;
				if ((widthOriginal*rapporto)<=larghezzaSlider)
				{
					$( this ).css("width","initial").css("height","100%");
				}
				else
				{
					$( this ).css("width","100%").css("height","initial");
				}
			});
	}, 1000);
}

ContentManager.prototype.setAudio = function (oggettoAudio)
{
	console.log("sono in setAudio, con oggettoAudio: " + JSON.stringify(oggettoAudio));
	var url=oggettoAudio.path;
	url=decodeURIComponent(url);
	var tipo=oggettoAudio.type;
	if (tipo=="LINK")
	{
		this.creaAudio(url, tipo);
	}
	else
	{
		this.creaAudio(this.contenutiUrl + "/" + url, "LOCAL");
	}
}


ContentManager.prototype.creaAudio = function (urlAudio, tipo)
{
	
	var self=this;
	console.log("sono in creaAudio, con url: " + urlAudio + " e tipo: " + tipo);

	$("#audioSrc").attr("src",urlAudio);
	
    $("#loader").show();

	this.timeoutCaricamento = setTimeout(function() {
		//alert("sono nel timeout");
		if ($("#loader").is(':visible'))
		{
			//non ha caricato l'audio... alzo bandiera bianca
			//rimuovo il listener dall'elemento... casomai riuscisse...
		    var audio = document.getElementById('audio');
		    audio.removeEventListener('loadeddata', self.startAudio);
			// e metto un messaggio
			$("#messaggioGenerico p").html(getLabel("ERRORE_CARICAMENTO_AUDIO","Errore nel caricamento dell'audio"));
			$("#messaggioGenerico").show();
			$("#loader").hide();
			$("#audioOn").hide();
			$("#audioPause").hide();

			back();

			setTimeout(function() {
				$("#messaggioGenerico").hide();
			}, 4000);
		}
	}, 5000);

	var audio = document.getElementById('audio');
	audio.addEventListener('loadeddata', function() {console.log("loadeddata")});
	//audio.addEventListener('loadeddata', self.startAudio);
	//audio.addEventListener('loadstart', function() {console.log("loadstart")});
	//audio.addEventListener('durationchange', function() {console.log("durationchange")});
	//audio.addEventListener('loadeddata', function() {console.log("loadeddata")});
	//audio.addEventListener('loadedmetadata', function() {console.log("loadedmetadata")});
	//audio.addEventListener('progress', function() {console.log("progress")});
	//audio.addEventListener('canplay', function() {console.log("canplay")});
	audio.addEventListener('canplay', self.startAudio);
	audio.addEventListener('canplaythrough', function() {console.log("canplaythrough")});
	audio.addEventListener('ended', self.endAudio);
	audio.load();


}

ContentManager.prototype.startAudio = function ()
{
    var audio = document.getElementById('audio');
    audio.removeEventListener('loadeddata', this.startAudio);
    $("#loader").hide();
    clearTimeout(this.timeoutCaricamento);
    console.log("Loaded the audio's data!");
	startAudio();


}

ContentManager.prototype.endAudio = function ()
{
    $("#audioSrc").attr("src","");
    var audio = document.getElementById('audio');
    audio.removeEventListener('ended', this.endAudio);
    //clearTimeout(timeoutCaricamento);
    console.log("Audio end");
    $("#audioOn").hide();
    $("#audioPause").hide();


}


ContentManager.prototype.setTesto = function (oggettoTesto)
{
	var self=this;

	var testo=oggettoTesto.text;
	nasconditutto();
	//var testo = "sdfgsdfg dfgsdfg\nsdfgsdfg dsfgdsf dfgdsfg dfgdsfg\nfdsgsdfg dfgsdfg dfsgdsfg\nsdgsdfgsf\nsdsdgsfdg\nsdsfgdfg\nsdfgdfsgdfg\nsdgsdfgdsfg\nsdfgdfsgdfg\nsdfgsdfg dsfgdsf dfgdsfg dfgdsfg\nfdsgsdfg dfgsdfg dfsgdsfg\nsdgsdfgsf\nsdsdgsfdg\nsdsfgdfg\nsdfgdfsgdfg\nsdgsdfgdsfg\nsdfgdfsgdfg";
	testo = testo.trim();
	//tolgo le virgolette
	//testo = testo.substring(1, (testo.length-1));

	console.log("testo prima: " + testo);
	testo = testo.replace(/(?:\r\n|\r|\n)/g, '<br />');
	//all'inizio e alla fine ci sono le virgolette, le tolgo
	console.log("testo dopo: " + testo);

	$("#casellaTesto").html(testo);

	var dimensioni = this.getViewDimensions();

	var altezzaDisponibile=dimensioni.altezza-dimensioni.altezzaHeader-dimensioni.altezzaFooter;
	var altezzaTesto=altezzaDisponibile*0.7; //è l'altezza massima

	$("#textContainer").show();
	var posizione;
	//vedo se l'altezza, con questo testo, è > o < della massima altezza possibile
	var altezzaCorrente=$("#textContainer")[0].clientHeight;
	if (altezzaCorrente>altezzaTesto)
	{
		//limito
		$("#casellaTesto").css("height",altezzaTesto+"px");
		posizione=(altezzaDisponibile*0.1) + dimensioni.altezzaHeader;
	}
	else
	{
		$("#casellaTesto").css("height",altezzaCorrente+"px");
		//ricalcolo
		var altezzaCorrente2=$("#textContainer")[0].clientHeight;
		posizione=(dimensioni.altezza-dimensioni.altezzaHeader-dimensioni.altezzaFooter-altezzaCorrente2)/2 + dimensioni.altezzaHeader;
	}

	$("#textContainer").css("top",posizione+"px");

	$("#backButton").hide();

}


ContentManager.prototype.setElementoScaricabile = function (oggettoScaricabile)
{
	var self=this;

	console.log("sono in setElementoScaricabile, con oggettoScaricabile: " + JSON.stringify(oggettoScaricabile));

	var url=oggettoScaricabile.path;
	url=decodeURIComponent(url);
	var tipo=oggettoScaricabile.type;
	var descrizione=oggettoScaricabile.descr;
	if (tipo=="LINK")
	{
		this.creaElementoScaricabile(url, tipo, descrizione);
	}
	else
	{
		this.creaElementoScaricabile(contenutiUrl + "/" + url, tipo,descrizione);
	}
}


ContentManager.prototype.creaElementoScaricabile = function (url, tipo, descrizione)
{
	var self=this;
	nasconditutto();

	console.log("sono in creaElementoScaricabile, con url: " + url + " e tipo: " + tipo);

	$("#loader").show();

	//rimetto di base il contenitore al valore iniziale
	$("#imageContainer").css("width","90%").css("left","5%");
	// e anche l'immagine interna
	$("#imageContainerImg").css("width","100%");
	$("#imageContainerImg").css("height","initial");

	var dimensioni = this.getViewDimensions();

	var altezzaImmagineMax=(dimensioni.altezza-dimensioni.altezzaHeader-dimensioni.altezzaFooter)*0.8; //altezza massima immagine
	console.log("altezza massima: " + altezzaImmagineMax);

	$("#backButton").hide();

	//quanto lo devo fare grande l'immagine?
	var larghezzaDiv=$("#imageContainer").width();
	var htmlToAdd="";

	htmlToAdd += "<image id='imageContainerImg' src='' style='width:100%;border: 0px;background: none;margin: 0px;box-shadow: none;margin-top: 0%;margin-bottom: 0%;' />";
	htmlToAdd += "<div onclick='back();' style='background-color:rgba(0, 0, 0, 0.6);text-align:center;width: 100%;border: 0px solid blue;position: absolute;right: 0px;top:0px;color:white'><p class='testoBig' style='margin:10px;'>"+getLabel('CHIUDI','Chiudi')+"</p></div>";
	htmlToAdd += "<div id='bottoneScarica' style='background-color:rgba(0, 0, 0, 0.6);text-align:center;width: 100%;border: 0px solid blue;position: absolute;right: 0px;bottom:0px;color:white'><p class='testoBig' style='margin:10px;'>"+getLabel('SCARICA','Scarica')+"</p></div>";
	htmlToAdd += "<div id='iconaScarica' style='box-shadow:rgba(0, 0, 0,0.7) 10px 10px 10px;border:1px solid gray;background:white;text-align:center;width: 20%;position:absolute;left:40%;top:0px;padding:5px;'><img src='assets/downloadIcon.png' style='width:100%'></div>";
	
	$("#imageContainer").html(htmlToAdd);

	var image = document.getElementById('imageContainerImg');
	var downloadingImage = new Image();

	downloadingImage.onload = function(){
		$("#imageContainer").css("border","1px solid "+ self.coloreGenerale);
		$("#imageContainer").show();

		$("#loader").hide();
		$("#sfondoAlfato").show();

	    image.src = this.src;

	    //l'immagine riesce ad entrare nell'altezza massima?
		var altezzaImg=$("#imageContainerImg")[0].height;
		console.log("altezza effettiva immagine: " + altezzaImg);

		var posizione;
		if (altezzaImg<altezzaImmagineMax)
		{
			//tutto ok, la devo solo centrare
			posizione=(dimensioni.altezza-dimensioni.altezzaHeader-dimensioni.altezzaFooter-altezzaImg)/2 + dimensioni.altezzaHeader;
		}
		else
		{
			//deve comandare l'altezza massima
			$("#imageContainerImg").css("height",altezzaImmagineMax+"px").css("width","initial");
			//vedo la larghezza effettiva
			var larghImg=$("#imageContainerImg")[0].width;
			// e setto il contenitore
			$("#imageContainer").css("width",larghImg+"px").css("left",((dimensioni.larghezza-larghImg)/2)+"px");
			posizione=(dimensioni.altezza-dimensioni.altezzaHeader-dimensioni.altezzaFooter-altezzaImmagineMax)/2 + dimensioni.altezzaHeader;
			altezzaImg=$("#imageContainerImg")[0].height;
		}
		$("#imageContainer").css("top",posizione+"px");

		//aggiusto l'icona di download
		var altezzaIcona=$("#iconaScarica")[0].clientWidth; //è più o meno quadrata... faccio la larghezza perchè magari non l'ha ancora caricata
		$("#iconaScarica").css("top",((altezzaImg-altezzaIcona)/2)+"px");

		$("#bottoneScarica").click(function() {

			$("#loader").show();

			  AR.platform.sendJSONObject({
			    topic: "scaricaImmagine",
			    url: encodeURIComponent(url),
			    descr: descrizione
			  })
/*
			//document.location = 'architectsdk://scaricaImmagine_' + encodeURIComponent(JSON.stringify({
				url: url,
				descr: descrizione
			}));
*/
		});

		$("#iconaScarica").click(function() {

			$("#loader").show();

			  AR.platform.sendJSONObject({
			    topic: "scaricaImmagine",
			    url: encodeURIComponent(url),
			    descr: descrizione
			  })
/*
			//document.location = 'architectsdk://scaricaImmagine_' + encodeURIComponent(JSON.stringify({
				url: url,
				descr: descrizione
			}));
*/

		});

		resizeBasedOnResolution(self.sender)// per sicurezza

		setTimeout(function() {
			resizeBasedOnResolution(self.sender)// per ulteriore sicurezza
		}, 1000);

		setTimeout(function() {
			resizeBasedOnResolution(self.sender)// per ulteriore sicurezza
		}, 2000);


	};

	downloadingImage.src = url;

}


ContentManager.prototype.setDynamic = function (immagine, punto, indice, tipologia)
{
	var self=this;

	console.log("sono in setDynamic, con immagine: "+immagine+ ", punto: " + punto + ", indice: " + indice + ", tipologia: " + tipologia);

	actualClass==null;
	for (var i=0;i<arrayDynamicClasses.length;i++)
	{
		if (arrayDynamicClasses[i].chiSono()==tipologia) actualClass=arrayDynamicClasses[i];
	}

	//ho una classe non nulla?
	if (actualClass==null) return;

	//disabilito temporaneamente la camera
	AR.hardware.camera.enabled = false;
	// e disabilito tutto
	disabilitatutto();

	// e faccio partire la classe dinamica
	actualClass.renderMain(immagine,punto,deviceId);


	renderMainExt(immagine, punto, indice, deviceId)

}




ContentManager.prototype.getViewDimensions = function ()
{
	var self=this;

	var dimensioni = new Object();
	dimensioni.altezzaHeader = $("#header").height();
	dimensioni.altezzaFooter = $("#footerMsg").height();
	dimensioni.altezza=$( window ).height();
	dimensioni.larghezza=$( window ).width();

	/* controllare, per lo slideshow e per l'elemento scaricabile usavo questo...
	var larghezza=$( window )[0].innerWidth;
	var altezza=$( window )[0].innerHeight;
	*/

	console.log("Dimesioni View: " + JSON.stringify(dimensioni));

	return dimensioni;
}


ContentManager.prototype.setPano = function (oggettoPano)
{
	var self=this;

	console.log("sono in setPano, con oggettoPano: " + JSON.stringify(oggettoPano));

	//preName serve solo se devo memorizzare o cercare una immagine che non in origine sul nostro server

	//devo scaricare il file(oppure prendere quello in cache), lo deve fare fuori da wikitude... poi ritornerà l'ok

	var url=decodeURIComponent(oggettoPano.path);
	var fileName="";
	var fileToSave="";
	
	var tipo=oggettoPano.type;
	if (tipo=="LINK")
	{
		fileName= url;
		//in caso di file sulla rete e non nei contenuti di livengine, compongo il filename
        var pathSplit= url.split("/");
        //il nome è l'ultimo elemento
        fileToSave=oggettoPano.preName + pathSplit[pathSplit.length-1];

	}
	else
	{
		fileName = this.contenutiUrl + "/" + url;
		var pathSplit= url.split("/");
		//il nome è l'ultimo elemento, normalmente è contenuti/xxx_yy_zz_xxxxxx.jpg
		fileToSave = pathSplit[pathSplit.length-1];
	}

	AR.platform.sendJSONObject({
		topic: "scaricaPanoImage",
		fileName: encodeURIComponent(fileName),
		connectionPresent: oggettoPano.connectionPresent,
		fileToSave: fileToSave
	}); 

	//e aspetto la risposta....
}



ContentManager.prototype.creaPano = function (img)
{
	var self=this;

	console.log("sono in creaPano, con img: " + img );
	
	var pathImg = this.contenutiPath + "pano/" + img; 
	console.log("carico " + pathImg );


	AR.hardware.camera.enabled = false;
	// e disabilito tutto
	nasconditutto();

	stopBarAnim=true;
	stopBarAnimFn();

	var dimensioni = this.getViewDimensions();

	$("#panoContainer").show();

	panoViewer = new Kaleidoscope.Image({source: pathImg, 
										containerId: '#panoImgContainer',
										initialYaw: 90,
										height: dimensioni.altezza,
        								width: dimensioni.larghezza});
	panoViewer.render();

	$("#loader").hide();

	resizeBasedOnResolution(this.sender);


/*
	//per riattivare:
  setTimeout(function() {
    AR.hardware.camera.enabled = true;
    back();
  }, 500);




*/



}




