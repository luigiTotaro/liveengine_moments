angular.module('LiveEngine.WikiPrincipale.Services', [])

.service('WikiService_principale', function(LoggerService, HelperService, $rootScope, $http, $ionicModal, $ionicPopup, $ionicLoading, $timeout, GlobalVariables, FileSystemService, SupportServices, LazyLoader, WikiService, LanguageService, GeoLocationService, $state) {

    var me = this;

    //me.toDownload = new Array();

    me.callbacks = [];


    me.registerCallback = function(key, functionName, callback) {

        if(!me.callbacks[key]) {
            me.callbacks[key] = [];
        }

        delete me.callbacks[key][functionName];

        if(callback) {
            me.callbacks[key][functionName] = callback;
        }

    }


    me.loadARchitectWorld = function (example)
    {


        console.log("Entrato in loadARchitectWorld Progetto");

        GlobalVariables.wikitudePlugin.requestAccess(function()
        {
            console.log("Request Access OK");


            // check if the current device is able to launch ARchitect Worlds
            GlobalVariables.wikitudePlugin.isDeviceSupported(function()
            {
                console.log("Device is supported");
               //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);


                GlobalVariables.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
                {
                    //console.log("loadARworld ok, call calljavascript: "+example.localPath);
                    /* Respond to successful world loading if you need to */

                    console.log("dovrei averla chiamata....");

                }, function errorFn(error)
                {
                    console.log("Loading AR web view failed");
                    console.log(error);
                    
                    //alert('Loading AR web view failed');
                    HelperService.showUnrecoverableError(LanguageService.getLabel('ERRORE_AVVIO_RA'), true);
                },
                example.path, example.requiredFeatures, example.startupConfiguration
                );
                GlobalVariables.wikitudePlugin.callJavaScript('passData("'+example.localPath+'","'+example.localDynamicARPath+'","'+example.idPrj+'",'+example.isApiRest+',"'+example.baseUrl+'","'+example.deviceId+'",'+LazyLoader.appConfiguration.showDisclaimerForComments+',"'+LazyLoader.appConfiguration.commentsNickname+'","'+example.multiTarget+'","'+GlobalVariables.systemLanguage+'")');
                
                GlobalVariables.wikitudePlugin.setJSONObjectReceivedCallback(function (objPassed)
                    {
                        console.log("onJSONObjectReceived!!!");
                        console.log(JSON.stringify(objPassed));
                        switch(objPassed.topic) {
                            case "scaricaImmagine":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.TRIGGER_WIKI_EVT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        type: 'download_item',
                                        url: decodeURIComponent(objPassed.url),
                                        descr: objPassed.descr
                                    }
                                });                                
                                break;
                            case "getLikes":
                                SupportServices.getLikes({
                                    idProgetto: objPassed.id,
                                    onComplete: function(err, json) {

                                        console.log("on complete: " + err + " - " + JSON.stringify(json));
                                        if(err) {
                                            //ci sta un errore, ma comunque continuo
                                            GlobalVariables.wikitudePlugin.callJavaScript('getLikesReturn(null)');

                                        } else {

                                            $timeout(function() {
                                              if (json.errore)
                                              {
                                                GlobalVariables.wikitudePlugin.callJavaScript('getLikesReturn(null)');
                                              }
                                              else
                                              {
                                                var toRet=JSON.stringify(json);
                                                GlobalVariables.wikitudePlugin.callJavaScript('getLikesReturn('+toRet+')');
                                              }

                                            });

                                        }

                                    }

                                });
                                break;
                            case "setLikes":
                                var coords=null;
                                if (GeoLocationService.getCurrentPosition() != null) coords= '{"lat": '+GeoLocationService.getCurrentPosition().coords.latitude+',"lon": '+GeoLocationService.getCurrentPosition().coords.longitude+'}';

                                SupportServices.setLikes({
                                    idProgetto: GlobalVariables.application.currentProgetto.idProgetto,
                                    idImage: objPassed.id,
                                    like: objPassed.like,
                                    coords: coords,
                                    onComplete: function(err, json) {

                                        console.log("on complete: " + err + " - " + JSON.stringify(json));
                                        if(err) {
                                            //ci sta un errore, ma comunque continuo
                                            GlobalVariables.wikitudePlugin.callJavaScript('setLikesReturn(null)');

                                        } else {

                                            $timeout(function() {
                                              if (json.errore)
                                              {
                                                GlobalVariables.wikitudePlugin.callJavaScript('setLikesReturn(null)');
                                              }
                                              else
                                              {
                                                var toRet=JSON.stringify(json.moments_likes);
                                                GlobalVariables.wikitudePlugin.callJavaScript('setLikesReturn('+toRet+')');
                                              }

                                            });

                                        }

                                    }

                                });
                                break;
                            case "setClap":
                                var coords=null;
                                if (GeoLocationService.getCurrentPosition() != null) coords= '{"lat": '+GeoLocationService.getCurrentPosition().coords.latitude+',"lon": '+GeoLocationService.getCurrentPosition().coords.longitude+'}';

                                SupportServices.setClap({
                                    idProgetto: GlobalVariables.application.currentProgetto.idProgetto,
                                    idImage: objPassed.id,
                                    coords: coords,
                                    onComplete: function(err, json) {

                                        console.log("on complete: " + err + " - " + JSON.stringify(json));
                                        if(err) {
                                            //ci sta un errore, ma comunque continuo
                                            GlobalVariables.wikitudePlugin.callJavaScript('setClapReturn(null)');

                                        } else {

                                            $timeout(function() {
                                              if (json.errore)
                                              {
                                                GlobalVariables.wikitudePlugin.callJavaScript('setClapReturn(null)');
                                              }
                                              else
                                              {
                                                var toRet=JSON.stringify(json.moments_likes);
                                                GlobalVariables.wikitudePlugin.callJavaScript('setClapReturn('+toRet+')');
                                              }

                                            });

                                        }

                                    }

                                });
                                break;
                            case "getLikesCommentsClaps":
                                SupportServices.getLikes({
                                    idProgetto: objPassed.id,
                                    onComplete: function(err, json) {

                                        console.log("on complete: " + err + " - " + JSON.stringify(json));
                                        if(err) {
                                            //ci sta un errore, ma comunque continuo
                                            GlobalVariables.wikitudePlugin.callJavaScript('getLikesCommentsClapsReturn(null)');

                                        } else {

                                            $timeout(function() {
                                              if (json.errore)
                                              {
                                                GlobalVariables.wikitudePlugin.callJavaScript('getLikesCommentsClapsReturn(null)');
                                              }
                                              else
                                              {
                                                var toRet=JSON.stringify(json);
                                                GlobalVariables.wikitudePlugin.callJavaScript('getLikesCommentsClapsReturn('+toRet+')');
                                              }

                                            });

                                        }

                                    }

                                });
                                break;                                
                            case "getUsersNumber":
                                var numero=GlobalVariables.application.currentProgetto.numUtenti;
                                GlobalVariables.wikitudePlugin.callJavaScript('getUsersNumber('+numero+')');
                                break;
                            case "checkConnessione":
                                var connessione=HelperService.isNetworkAvailable();
                                console.log("Check connessione: " + connessione);
                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessione('+connessione+')');
                                break;
                            case "openContent":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_AUGMENTED_CONTENT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        id: objPassed.id,
                                        titolo: objPassed.titolo
                                    }
                                });
                                break;
                            case "linkEsterno":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_EXTERNAL_URL,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        url: decodeURIComponent(objPassed.url)
                                    }
                                });
                                break;

                            case "scaricaSlideshow":
                                //nell'oggetto ci sta l'id del punto e le immagini da scaricare
                                WikiService.toDownload = new Array();
                                var idPunto=objPassed.array[0];
                                for (var index=1;index<objPassed.array.length;index++)
                                {
                                  WikiService.toDownload.push(decodeURIComponent(objPassed.array[index]));
                                }
                                console.log("array da scaricare");
                                console.log(WikiService.toDownload);
                                WikiService.downloadFiles(0,idPunto);
                                break;
                            case "existSlideshow":
                                //esiste, nella directory slideshow, qualche file che inizia con l'id del punto?
                                var name=objPassed.id+"_0"; //almeno il primo
                                FileSystemService.fileExists({
                                    directory: SupportServices.BUFFER_DIRECTORY + '/slideshow',
                                    fileName: name,
                                    onComplete: function(err, isAvailable) {
                                        if(err) {
                                            //errore, getto la spugna
                                            console.log("Errore, comunico che non ci sono files");
                                            GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                        } else {
                                            if(!isAvailable) {
                                                 //non esiste, getto la spugna
                                                console.log("Non Esiste, comunico che non ci sono files");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                            } else {
                                                //esiste, vado avanti
                                                console.log("Esiste");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(true)');
                                            }
                                        }
                                    }
                                });
                                break;
                            case "scaricaPanoImage":
                                //var fileName=objPassed.fileName;
                                //vedo se esiste
                                FileSystemService.fileExists({
                                    directory: SupportServices.BUFFER_DIRECTORY + '/pano',
                                    fileName: objPassed.fileToSave,
                                    onComplete: function(err, isAvailable) {
                                        if(err) {
                                            console.log("err: " + err)
                                            //errore, se ci sta la connessione lo scarico
                                            if (objPassed.connectionPresent)
                                            {
                                                console.log("Non esiste, provo a scaricarlo");
                                                WikiService.downloadPanoFile(decodeURIComponent(objPassed.fileName), objPassed.fileToSave);
                                            }
                                            else
                                            {
                                                console.log("Non esiste e non ho la connessione, rinuncio");
                                                GlobalVariables.wikitudePlugin.callJavaScript('panoReturn(false,null)');
                                            }
                                        } else {
                                            if(!isAvailable) {
                                                 //non esiste, se ci sta la connessione lo scarico
                                                if (objPassed.connectionPresent)
                                                {
                                                    console.log("Non esiste, provo a scaricarlo");
                                                    WikiService.downloadPanoFile(decodeURIComponent(objPassed.fileName), objPassed.fileToSave);
                                                }
                                                else
                                                {
                                                    console.log("Non esiste e non ho la connessione, rinuncio");
                                                    GlobalVariables.wikitudePlugin.callJavaScript('panoReturn(false,null)');
                                                }                                            
                                            } else {
                                                //esiste, uso quello
                                                console.log("Esiste");
                                                GlobalVariables.wikitudePlugin.callJavaScript("panoReturn(true,'"+objPassed.fileToSave+"')");
                                            }
                                        }
                                    }
                                });



                                //WikiService.downloadPanoFile(decodeURIComponent(fileName));
                                break;
                            case "writeCommentMoments":
                                GlobalVariables.wikitudePlugin.hide();
                                WikiService.showPopupWriteComment(objPassed);
                                break;
                            case "commentDisclaimerOK":
                                LazyLoader.appConfiguration.showDisclaimerForComments = false;
                                LazyLoader.saveConfiguration({
                                    onComplete: function(err) {
                                    

                                    }
                                })
                                break;
                            case "setNickname":
                                LazyLoader.appConfiguration.commentsNickname = objPassed.nick;
                                LazyLoader.saveConfiguration({
                                    onComplete: function(err) {
                                    

                                    }
                                })
                                break;
                            case "socialPublish":

                                GlobalVariables.wikitudePlugin.hide();
                                
                                $ionicLoading.show({
                                    template: '<div style="padding:10px 5px;">Attendere.....</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
                                    duration: 15000
                                });

                                var appParameter="";

                                if (objPassed.type=="fb")
                                {
                                    
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.facebook";
                                    else appParameter="com.facebook.katana";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per fb');
                                        window.plugins.socialsharing.canShareVia(appParameter, objPassed.text, null, decodeURIComponent(objPassed.image), null, function(e)
                                        {
                                            console.log("success: " + e)
                                           window.plugins.socialsharing.shareViaFacebook(null, decodeURIComponent(objPassed.image), decodeURIComponent(objPassed.link), function()
                                            {
                                                console.log('share ok fb');
                                                WikiService.showPopupReturn2AR("fb");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore fb: ' + errormsg);
                                                WikiService.showPopupReturn2AR("fb");
                                            });
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("fb_error");
                                            $ionicLoading.hide();
                                        });
                                    }, 1000);                              
                                }
                                else if (objPassed.type=="tw")
                                {
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.twitter";
                                    else appParameter="twitter";

                                     setTimeout(function() {
                                        console.log('chiamo il plugin di share per tw');
                                        window.plugins.socialsharing.canShareVia(appParameter, decodeURIComponent(objPassed.text), null, decodeURIComponent(objPassed.image), null, function(e)
                                        {
                                            console.log("success: " + e);
                                            //passare null dove non utilizzato
                                            window.plugins.socialsharing.shareViaTwitter(decodeURIComponent(objPassed.text), decodeURIComponent(objPassed.image), decodeURIComponent(objPassed.link), function()
                                            {
                                                console.log('share ok tw');
                                                WikiService.showPopupReturn2AR("tw");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore tw: ' + errormsg);
                                                WikiService.showPopupReturn2AR("tw");
                                            });  
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("tw_error");
                                            $ionicLoading.hide();
                                        });
                                        console.log("dovrei aver fatto");
                                    }, 1000);                              
                                }
                                else if (objPassed.type=="insta")
                                {

                                    if (GlobalVariables.platform=="iOS") appParameter="instagram";
                                    else appParameter="instagram";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per instagram');
                                        window.plugins.socialsharing.canShareVia(appParameter, decodeURIComponent(objPassed.text), null, decodeURIComponent(objPassed.image), null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaInstagram(decodeURIComponent(objPassed.text), decodeURIComponent(objPassed.image), function()
                                            {
                                              console.log('share ok insta');
                                                WikiService.showPopupReturn2AR("insta");
                                                //GlobalVariables.wikitudePlugin.show();
                                            }, function(errormsg)
                                            {
                                              console.log('Errore insta: ' + errormsg);
                                                WikiService.showPopupReturn2AR("insta");
                                                 //GlobalVariables.wikitudePlugin.show();
                                           });                               
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("insta_error");
                                            $ionicLoading.hide();
                                        });
                                     }, 1000);                              
                                }
                                break;
                            case "salvaConfig": //viene chiamata dalla classe dinamica Commento_xxxxx, ma al momento è commentata dappertutto
                                /*
                                LazyLoader.appConfiguration.showDisclaimerForComments = parse.parameter.showCommentsDisclaimer;
                                LazyLoader.appConfiguration.commentsNickname = parse.parameter.commentsNickname;
                                LazyLoader.saveConfiguration({
                                    onComplete: function(err) {
                                    }
                                })
                                */
                                break;                                
                            default:
                                console.log("Esco dalla RA");
                                try{
                                    GlobalVariables.wikitudePlugin.close();
                                    console.log("dovrei averla chiusa");
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                catch(err){
                                    console.log(err);
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                        }

                    }); 
                
                GlobalVariables.wikitudePlugin.setOnUrlInvokeCallback(function (url)
                    {
                        var parse = WikiService.parseWikitudeCallback(url);
                        switch(parse.value) {
                            case "storage":
                                break;
                            /*
                            case "scaricaImmagine":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.TRIGGER_WIKI_EVT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        type: 'download_item',
                                        url: parse.parameter.url,
                                        descr: parse.parameter.descr
                                    }
                                });
                                break;
                            */
                            /*
                            case "salvaConfig": //viene chiamata dalla classe dinamica Commento_xxxxx, ma al momento è commentatat dappertutto
                                LazyLoader.appConfiguration.showDisclaimerForComments = parse.parameter.showCommentsDisclaimer;
                                LazyLoader.appConfiguration.commentsNickname = parse.parameter.commentsNickname;
                                LazyLoader.saveConfiguration({
                                    onComplete: function(err) {
                                    }
                                })
                                break;
                            */
                            /*
                            case "linkEsterno":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_EXTERNAL_URL,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        url: parse.parameter
                                    }
                                });
                                break;
                            */
                            /*
                            case "checkConnessione":
                                var connessione=HelperService.isNetworkAvailable();
                                console.log("Check connessione: " + connessione);
                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessione('+connessione+')');
                                break;
                            */
                            /*
                            case "getUsersNumber":
                                var numero=GlobalVariables.application.currentProgetto.numUtenti;
                                GlobalVariables.wikitudePlugin.callJavaScript('getUsersNumber('+numero+')');
                                break;
                            */
                            /*
                            case "scaricaSlideshow":
                                //nell'oggetto ci sta l'id del punto e le immagini da scaricare
                                WikiService.toDownload = new Array();
                                var idPunto=parse.parameter[0];
                                for (var index=1;index<parse.parameter.length;index++)
                                {
                                  WikiService.toDownload.push(parse.parameter[index]);
                                }
                                console.log("array da scaricare");
                                console.log(WikiService.toDownload);
                                WikiService.downloadFiles(0,idPunto);
                                break;
                            case "existSlideshow":
                                //esiste, nella directory slideshow, qualche file che inizia con l'id del punto?
                                var name=parse.parameter+"_0"; //almeno il primo
                                FileSystemService.fileExists({
                                    directory: SupportServices.BUFFER_DIRECTORY + '/slideshow',
                                    fileName: name,
                                    onComplete: function(err, isAvailable) {
                                        if(err) {
                                            //errore, getto la spugna
                                            console.log("Errore, comunico che non ci sono files");
                                            GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                        } else {
                                            if(!isAvailable) {
                                                 //non esiste, getto la spugna
                                                console.log("Non Esiste, comunico che non ci sono files");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                            } else {
                                                //esiste, vado avanti
                                                console.log("Esiste");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(true)');
                                            }
                                        }
                                    }
                                });
                                break;
                            */
                            /*
                            case "openContent":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_AUGMENTED_CONTENT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        id: parse.parameter.id,
                                        titolo: parse.parameter.titolo
                                    }
                                });
                                break;
                            */
                            /*
                            case "socialPublish":
                                var oggetto=parse.parameter;

                                GlobalVariables.wikitudePlugin.hide();
                                
                                $ionicLoading.show({
                                    template: '<div style="padding:10px 5px;">Attendere.....</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
                                    duration: 15000
                                });

                                var appParameter="";

                                if (oggetto.type=="fb")
                                {
                                    
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.facebook";
                                    else appParameter="com.facebook.katana";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per fb');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                           window.plugins.socialsharing.shareViaFacebook(null, oggetto.image, oggetto.link, function()
                                            {
                                                console.log('share ok fb');
                                                WikiService.showPopupReturn2AR("fb");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore fb: ' + errormsg);
                                                WikiService.showPopupReturn2AR("fb");
                                            });
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("fb_error");
                                            $ionicLoading.hide();
                                        });
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="tw")
                                {
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.twitter";
                                    else appParameter="twitter";

                                     setTimeout(function() {
                                        console.log('chiamo il plugin di share per tw');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e);
                                            //passare null dove non utilizzato
                                            window.plugins.socialsharing.shareViaTwitter(oggetto.text, oggetto.image, oggetto.link, function()
                                            {
                                                console.log('share ok tw');
                                                WikiService.showPopupReturn2AR("tw");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore tw: ' + errormsg);
                                                WikiService.showPopupReturn2AR("tw");
                                            });  
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("tw_error");
                                            $ionicLoading.hide();
                                        });
                                        console.log("dovrei aver fatto");
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="insta")
                                {

                                    if (GlobalVariables.platform=="iOS") appParameter="instagram";
                                    else appParameter="instagram";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per instagram');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaInstagram(oggetto.text, oggetto.image, function()
                                            {
                                              console.log('share ok insta');
                                                WikiService.showPopupReturn2AR("insta");
                                                //GlobalVariables.wikitudePlugin.show();
                                            }, function(errormsg)
                                            {
                                              console.log('Errore insta: ' + errormsg);
                                                WikiService.showPopupReturn2AR("insta");
                                                 //GlobalVariables.wikitudePlugin.show();
                                           });                               
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("insta_error");
                                            $ionicLoading.hide();
                                        });
                                     }, 1000);                              
                                }
                                break;
                                */
                            case "writeComment":
                                /*
                                GlobalVariables.wikitudePlugin.hide();
                                WikiService.showPopupWriteComment(parse.parameter);
                                break;
                                */
                            default:
                                /*
                                console.log("Esco dalla RA");
                                try{
                                    GlobalVariables.wikitudePlugin.close();
                                    console.log("dovrei averla chiusa");
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                catch(err){
                                    console.log(err);
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                */
                        }
                    });


            }, function(errorMessage)
            {
                console.log("Device is NOT supported");
                //alert(errorMessage);
                HelperService.showUnrecoverableError(LanguageService.getLabel('DEVICE_NON_SUPPORTATO'), true);
            },
            example.requiredFeatures
            );

        }, function(errorMessage)
        {
            console.log(errorMessage);
            //alert("Non riesco ad accedere alla Fotocamera");
            HelperService.showUnrecoverableError(LanguageService.getLabel('NO_ACCESSO_FOTOCAMERA'), true);
        },
        example.requiredFeatures
        );



    }


/*
    me.go2ProjectPage = function() {
        $ionicLoading.show({
          template: LanguageService.getLabel('ATTENDERE')
        });

        SupportServices.sceltaProgetto({
          onComplete: function(err, json) {

              $ionicLoading.hide();
              if(err) {

                  var alertPopup = $ionicPopup.alert({
                     title: 'Attenzione',
                     template: '----Sembrano esserci problemi di rete che impediscono la fruizione dell\'applicazione'
                   });

                  return;
              }

              $state.go('progetto_detail');

          }

        });
    };
*/



})
