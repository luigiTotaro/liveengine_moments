angular.module('LiveEngine.Album.Services', [])


.service('AlbumServiceHelper', function(GlobalVariables, $rootScope, $timeout,
$http, $ionicModal, $ionicPopup, $sce, ionImgCacheSrv, SupportServices, LanguageService, GeoLocationService, LazyLoader, LoggerService) {

  var self = this;

  self.showAlbum = function(parentScope) {

      var usersAlbumScope = $rootScope.$new(true);


      usersAlbumScope.closeUsersGuideWnd = function() {

          usersAlbumScope.usersGuideWnd.remove();
          usersAlbumScope.$destroy();
          delete usersAlbumScope;

      };


      usersAlbumScope.likeClick = function(targetId,like) {
        //console.log(targetId, like);
        //chiamo la funzione per inserire/updatare il like

        var coords=null;
        if (GeoLocationService.getCurrentPosition() != null) coords= '{"lat": '+GeoLocationService.getCurrentPosition().coords.latitude+',"lon": '+GeoLocationService.getCurrentPosition().coords.longitude+'}';

        //da passare alla funzione di set
        //console.log(GlobalVariables.application.currentProgetto.idProgetto);
        //console.log(targetId);
        //console.log(like);
        //console.log(coords);

        
        SupportServices.setLikes({
            idProgetto: GlobalVariables.application.currentProgetto.idProgetto,
            idImage: targetId,
            like: like,
            coords: coords,
            onComplete: function(err, json) {

                //console.log("on complete: " + err + " - " + JSON.stringify(json));
                if(err) {

                    HelperService.showUnrecoverableError(err, true);
                    $ionicLoading.hide();

                } else {

                    $timeout(function() {
                      if (json.errore)
                      {
                        HelperService.showUnrecoverableError(json.errore, true);
                        $ionicLoading.hide();                      
                      }

                      GlobalVariables.application.currentProgetto.moments_likes = json.moments_likes;                  
                      
                      //console.log(GlobalVariables.application.currentProgetto.moments_likes);

                      self.aggiornaLikes(usersAlbumScope);
                      //AlbumServiceHelper.showAlbum();

                      usersAlbumScope.showHideIcons(targetId);
                    });

                }

            }

        });
      };

      usersAlbumScope.carrelloClick = function(targetUrl) {
        


        //console.log(targetId, like);
        // prendo l'array di immagini attuale
        var arrayStr = window.localStorage.getItem("immaginiCarrello");
        
        //console.log(arrayStr);
        
        var array = new Array();
        if (arrayStr==null)
        {
          var item = new Object();
          item.prjId = GlobalVariables.application.currentProgetto.idProgetto;
          item.targetUrl = targetUrl;
          array.push(item);
          $ionicPopup.alert({
                           title: 'Foto aggiunta al tuo carrello'
                          });          
        }
        else
        {
          //controllo se già esistente
          array=JSON.parse(arrayStr);
          if (array.length==0)
          {
            var item = new Object();
            item.prjId = GlobalVariables.application.currentProgetto.idProgetto;
            item.targetUrl = targetUrl;
            array.push(item);
            $ionicPopup.alert({
                             title: 'Foto aggiunta al tuo carrello'
                            });
          }
          else
          {
            var trovato=false;
            for (var i=0;i<array.length;i++)
            {
              if (array[i].targetUrl==targetUrl) trovato = true;
            }
            if (trovato)
            {
              //console.log("Elemento già presente");
              $ionicPopup.alert({
                               title: 'Foto già presente nel tuo carrello'
                              });
            }
            else
            {
              var item = new Object();
              item.prjId = GlobalVariables.application.currentProgetto.idProgetto;
              item.targetUrl = targetUrl;
              array.push(item);
              $ionicPopup.alert({
                               title: 'Foto aggiunta al tuo carrello'
                              });
            }

          }

        }

        window.localStorage.setItem("immaginiCarrello", JSON.stringify(array));

        //var value = window.localStorage.getItem("immaginiCarrello");
        //console.log("localstorage: "+ value);        

        /*
        window.localStorage.setItem("gino", "mutanda");

        var value = window.localStorage.getItem("gino");
        console.log("localstorage: "+ value);        
*/

      };

      usersAlbumScope.clapClick = function(targetId) {
        //console.log(targetId, like);
        //chiamo la funzione per inserire/updatare il like

        var coords=null;
        if (GeoLocationService.getCurrentPosition() != null) coords= '{"lat": '+GeoLocationService.getCurrentPosition().coords.latitude+',"lon": '+GeoLocationService.getCurrentPosition().coords.longitude+'}';

        //da passare alla funzione di set
        //console.log(GlobalVariables.application.currentProgetto.idProgetto);
        //console.log(targetId);
        //console.log(coords);

        
        SupportServices.setClap({
            idProgetto: GlobalVariables.application.currentProgetto.idProgetto,
            idImage: targetId,
            coords: coords,
            onComplete: function(err, json) {

                //console.log("on complete: " + err + " - " + JSON.stringify(json));
                if(err) {

                    HelperService.showUnrecoverableError(err, true);
                    $ionicLoading.hide();

                } else {

                    $timeout(function() {
                      if (json.errore)
                      {
                        HelperService.showUnrecoverableError(json.errore, true);
                        $ionicLoading.hide();                      
                      }

                      GlobalVariables.application.currentProgetto.moments_likes = json.moments_likes;                  
                      
                      //console.log(GlobalVariables.application.currentProgetto.moments_likes);

                      self.aggiornaLikes(usersAlbumScope);
                      //AlbumServiceHelper.showAlbum();

                    });

                }

            }

        });
      };

       usersAlbumScope.onSlideChanged = function() {
        //console.log("cambiato");
      };     

      
      usersAlbumScope.showHideIcons = function(targetId) {
        //console.log(targetId);
        //trovo quell'item e cambio lo stato delle icone visibili o meno
        _.each(usersAlbumScope.slide_list, function(item) {
            //item.url = GlobalVariables.baseUrl + "/" + item.url;
            if (item.id==targetId)
            {
              item.iconsVisible = !item.iconsVisible;
            }
        });        

      };


      usersAlbumScope.hsClick = function(hsId) {
        //console.log(hsId);
        var config=null;

        _.each(usersAlbumScope.slide_list, function(item) {
            //item.url = GlobalVariables.baseUrl + "/" + item.url;
            for (var i=0;i<item.punti.length;i++)
            {
              if (item.punti[i].id==hsId)
              {
                //console.log("trovato");
                config=JSON.parse(item.punti[i].config)[0];
                //console.log(config);
                if (config.tipo==1) //video
                {
                  if (parentScope.audioState==1)
                  {
                    parentScope.audio.pause();
                    parentScope.audioState=2; //audio in pausa                    
                  }
                  self.showVideo(config.config);
                }
                if (config.tipo==2) //slideshow
                {
                  self.showSlideshow(config.config);
                }
                if (config.tipo==3) //testo
                {
                  self.showText(config.config);
                }
                if (config.tipo==6) //link esterno
                {
                    if (parentScope.audioState==1)
                    {
                      parentScope.audio.pause();
                      parentScope.audioState=2; //audio in pausa                    
                    }
                    var myConfig= JSON.parse(config.config);
                    LoggerService.triggerAction({
                        action: LoggerService.ACTION.OPEN_EXTERNAL_URL,
                        state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                        data: {
                            url: decodeURIComponent(myConfig.url)
                        }
                    });


                }


              }
            }
            
        });

        //self.showVideo(hsId);
      };     

      usersAlbumScope.commentClick = function(targetId) {
        //console.log(targetId);
        self.showComments(targetId);
      };    


      usersAlbumScope.slide_list = [];
      usersAlbumScope.imgLike = [];


      usersAlbumScope.options = {
          loop: false,
          effect: 'slide',
          speed: 500,
      }

      usersAlbumScope.usersGuideWnd = $ionicModal.fromTemplate(

        '<ion-modal-view id="albumSlides" padding="true" cache-view="false" style="background-color:rgba(255,255,255,1.0); width:100%; height:100%; left:0px; right:0px; top:0px;">' +
              '<ion-content scroll="false" padding="false" style="">' +

                  '<a ng-click="closeUsersGuideWnd()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated"></a>' +

                  '<ion-slides options="options" onChange="onSlideChanged()" slider="data.slider">' +

                      //slides +
                      '<ion-slide-page ng-repeat="current_slide in slide_list" style="padding:16px;">' +

                          '<ion-slide-page>'+

                            '<div id="guide_{{$index}}" class="box immagine" ion-img-cache-bg style="background-image:url({{current_slide.url}});">' +
                                
                                '<div id="containerHs">' +                                 
                                  '<div ng-repeat="current_hs in current_slide.punti" style="" class="divHs">' +
                                    '<img src="{{current_hs.indicatorePath}}" class="hs" ng-click="hsClick({{current_hs.id}})"></img>' +
                                    '<p style="text-align: center;margin-bottom:0px;">{{current_hs.label}}</p>' +
                                  '</div>' +
                                '</div>' +

                                
                                '<div id="containerLikes" class="containerLikes" ng-if="current_slide.iconsVisible == true" style="display: flex;justify-content: space-around;">' +                                 
                                      

                                      '<img src="{{imgLike[2]}}" class="likeImg" ng-click="likeClick(current_slide.id,2)"></img>' +
                                      '<p style="text-align: center;" class="likeValue">{{current_slide.likes[2]}}</p>' +                                    
                                      '<img src="{{imgLike[3]}}" class="likeImg" ng-click="likeClick(current_slide.id,3)"></img>' +
                                      '<p style="text-align: center;" class="likeValue">{{current_slide.likes[3]}}</p>' +  
                                      '<img src="{{imgLike[4]}}" class="likeImg" ng-click="likeClick(current_slide.id,4)"></img>' +
                                      '<p style="text-align: center;" class="likeValue">{{current_slide.likes[4]}}</p>' +  
                                      '<img src="{{imgLike[5]}}" class="likeImg" ng-click="likeClick(current_slide.id,5)"></img>' +
                                      '<p style="text-align: center;" class="likeValue">{{current_slide.likes[5]}}</p>' +  
                                      '<img src="{{imgLike[6]}}" class="likeImg" ng-click="likeClick(current_slide.id,6)"></img>' +
                                      '<p style="text-align: center;" class="likeValue">{{current_slide.likes[6]}}</p>' +  
                                      '<img src="{{imgLike[7]}}" class="likeImg" ng-click="likeClick(current_slide.id,6)"></img>' +
                                      '<p style="text-align: center;" class="likeValue">{{current_slide.likes[7]}}</p>' +  

/* 

                                    '<div ng-repeat="current_img in imgLike" style="">' +
                                      

                                      '<img ng-if="$index > 1" src="{{current_img}}" class="likeImg" ng-click="likeClick(current_slide.id,$index)"></img>' +
                                      '<p ng-if="$index > 1" style="text-align: center;" class="likeValue">{{current_slide.likes[$index]}}</p>' +

                                  '</div>' +
 */                              
                                '</div>' +

                                '<div id="containerIcons" class="containerIcons">' +                                 
                                  '<div class="containerIconsSingle">' +
                                    '<img class="iconsImage" src="{{current_slide.LikeImg}}" ng-click="showHideIcons(current_slide.id)"></img>' +
                                    '<p style="text-align: center;" class="iconsValue">{{current_slide.totalLikes}}</p>' +
                                  '</div>' +
                                  '<div class="containerIconsSingle">' +
                                    '<img class="iconsImage" src="{{imgComment}}" ng-click="commentClick(current_slide.id)"></img>' +
                                    '<p style="text-align: center;" class="iconsValue">{{current_slide.totalCommenti}}</p>' +
                                  '</div>' +
                                  '<div class="containerIconsSingle">' +
                                    '<img class="iconsImage" src="{{imgClap}}" ng-click="clapClick(current_slide.id)"></img>' +
                                    '<p style="text-align: center;" class="iconsValue">{{current_slide.totalClap}}</p>' +
                                  '</div>' +
                                  '<div class="containerIconsSingle">' +
                                    '<img class="iconsImage" src="{{imgCarrello}}" ng-click="carrelloClick(current_slide.url)"></img>' +
                                    '<p style="text-align: center;" class="iconsValue"></p>' +
                                  '</div>' +
                                '</div>' +

                            '</div>' +

                            //'<div id="guide_{{$index}}" class="box" ion-img-cache-bg style="background-repeat: no-repeat; background-position: center center; background-size:contain; background-image:url({{current_slide.url}}); height:100%;"></div>'+
                            //'<img id="guide_{{$index}}" class="" ion-img-cache-bg src="{{current_slide.url}}" style=""></div>'+

                          '</ion-slide-page>' +

                      '</ion-slide-page>' +

                  '</ion-slides>' +

              '</ion-content>' +
          '</ion-modal-view>', {
              scope: usersAlbumScope,
              animation :'none',
              hardwareBackButtonClose: false
          }

    );



    usersAlbumScope.usersGuideWnd.show().then(function() {

        //preparo le immagini da mostrare
        var targets= GlobalVariables.application.currentProgetto.detail.target;
        //var slides = new array();
        var likes = GlobalVariables.application.currentProgetto.moments_likes;
        var comments = GlobalVariables.application.currentProgetto.moments_comments;
        //console.log("likes");
        //console.log(likes);
        //console.log("comments");
        //console.log(comments);
        //console.log(GlobalVariables.application.currentProgetto.moments_likes);

        usersAlbumScope.imgLike[0]=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.like;
        usersAlbumScope.imgLike[1]=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.icona_1;
        usersAlbumScope.imgLike[2]=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.icona_2;
        usersAlbumScope.imgLike[3]=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.icona_3;
        usersAlbumScope.imgLike[4]=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.icona_4;
        usersAlbumScope.imgLike[5]=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.icona_5;
        usersAlbumScope.imgLike[6]=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.icona_6;
        usersAlbumScope.imgLike[7]=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.icona_7;        

        usersAlbumScope.imgClap=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.clap;
        usersAlbumScope.imgComment=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.comment;
        usersAlbumScope.imgCarrello=GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.moments_icons.carrello;


        _.each(targets, function(target) {
            //item.url = GlobalVariables.baseUrl + "/" + item.url;
            var item= new Object();
            item.url=target.imagePath;
            item.url = item.url.replace("_indice.", ".");

            item.id=target.targetName;
            item.punti=target.punti;
            //item.puntiDom="";
            //creo la struttura per gli hotspot
            for (var i=0;i<item.punti.length;i++)
            {
              var path="";
              //if (item.punti[i].tipoIndicatore == -1)
              if (typeof item.punti[i].hsc_path != "undefined")
              {
                path=GlobalVariables.baseUrl + "/" + item.punti[i].hsc_path;
              }
              else
              {
                if (item.punti[i].tipoIndicatore==-1) item.punti[i].tipoIndicatore=0;
                path="clientRecognition/assets/indicatore_"+item.punti[i].tipoIndicatore+"_"+item.punti[i].coloreIndicatore+".png";
              }

              item.punti[i].indicatorePath=path;
            }
            item.iconsVisible=false;

            

            //per ogni slide (immagine) devo vedere quanti like ci sono stati, e se uno di questi è stato scelto dall'utente corrente
            item.likes = new Array(0,0,0,0,0,0,0,0,0,0);
            item.LikeImg = usersAlbumScope.imgLike[0];
            var totalLike=0;
            if (item.id!=null)
            {
              //console.log("cerco i likes per l'immagine " + item.id);
              _.each(likes, function(like) {
                  
                  if (like.idImmagine==item.id)
                  {
                    if (like.like!=null)
                    {
                      item.likes[like.like]++;
                      totalLike++;
                      if (like.deviceId==GlobalVariables.deviceUUID) item.LikeImg=usersAlbumScope.imgLike[like.like];
                    }
                  }

              });
              item.totalLikes=totalLike;
            }

            //per ogni slide (immagine) devo vedere quanti clap ci sono stati
            var totalClap=0;
            if (item.id!=null)
            {
              //console.log("cerco i clap per l'immagine " + item.id);
              _.each(likes, function(like) {
                  
                  if (like.idImmagine==item.id)
                  {
                    totalClap=totalClap+parseInt(like.clap);
                  }

              });
              item.totalClap=totalClap;
            }

            //per ogni slide (immagine) devo vedere quanti commenti ci sono stati
            var totalCommenti=0;
            if (item.id!=null)
            {
              //console.log("cerco i commenti per l'immagine " + item.id);
              _.each(comments, function(comment) {
                  
                  if (comment.idImmagine==item.id)
                  {
                    totalCommenti=parseInt(comment.totale);
                  }

              });
              item.totalCommenti=totalCommenti;
            }

            usersAlbumScope.slide_list.push(item);
            


        });

        //console.log("creato array target");
        //console.log(GlobalVariables.deviceUUID);
        //console.log(usersAlbumScope.slide_list);


        //console.log(usersAlbumScope.imgLike);

        /*
        var elementi = document.getElementsByClassName("immagine");
        console.log("selezionati gli elementi target");
        console.log(elementi);
        console.log(elementi.length);
        
        for (var i = 0; i < elementi.length; i++) {
          //trovo la dimensione dell'immagine
          console.log(elementi[i].style.backgroundImage);

        }
*/

         

    });


    usersAlbumScope.$on('modal.shown', function() {
      console.log('Modal is shown!');

        var elementi = document.getElementsByClassName("immagine");
        //console.log("selezionati gli elementi target");
        //console.log(elementi);
        //console.log(elementi.length);
        
        for (var i = 0; i < elementi.length; i++) {
          //trovo la dimensione dell'immagine
          //console.log(elementi[i].style.backgroundImage);
/*
          var img = new Image();
          img.src = item.css('background-image').replace(/url\(|\)$|"/ig, '');
            return img.width + ' ' + img.height;
*/
          //elementi[i].style.backgroundColor = "red";
        }
    });

  };

  self.aggiornaLikes = function(myScope) {

    //console.log(self);

    var likes = GlobalVariables.application.currentProgetto.moments_likes;

    _.each(myScope.slide_list, function(item) {
        //item.url = GlobalVariables.baseUrl + "/" + item.url;

        //per ogni slide (immagine) devo vedere quanti like ci sono stati, e se uno di questi è stato scelto dall'utente corrente
        item.likes = new Array(0,0,0,0,0,0,0,0,0,0);
        var totalLike=0;
        if (item.id!=null)
        {
          //console.log("cerco i likes per l'immagine " + item.id);
          _.each(likes, function(like) {
              
              if (like.idImmagine==item.id)
              {
                if (like.like!=null)
                {
                  item.likes[like.like]++;
                  totalLike++;
                  if (like.deviceId==GlobalVariables.deviceUUID) item.LikeImg=myScope.imgLike[like.like];
                }

              }

          });
          item.totalLikes=totalLike;
        }

        //per ogni slide (immagine) devo vedere quanti clap ci sono stati
        var totalClap=0;
        if (item.id!=null)
        {
          //console.log("cerco i clap per l'immagine " + item.id);
          _.each(likes, function(like) {
              
              if (like.idImmagine==item.id)
              {
                totalClap=totalClap+parseInt(like.clap);
              }

          });
          item.totalClap=totalClap;
        }



    });

  }



  self.showVideo = function(config) {

      var usersVideoScope = $rootScope.$new(true);

      usersVideoScope.closeVideoWnd = function() {
          usersVideoScope.usersGuideWnd.remove();
          usersVideoScope.$destroy();
          delete usersVideoScope;
      };



      usersVideoScope.options = {
          loop: false,
          effect: 'slide',
          speed: 500,
      }

      usersVideoScope.usersGuideWnd = $ionicModal.fromTemplate(

        '<ion-modal-view id="video" padding="true" cache-view="false" style="background-color:rgba(0,0,0,0.8); width:100%; height:100%; left:0px; right:0px; top:0px;">' +
              '<ion-content class="videoPage" scroll="false" padding="false" style="">' +

                  //'<a ng-click="closeVideoWnd()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated"></a>' +
                  '<span>' + 
                  '<div id="videoContainer" style="width:100%;"></div>'+
                  //'<video id="videoElement" controls style="width:100%"></video>' +
                  '</span>' +
                  
                  '<a ng-click="closeVideoWnd()" style="position:absolute; top: 5px; right: 5px; z-index:99999;color: white;opacity:0.8;" class="button button-icon icon ion-ios-close activated"></a>' +
             '</ion-content>' +
          '</ion-modal-view>', {
              scope: usersVideoScope,
              animation :'none',
              hardwareBackButtonClose: false
          }

    );



    usersVideoScope.usersGuideWnd.show().then(function() {
      //cerco l'id tra gli hs
        console.log("Show Video");
        var myConfig= JSON.parse(config);
        //console.log(myConfig);

        //YT o normale?
        if (myConfig.type=="YT")
        {
          
          var htmlToAdd = "<iframe id='playerYT' type='text/html' style='padding-top:0px;padding-bottom:0px;' ";
          htmlToAdd += "src='http://www.youtube.com/embed/"+decodeURIComponent(myConfig.path)+"?enablejsapi=1&controls=2&autoplay=1&modestbranding=1&rel=0&showinfo=0&origin=http://livengine.mediasoftonline.com' frameborder='0' allowFullScreen='allowFullScreen'></iframe>";
          div = document.getElementById( 'videoContainer' );
          div.insertAdjacentHTML( 'beforeend', htmlToAdd );


          iframe = document.getElementById( 'playerYT' );

          iframe.webkitRequestFullScreen();
         

        }
        else
        {
          var htmlToAdd = "<video id='videoElement' controls style='width:100%'></video>";
          div = document.getElementById( 'videoContainer' );
          div.insertAdjacentHTML( 'beforeend', htmlToAdd );


          var videoElement = document.getElementById('videoElement');    
          //console.log(videoElement);
          var source = document.createElement('source');

          var path="";
          if (myConfig.type=="LOCAL") path=GlobalVariables.baseUrl + "/" + decodeURIComponent(myConfig.path);
          else path=decodeURIComponent(myConfig.path);     

          source.setAttribute('src', path);

          videoElement.appendChild(source);
          videoElement.play();

          videoElement.webkitRequestFullScreen();
        }






    });



  };


  self.showSlideshow = function(config) {

      var usersSlideshowScope = $rootScope.$new(true);

      usersSlideshowScope.closeVideoWnd = function() {
          usersSlideshowScope.usersGuideWnd.remove();
          usersSlideshowScope.$destroy();
          delete usersSlideshowScope;
      };



      usersSlideshowScope.slide_list = [];

      usersSlideshowScope.options = {
          loop: false,
          effect: 'slide',
          speed: 500,
      }


      usersSlideshowScope.usersGuideWnd = $ionicModal.fromTemplate(

        '<ion-modal-view id="slideshow" padding="true" cache-view="false" style="background-color:rgba(255,255,255,0.8); width:100%; height:100%; left:0px; right:0px; top:0px;">' +
              '<ion-content class="videoPage" scroll="false" padding="false" style="">' +

                  '<ion-slides options="options" slider="data.slider">' +
                      '<ion-slide-page ng-repeat="current_slide in slide_list">' +
                          '<ion-slide-page>'+
                            '<div id="guide_{{$index}}" class="box" ion-img-cache-bg style="background-repeat: no-repeat; background-position: center center; background-size:contain; background-image:url({{current_slide.url}}); height:100%;"></div>'+
                          '</ion-slide-page>' +
                      '</ion-slide-page>' +
                  '</ion-slides>' +

                  '<a ng-click="closeVideoWnd()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated"></a>' +
             '</ion-content>' +
          '</ion-modal-view>', {
              scope: usersSlideshowScope,
              animation :'none',
              hardwareBackButtonClose: false
          }

    );



    usersSlideshowScope.usersGuideWnd.show().then(function() {
      //cerco l'id tra gli hs
        console.log("Show Slideshow");
        var myConfig= JSON.parse(config);
        //console.log(myConfig);

        for (var i=0;i<myConfig.length;i++)
        {
          var item= new Object();
          if (myConfig[i].type=="LOCAL") item.url=GlobalVariables.baseUrl + "/" + myConfig[i].path;
          else item.url=decodeURIComponent(myConfig[i].path);     

          usersSlideshowScope.slide_list.push(item);
        }

        //console.log(usersSlideshowScope.slide_list);


    });



  };


  self.showText = function(config) {

      var usersTextScope = $rootScope.$new(true);

      usersTextScope.closeVideoWnd = function() {
          usersTextScope.usersGuideWnd.remove();
          usersTextScope.$destroy();
          delete usersTextScope;
      };

      usersTextScope.trustAsHtml = function(string) {
        return $sce.trustAsHtml(string);
      };

      usersTextScope.htmlComments = ""; 
      
      usersTextScope.options = {
          loop: false,
          effect: 'slide',
          speed: 500,
      }


      usersTextScope.usersGuideWnd = $ionicModal.fromTemplate(

        '<ion-modal-view id="Text" padding="true" cache-view="false" style="background-color:rgba(255,255,255,0.9); width:100%; height:100%; left:0px; right:0px; top:0px;">' +
              '<ion-content class="videoPage" scroll="false" padding="false" style="">' +

                  '<div class="TextContainer" style="color:#000000;text-align:left;margin-left:5%;width:90%;border:1px solid black;overflow-y: auto;text-align:left;background-color: #ffffff;padding: 2%;" ng-bind-html="trustAsHtml(htmlText)"></div>' +

                  '<a ng-click="closeVideoWnd()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated"></a>' +
             '</ion-content>' +
          '</ion-modal-view>', {
              scope: usersTextScope,
              animation :'none',
              hardwareBackButtonClose: false
          }

    );



    usersTextScope.usersGuideWnd.show().then(function() {
      //cerco l'id tra gli hs
        console.log("Show Text");
        var myConfig= JSON.parse(config);
        //console.log(myConfig);

        usersTextScope.htmlText=myConfig.text;


    });



  };  


  self.showComments = function(targetId) {

      var usersCommentsScope = $rootScope.$new(true);

      usersCommentsScope.closeCommentsWnd = function() {
          usersCommentsScope.usersCommentsWnd.remove();
          usersCommentsScope.$destroy();
          delete usersCommentsScope;
      };

      usersCommentsScope.comment_data = {
        tmp_nickname: LazyLoader.appConfiguration.commentsNickname,
        tmp_comment: "",
        show_warn_nickname: false,
        show_warn_comment: false,
        show_warn_nickname_comment: false
      };

      usersCommentsScope.htmlComments = ""; //parte html dove verranno messi i commenti
      usersCommentsScope.comments = ""; //parte html dove verranno messi i commenti
      usersCommentsScope.step = 0; //SHOW_COMMENTS;
      usersCommentsScope.titolo = "Ecco i commenti";       

      usersCommentsScope.trustAsHtml = function(string) {
        return $sce.trustAsHtml(string);
      };

      usersCommentsScope.checkDisclaimer = function() {
        //verifico se l'utente deve accettare il disclaimer
        if (LazyLoader.appConfiguration.showDisclaimerForComments==true)
        {
          usersCommentsScope.titolo="Accettare il Disclaimer";
          usersCommentsScope.step=1; //SHOW_DISCLAIMER
          //devo caricare il disclaimer

          SupportServices.getDisclaimer({
              onComplete: function(testo) {

                  $timeout(function() {
                      usersCommentsScope.htmlComments=testo.disclaimer;
                  });
              }
          });  


        }
        else
        {
          usersCommentsScope.titolo=LanguageService.getLabel('SOCIAL_COMMENTA_INSERISCI');
          usersCommentsScope.htmlComments="scrivo";
          usersCommentsScope.step=2; //INSERT_COMMENT
        }
      };

      usersCommentsScope.annulla = function() {
        //non ha accettato il disclaimer, ritorno alla lista commenti
        usersCommentsScope.titolo="Ecco i commenti";
        usersCommentsScope.htmlComments=usersCommentsScope.comments;
        usersCommentsScope.step=0; //SHOW_COMMENTS;

      };

      usersCommentsScope.scrivi = function() {
        //ha accettato il disclaimer
        //salvo la scelta
        LazyLoader.appConfiguration.showDisclaimerForComments=false;
        LazyLoader.saveConfiguration({
          onComplete: function(err) {
            usersCommentsScope.titolo=LanguageService.getLabel('SOCIAL_COMMENTA_INSERISCI');
            usersCommentsScope.step=2; //INSERT_COMMENT
          }
        });


      };

      usersCommentsScope.annullaCommento = function()
      {
        //ritorno ai commenti  
        usersCommentsScope.comment_data.tmp_comment="";

        usersCommentsScope.titolo="Ecco i commenti";
        usersCommentsScope.htmlComments=usersCommentsScope.comments;
        usersCommentsScope.step=0; //SHOW_COMMENTS;        
      };      

      usersCommentsScope.inviaCommento = function() {
          //controllo il nickname e il commento
          
          //tolgo i ritorni a capo
          var commento=usersCommentsScope.comment_data.tmp_comment;
          //console.log(usersCommentsScope.comment_data.tmp_comment);
          usersCommentsScope.comment_data.tmp_comment = usersCommentsScope.comment_data.tmp_comment.replace(/(\r\n|\n|\r)/gm," ");
          //console.log(usersCommentsScope.comment_data.tmp_comment);

          var reg = /[^-A-Za-z0-9 .,!?&:_@$^;|<>"'=+*°#()èéòàùìÈÉÀÁÌÍÒÓÙÚ]/;
          var testNick = reg.test(usersCommentsScope.comment_data.tmp_nickname);    
          var testCommento = reg.test(usersCommentsScope.comment_data.tmp_comment);    
          
          usersCommentsScope.comment_data.show_warn_nickname_comment=false;
          usersCommentsScope.comment_data.show_warn_nickname=false;
          usersCommentsScope.comment_data.show_warn_comment=false;

          if ((testNick==true) && (testCommento==true))
          {
              console.log("caratteri non ammessi nel nickname e nel commento");
              usersCommentsScope.comment_data.show_warn_nickname_comment=true;
              setTimeout(function() {
                  usersCommentsScope.comment_data.show_warn_nickname_comment=false;
              }, 4000);       
            return;
          }        
          if (testNick==true)
          {
              console.log("caratteri non ammessi nel nickname");
              usersCommentsScope.comment_data.show_warn_nickname=true;
              setTimeout(function() {
                  usersCommentsScope.comment_data.show_warn_nickname=false;
              }, 4000);       
            return;
          }        
          if (testCommento==true)
          {
              console.log("caratteri non ammessi nel commento");
              usersCommentsScope.comment_data.show_warn_comment=true;
              setTimeout(function() {
                  usersCommentsScope.comment_data.show_warn_comment=false;
              }, 4000);       
            return;
          } 
          
          //salvo il nickname
          LazyLoader.appConfiguration.commentsNickname=usersCommentsScope.comment_data.tmp_nickname;
          LazyLoader.saveConfiguration({
            onComplete: function(err) {}
          });

          

          //scrivo il commento e aggiorno la lista dei commenti
          SupportServices.setCommento({
              idImmagine: targetId,
              commento: usersCommentsScope.comment_data.tmp_comment,
              nickname: usersCommentsScope.comment_data.tmp_nickname,
              onComplete: function(err, json) {

                  //console.log("on complete: " + err + " - " + JSON.stringify(json));
                  if(err) {



                  } else {

                      $timeout(function() {
                        usersCommentsScope.comments="";

                        if (json.errore)
                        {

                        }

                        var commenti=json.commenti;

                        usersCommentsScope.htmlComments="";
                        if (commenti.length==0)
                        {
                          
                          usersCommentsScope.comments="Nessun commento presente...";
                        }
                        else
                        {
                          for (var i=0;i<commenti.length;i++)
                          {
                            usersCommentsScope.comments+="<div style='color:#000000;margin-top:10px;margin-left:10px;'><b>"+commenti[i].nickname+"</b> - <i>"+commenti[i].data+"</i>: "+commenti[i].commento+"</div>";
                          }
                        }
                        usersCommentsScope.htmlComments=usersCommentsScope.comments;
                        //ritorno ai commenti
                        usersCommentsScope.comment_data.tmp_comment="";

                        usersCommentsScope.titolo="Ecco i commenti";
                        usersCommentsScope.htmlComments=usersCommentsScope.comments;
                        usersCommentsScope.step=0; //SHOW_COMMENTS; 



                      });

                  }

              }

          });



          /*
          var return_oggetto = new Object();
          return_oggetto.type = "SEND";
          return_oggetto.nick = $rootScope.comment_data.tmp_nickname;
          return_oggetto.commento = $rootScope.comment_data.tmp_comment;

          $rootScope.popupScreen.remove();
          //alert($rootScope.comment_data.tmp_nickname);
          //alert($rootScope.comment_data.tmp_comment);

          var jsonString = JSON.stringify(return_oggetto);
          //tolgo eventuali apici, sostituendoli...
          jsonString = jsonString.replace(/'/g, "|");   

          var encodedJson = encodeURIComponent(jsonString);
          var funzione = "commentReturn('" + encodedJson + "')";
          //console.log("funzione: " + funzione);

          GlobalVariables.wikitudePlugin.show();
          GlobalVariables.wikitudePlugin.callJavaScript(funzione);
          */
      };


      usersCommentsScope.options = {
          loop: false,
          effect: 'slide',
          speed: 500,
      }

      usersCommentsScope.usersCommentsWnd = $ionicModal.fromTemplate(

        '<ion-modal-view id="commentsList" padding="true" cache-view="false" style="background-color:rgba(255,255,255,0.98); width:100%; height:100%; left:0px; right:0px; top:0px;">' +
              '<ion-content id="ioncontentcommenti" scroll="true" padding="false" style="bottom:13%;">' +
              
              //step SHOW_COMMENTS
              '<p style="text-align: center;margin-top: 5%;">{{titolo}}</p>' +

              '<div ng-if="step != 2" class="commentsContainer" style="color:#000000;text-align:left;margin-left:5%;width:90%;border:0px solid black;overflow-y: auto;text-align:left;" ng-bind-html="trustAsHtml(htmlComments)"></div>' +

              '<div ng-if="step == 2">' + 
                (LazyLoader.appConfiguration.commentsNickname == "" ? '<p style="color:#000000;text-align:left;margin-left:5%;">'+LanguageService.getLabel('SOCIAL_COMMENTA_NICKNAME')+'</p>' : '') +
                '<p style="color:#000000;text-align:left;margin-left:5%;">'+LanguageService.getLabel('NICKNAME')+'</p>' + 
                '<input type="text" ng-model="comment_data.tmp_nickname" id="comment_nickname" maxlength="20" style="border:1px solid;color:#000000;text-align:left;margin-left:5%;width:50%;" placeholder="" value="'+LazyLoader.appConfiguration.commentsNickname+'">' + 
                '<p style="color:#000000;text-align:left;margin-left:5%;">'+LanguageService.getLabel('COMMENTO')+'</p>' + 
                '<textarea rows="10" ng-model="comment_data.tmp_comment" style="color:#000000;text-align:left;margin-left:5%;width:90%;border:1px solid black;" placeholder="" value=""></textarea>' +

                 '<div ng-show="comment_data.show_warn_nickname" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>'+LanguageService.getLabel('SOCIAL_COMMENTO_ERRORE_NICK')+'</p></div>' +
                  '<div ng-show="comment_data.show_warn_comment" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>'+LanguageService.getLabel('SOCIAL_COMMENTO_ERRORE_COMMENTO')+'</p></div>' +
                '<div ng-show="comment_data.show_warn_nickname_comment" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>'+LanguageService.getLabel('SOCIAL_COMMENTO_ERRORE_COMMENTO_NICK')+'</p></div>' +

             '</div>' +

            
             '</ion-content>' +

              '<div class="row" style="position:absolute; bottom:5px; padding:0px; left:5px; right:5px; width:auto;background-color: #ffffff;">' +

              //step SHOW_COMMENTS
             '<div ng-if="step == 0" class="col" style="padding: 0px 2.5px 0px 0px;">' +
               '<button ng-click="checkDisclaimer()" class="button button-full button-balanced" style="margin:0px;">SCRIVI</button>' +
             '</div>' +

              '<div ng-if="step == 0" class="col" style="padding: 0px 0px 0px 2.5px;">' +
                  '<button ng-click="closeCommentsWnd()" class="button button-full button-stable" style="margin:0px;">CHIUDI</button>' +
             '</div>' +

             // step SHOW_DISCLAIMER
             '<div ng-if="step == 1" class="col" style="padding: 0px 2.5px 0px 0px;">' +
               '<button ng-click="scrivi()" class="button button-full button-balanced" style="margin:0px;">ACCETTA</button>' +
             '</div>' +

             '<div ng-if="step == 1" class="col" style="padding: 0px 0px 0px 2.5px;">' +
               '<button ng-click="annulla()" class="button button-full button-stable" style="margin:0px;">ANNULLA</button>' +
             '</div>' +

             // step INSERT_COMMENT
             '<div ng-if="step == 2" class="col" style="padding: 0px 2.5px 0px 0px;">' +
               '<button ng-click="inviaCommento()" class="button button-full button-balanced" style="margin:0px;">'+LanguageService.getLabel('INVIA')+'</button>' +
             '</div>' +

              '<div ng-if="step == 2" class="col" style="padding: 0px 0px 0px 2.5px;">' +
                  '<button ng-click="annullaCommento()" class="button button-full button-stable" style="margin:0px;">'+LanguageService.getLabel('ANNULLA')+'</button>' +
             '</div>' +


              '</div>' +


          '</ion-modal-view>', {
              scope: usersCommentsScope,
              animation :'none',
              hardwareBackButtonClose: false
          }

    );



    usersCommentsScope.usersCommentsWnd.show().then(function() {
      //recuper i commenti per quell'immagine
        SupportServices.getComments({
            idImmagine: targetId,
            onComplete: function(err, json) {

                //console.log("on complete: " + err + " - " + JSON.stringify(json));
                if(err) {



                } else {

                    $timeout(function() {
                      if (json.errore)
                      {

                      }

                      var commenti=json.commenti;

                      usersCommentsScope.htmlComments="";
                      if (commenti.length==0)
                      {
                        
                        usersCommentsScope.comments="Nessun commento presente...";
                      }
                      else
                      {
                        for (var i=0;i<commenti.length;i++)
                        {
                          usersCommentsScope.comments+="<div style='color:#000000;margin-top:10px;margin-left:10px;'><b>"+commenti[i].nickname+"</b> - <i>"+commenti[i].data+"</i>: "+commenti[i].commento+"</div>";
                        }
                      }
                      usersCommentsScope.htmlComments=usersCommentsScope.comments;





                    });

                }

            }

        });      

      //cerco l'id tra gli hs
        /*
        console.log("Show Video");
        var myConfig= JSON.parse(config);
        console.log(myConfig);

        var videoElement = document.getElementById('videoElement');    
        console.log(videoElement);
        var source = document.createElement('source');

        source.setAttribute('src', decodeURIComponent(myConfig.path));

        videoElement.appendChild(source);
        videoElement.play();

        videoElement.webkitRequestFullScreen();
*/


    });



  };




})


