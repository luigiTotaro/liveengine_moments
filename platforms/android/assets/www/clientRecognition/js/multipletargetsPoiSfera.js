
var World = {
	loaded: false,
	rotating: false,
	drawables: [],

	init: function initFn() {
		this.loaded=false;
		//AR.context.destroyAll();
        this.createDrawables();
		//this.createOverlays();
	},


    createDrawables: function createDrawablesFn() {

		//PoiRadar.show();
		//$('#radarContainer').unbind('click');
		//$("#radarContainer").click(PoiRadar.clickedRadar);

		//deve essere una funzione ricorsiva, perchè non so quanto ci metto a caricare le immagini

		console.log("pronti");
		console.log(oggetto);

		World.carica(0);
    },

    carica: function caricaFn(indice) {

		console.log("Carica: " + indice);
		
		
		//l'indice mi dice a quanti gradi, su 360, mettere l'immagine
		var gradi=0
		var altezza=0;
		if (indice<configurazione.qtaRigaCentrale)
		{
			//riga centrale
			gradi=indice*(360/configurazione.qtaRigaCentrale);
		
		}
		else if (indice<(configurazione.qtaRigaCentrale+configurazione.qtaPrimaRiga))
		{
			//prima riga in alto
			gradi=(indice-configurazione.qtaRigaCentrale)*(360/configurazione.qtaPrimaRiga);
			altezza=configurazione.altPrimaRiga;
		}
		else if (indice<(configurazione.qtaRigaCentrale+(configurazione.qtaPrimaRiga*2)))
		{
			//prima riga in basso
			gradi=(indice-configurazione.qtaRigaCentrale-configurazione.qtaPrimaRiga)*(360/configurazione.qtaPrimaRiga);
			altezza=-(configurazione.altPrimaRiga);
		}
		else if (indice<(configurazione.qtaRigaCentrale+(configurazione.qtaPrimaRiga*2)+configurazione.qtaSecondaRiga))
		{
			//seconda riga in alto
			gradi=(indice-configurazione.qtaRigaCentrale-(configurazione.qtaPrimaRiga*2))*(360/configurazione.qtaSecondaRiga);
			altezza=configurazione.altSecondaRiga;
		}
		else if (indice<(configurazione.qtaRigaCentrale+(configurazione.qtaPrimaRiga*2)+(configurazione.qtaSecondaRiga*2)))
		{
			//seconda riga in basso
			gradi=(indice-configurazione.qtaRigaCentrale-(configurazione.qtaPrimaRiga*2)-configurazione.qtaPrimaRiga)*(360/configurazione.qtaSecondaRiga);
			altezza=-(configurazione.altSecondaRiga);
		}



/*
		var gradi=0
		var altezza=0;
		if (indice<10) gradi=indice*36;
		else if (indice<18)
		{
			gradi=(indice-10)*45;
			altezza=1.5;
		}
		else
		{
			gradi=(indice-18)*45;
			altezza=-1.5;
		}
*/		
		//da questi gradi ricavo nord e est, o y e x nel sistema cartesiano
		var east=configurazione.offset*(Math.cos(gradi * Math.PI / 180));
		var north=configurazione.offset*(Math.sin(gradi * Math.PI / 180));


		//var sdu=Number(oggetto[indice].sdu);
		var sdu=configurazione.sdu;

		var markerLocation = new AR.RelativeLocation(null, north, east, altezza);
		
		console.log(oggetto[indice] + " @ " + north + " - " + east + " - H:" + sdu);
		//console.log("markerLocation");
		//console.log(JSON.stringify(markerLocation));

		// create an AR.ImageDrawable for the marker in idle state
		var imgTemp = new AR.ImageResource(oggetto[indice],
		{
			onLoaded: function(width, height)
			{
			  	console.log('Resource loaded! L:' + width + " - H:" + height);
				if (width>height)
				{
					//portrait, riporto l'sdu in base alla larghezza
					sdu=(sdu*height)/width;
				}

				var overlay = new AR.ImageDrawable(imgTemp, sdu,
				{
					enabled: true,
					opacity : 1.0,
					zOrder: 10
				});
				overlay.onClick = World.showImage(oggetto[indice]);
				//console.log("overlay");
				//console.log(JSON.stringify(overlay));

				var markerObject = new AR.GeoObject(markerLocation, {
				    drawables: {
				        cam: [overlay]
				    }
				});
				indice++;
				var perc=(indice/oggetto.length)*100;
				$("#barraCaricamentoAttuale").css("width", perc+"%");
				$("#testoCaricamento").html("Caricamento immagini: "+parseInt(perc)+"%");

				if (indice<oggetto.length)
				{
					World.carica(indice);
				}
				else
				{
					console.log("caricate tutte le immagini");
					$("#barraCaricamentoAttuale").hide();
					$("#testoCaricamento").hide();
					$("#barraCaricamentoSfondo").hide();
				}

			},
			onError: function()
			{
			  console.log('An error occurred loading the resource!');
			}
		});


		//console.log(JSON.stringify(imgTemp));
		

		//console.log("markerObject");
		//console.log(JSON.stringify(markerObject));


    },

	showImage: function (url)
	{
        return function() {
			//alert(url);
	        showImage(url);
        };
	},



	worldLoaded: function worldLoadedFn() {
		console.log("worldLoaded");
		//startAnimationBar(RA_COPERTINA);
	},



};


var logEnabled= false;
var language="it";

var	oggetto = new Array();
var contenutiPath="";
var idProgetto="";
var arrayDynamicClasses=[];

if (logEnabled==true)
{
	AR.logger.activateDebugMode();
}

console.log("sono qui dentro");

function passData(localPath, idPrj, baseUrl)
{
	console.log("qui");

	console.log("v 1.0 - passData: "+localPath+" - "+idPrj+ " - " + baseUrl);
	contenutiPath = localPath + "/";
	contenutiUrl = baseUrl;
	idProgetto = idPrj;
	
	loadJson(contenutiPath+"dettaglio_fotosfera_"+idProgetto+".json");


}

var numeroUtenti;
var	oggetto = new Array();
var configurazione = new Object();
var	arrayOverlay = new Array();

var labelsObj;
var contenutiUrl;
var stopBarAnim=false;
var idCopertinaRilevata=-1;
var oggettoAttesaConnessione;


$( window ).resize(function() {
	resizeBasedOnResolution(RA_POI);
});


function poiLoaded()
{
/*
	alert("finito il caricamento dei poi");
	//alert("finito il caricamento dei poi");
	if (updateLabel.length>0) aggiornaArrayLabel(0);

	// e faccio partire anche un timer che me le aggiorna
*/
}

function showImage(url)
{
	$("#imageFotosfera").show();
	$("#imageFotosfera").css("background-image","url("+url+")");
}

function loadJson(myUrl)
{
	console.log("loadJson: " + myUrl);

	oggetto = new Array();
	//console.log("carico il file: " + myUrl);
	$.ajax({
		url: myUrl,
		dataType: 'text'
		//dataType: 'json'
		})
		.done(function( jsonCrypt )
		{

			//devo decriptarlo
			var jsonStr=decode(jsonCrypt);
			var json=JSON.parse(jsonStr);

			//resizeBasedOnResolution(RA_GENERALE);
			configurazione.qtaRigaCentrale = parseInt(json.immagini.length*json.qtaRigaCentrale);
			configurazione.qtaPrimaRiga = parseInt(json.immagini.length*json.qtaPrimaRiga);
			configurazione.qtaSecondaRiga = parseInt(json.immagini.length*json.qtaSecondaRiga);
			configurazione.altPrimaRiga = Number(json.altPrimaRiga);
			configurazione.altSecondaRiga = Number(json.altSecondaRiga);
			configurazione.offset = Number(json.offset);
			configurazione.sdu = Number(json.sdu);
			console.log(JSON.stringify(configurazione));

			for (var i=0;i<json.immagini.length;i++)
			{
				if (json.immagini[i].type=="LOCAL") oggetto.push(contenutiUrl + "/" + decodeURIComponent(json.immagini[i].path));
				else oggetto.push(decodeURIComponent(json.immagini[i].path));
			}
			console.log(oggetto);

			resizeBasedOnResolution(RA_POI);

			World.init();


		})
		.fail(function( jqXHR, textStatus ) {
	  		console.log( "Request failed: " + textStatus );
		});



}


function closeImage()
{
	$("#imageFotosfera").hide();
	$("#imageFotosfera").css("background-image","");
}

$(document).ready(function()
{
//$("#loader").show();

	setTimeout(function() {
		resizeBasedOnResolution(RA_POI);
	}, 100);	

	setTimeout(function() {
		resizeBasedOnResolution(RA_POI);
	}, 1000);

	
/*
	$("#barraCaricamentoSfondo").hide();
	$("#imageFotosfera").show();
	$("#imageFotosfera").css("background-image","url(http://livengine.mediasoftonline.com/momentsStuff/slide1.jpg)");
*/


	/*
	var indice=0;
	var totale=10;

	setInterval(function() {
		indice++;
		var perc=(indice/totale)*100;
		$("#barraCaricamentoAttuale").css("width", perc+"%");
		$("#testoCaricamento").html("Caricamento immagini: "+parseInt(perc)+"%");

	}, 1000);
	*/
});
	
