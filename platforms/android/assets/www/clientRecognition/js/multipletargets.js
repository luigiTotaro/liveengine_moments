var World = {
	loaded: false,
	rotating: false,


	init: function initFn() {
		this.loaded=false;
		//AR.context.destroyAll();
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {

		var self = this;
		this.tcr = new AR.TargetCollectionResource(contenutiPath + oggetto.wtc);
		
		this.tracker = new AR.ImageTracker(this.tcr, {
			maximumNumberOfConcurrentlyTrackableTargets: numeroMultiTarget,
			extendedRangeRecognition: AR.CONST.IMAGE_RECOGNITION_RANGE_EXTENSION.OFF,
			trackerEfficiencyMode: AR.CONST.TRACKER_EFFICIENCY_MODE.ENERGY_EFFICIENCY,
			onTargetsLoaded: this.worldLoaded
		});


		arrayOverlay = [];
		arrayImages2Load = [];
		arrayImageResourceLike = [];

		//creo l'oggetto che contiene tutti gli hotspot per questa rilevazione
		var overlayObject = new Hotspot(oggetto,AR,this);
		overlayObject.create(null);


		arrayOverlay=overlayObject.getArrayOverlay();
		arrayImages2Load=overlayObject.getArrayImages2Load();
		arrayImageResourceLike=overlayObject.getArrayImageResourceLike();

		//var trackable = createTrackable(this.tracker,arrayOverlay);
		var trackable = new AR.ImageTrackable(this.tracker, "*", {
			onImageRecognized: function (a)
	        {
				console.log("ho rilevato " + a);
				//ciclo su tutti gli overlay, e aggiungo solo quelli che gli appartengono
				for (var i=0;i<arrayOverlay.length;i++)
				{
					if (arrayOverlay[i].targetRef==a) this.addImageTargetCamDrawables(a, arrayOverlay[i]);
				}
				scegliOverlay(a);
				$("#messaggioIniziale").hide(); //per sicurezza
				console.log("Pagine riconosciute: " + JSON.stringify(pagineRiconosciute));
			},
			onImageLost: function onExitFieldOfVisionFn(a)
	        {
				console.log("ho perso la rilevazione di " + a);
				paginaPersa(a);
				//ciclo su tutti gli overlay, e rimuovo solo quelli che gli appartengono
				for (var i=0;i<arrayOverlay.length;i++)
				{
					if (arrayOverlay[i].targetRef==a)
					{
						//potrebbe essere un video overlay da stoppare
						console.log("video overlay:");
						if (typeof arrayOverlay[i].isOverlayVideo != 'undefined')
						{
							console.log("esiste isOverlayVideo");
							console.log(arrayOverlay[i].isOverlayVideo);
							if (arrayOverlay[i].isOverlayVideo==true)
							{
								console.log("ed è anche true");
								arrayOverlay[i].stop();					
							}
						}						


						this.removeImageTargetCamDrawables(a, arrayOverlay[i]);
						arrayOverlay[i].enabled=false;
					}
				}
				if ($("#momentsIconsDiv").is(":visible"))
				{
					$("#momentsIconsDiv").css('display', 'none');
				}
				console.log("Pagine riconosciute: " + JSON.stringify(pagineRiconosciute));
				if (pagineRiconosciute.length==0) startAnimationBar(RA_GENERALE);
			},
		});

		$("#backButton").show();
		console.log("ImageTrackable Pronto");
		document.getElementById('loadingMessage').innerHTML = getLabel("INQUADRA_PAGINA","Inquadra una pagina.....");
		startAnimationBar(RA_GENERALE);

	},

	mostraContenuti: function (indiceImmagine, indiceElemento)
	{
        return function() {
			//console.log("sono in mostraContenuti, con indiceImmagine: "+indiceImmagine + " e indiceElemento: " + indiceElemento);
			//console.log("dati: " +JSON.stringify(oggetto.target[indiceImmagine].punti[indiceElemento]));
			showContenuto(indiceImmagine, indiceElemento);
        };
	},

	showHideLikeIcons: function (idImmagine)
	{
        return function() {
			toggleLikeIcons(idImmagine);
        };
	},

	clap: function (idImmagine)
	{
        return function() {
			addClap(idImmagine);
        };
	},

	comment: function (idImmagine)
	{
        return function() {
			mostraCommenti(idImmagine);
        };
	},

	worldLoaded: function worldLoadedFn() {
		console.log("worldLoaded");
		document.getElementById('loadingMessage').innerHTML = getLabel("INQUADRA_PAGINA","Inquadra una pagina.....");

		
		/*

		$("#loader").show(); //poi non serve , sta messo di default su showContenutoExecute
		var oggettoPano = new Object();
		//oggettoPano.path="contenuti/1234_1_1234567890.jpg";
		//oggettoPano.type="";
		
		oggettoPano.path="http://thiago.me/image-360/Polie_Academy_53.JPG";
		oggettoPano.type="LINK";

		oggettoPano.connectionPresent=true;
		oggettoPano.preName="3210_2_1_";

		contentManager.setPano(oggettoPano); 
*/

	}


};

var pagineRiconosciute=new Array(); //per il multitarget mi dice quando sono a 0, in modo da rimettere la barra
var timerHeader=null;
var indiceHeaderPaginaMostrata=0; //indice all'interno dell'array che mi dice cosa sta mostrando e cosa mostrerà

var baseUrl = "http://192.170.5.25:8888/liveEngine_webapp";
var contenutiPath="";
var dynClassesPath="";
var idProgetto="";
var isApiRest = false;
var contenutiUrl = "";

var	oggetto = new Array();
var	arrayOverlay = new Array();
var	arrayImages2Load = new Array();
var arrayImageResourceLike = new Array();

var aspectRationPlayerYT;
var deviceId="";
var swiper=null;
var oggettoAttesaConnessione;
var fullscreenVideo=true;
var fullscreenSwiper=true;
var arrayImagesSwiper=null;

var rotationAnimation;

var dynamicPosClass;
var arrayDynamicClasses = new Array();
var actualClass=null;
var showCommentsDisclaimer;
var commentsNickname;
var stopBarAnim=false;
var contenutoScelto=false;
var numeroMultiTarget=2; //default;
var popupOnScreen=false;
var language="it";

var panoViewer;

var idImmagine4Moments=-1;

/*
function createTrackable(tracker,arrayOverlay)
{
	console.log("----createTrackable - " + tracker + " - " + arrayOverlay);

	var trackable = new AR.ImageTrackable(tracker, "*", {
		//drawables: {
		//	cam: arrayOverlay
		//},
		onImageRecognized: function (a)
        {
			console.log("----ho rilevato " + a);
			//paginaRilevata=true;

			//ciclo su tutti gli overlay, e aggiungo solo quelli che gli appartengono
			for (var i=0;i<arrayOverlay.length;i++)
			{
				if (arrayOverlay[i].targetRef==a)
				{
					this.addImageTargetCamDrawables(a, arrayOverlay[i]);
					//arrayOverlay[i].enabled=true;
				}
			}

			scegliOverlay(a);

			$("#messaggioIniziale").hide(); //per sicurezza
			console.log("Pagine riconosciute: " + JSON.stringify(pagineRiconosciute));

			//trackable.snapToScreen.enabled = false;
		},
		onImageLost: function onExitFieldOfVisionFn(a)
        {
			console.log("---ho perso la rilevazione di " + a);
			//paginaRilevata=false;
			//pagineRiconosciute--;
			paginaPersa(a);

			//ciclo su tutti gli overlay, e rimuovo solo quelli che gli appartengono
			for (var i=0;i<arrayOverlay.length;i++)
			{
				if (arrayOverlay[i].targetRef==a)
				{
					//console.log("considero l'oggetto: " + JSON.stringify(arrayOverlay[i]));
					this.removeImageTargetCamDrawables(a, arrayOverlay[i]);
					arrayOverlay[i].enabled=false;
				}
			}

			console.log("Pagine riconosciute: " + JSON.stringify(pagineRiconosciute));

			if (pagineRiconosciute.length==0)
			{
				//document.getElementById('loadingMessage').innerHTML = getLabel("INQUADRA_PAGINA","Inquadra una pagina.....");
				//disabilitatutto();
				//itemRilevato="";
				//trackable.snapToScreen.enabled = false;
				startAnimationBar(RA_GENERALE);
			}
		},
	});

	return trackable;

}

*/


console.log("sono dentro la RA");

//entry point da wikiservice....
function passData(localPath, localDynamicARPath, idPrj, apiRest, baseUrl, localDeviceId, loc_showCommentsDisclaimer, loc_commentsNickname, multitarget, systemLanguage)
{
	console.log("v 1.0 - passData: "+localPath+" - "+idPrj+ " - " + apiRest + " - " + baseUrl + " - " + localDeviceId + " - " + loc_showCommentsDisclaimer + " - " + loc_commentsNickname + " - " + multitarget + " - " + systemLanguage);
	//alert("v 1.0 - passData: "+localPath+" - "+idPrj+ " - " + apiRest + " - " + baseUrl + " - " + deviceId);

	console.log("RA Ver: " + AR.context.versionNumber);
	console.log("sensors.enabled: " + AR.hardware.sensors.enabled);

	contenutiPath = localPath + "/";
	dynClassesPath = localDynamicARPath + "/";
	idProgetto = idPrj;
	isApiRest = apiRest
	contenutiUrl = baseUrl;
	deviceId=localDeviceId
	showCommentsDisclaimer=loc_showCommentsDisclaimer;
	commentsNickname=loc_commentsNickname
	numeroMultiTarget=parseInt(multitarget) || 2;
	language=systemLanguage;

	resizeBasedOnResolution(RA_GENERALE);

	$("#messaggioIniziale").show();
	$("#barraScansione").show();
	
	AR.hardware.sensors.enabled=false;
	console.log("sensors.enabled now: " + AR.hardware.sensors.enabled);

	requestUsersNumber();
	setInterval(function() {
		requestUsersNumber();
	}, 10000);

	//AR.hardware.camera.zoom  = 1;
	//alert(JSON.stringify(AR.hardware.camera.features));

	//carico i likes, clap ecc, del progetto in questione
	getLikes(idPrj);

	//loadJsonLabels(localPath+"/configuration.json");
}

$( window ).resize(function() {
	resizeBasedOnResolution(RA_GENERALE);
});

function getLikesReturn(json)
{
	console.log("tornato da getLikes, con oggetto likes-comments:");
	console.log(json);
	console.log(JSON.stringify(json));
	oggetto = new Object();
	if (json==null)
	{
		oggetto.likes=null;
		oggetto.comments=null;		
	}
	else
	{
		oggetto.likes=json.moments_likes;
		oggetto.comments=json.moments_comments;		
	}
	console.log(JSON.stringify(oggetto));
	
	loadJsonLabels(contenutiPath+"configuration.json");

}


function loadJsonLabels(myUrl)
{
	//oggetto = new Object();
	//console.log("carico il file: " + myUrl);
	$.ajax({
		url: myUrl,
		dataType: 'text'
		//dataType: 'json'
		})
		.done(function( jsonCrypt )
		{
			console.log("file json caricato");
			//devo decriptarlo
			var jsonStr=decode(jsonCrypt);
			var json=JSON.parse(jsonStr);

			labelsObj=json.labels;
			//traduco le labels
			//$("#labelChiudi").html(getLabel("BTN_RITORNA","Ritorna"));
			$("#loadingMessage").html(getLabel("ATTENDERE","Attendere..."));
			$("#messaggiDownload p").html(getLabel("ATTENDERE_SCARICAMENTO_CONTENUTO","Attendere, scaricamento contenuto in corso"));
			$("#testoInquadraCopertina").html(getLabel("INQUADRA_PAGINA","Inquadra una pagina..."));
			
			loadJson(contenutiPath+"dettaglio_progetto_"+idProgetto+".json");
		})
		.fail(function( jqXHR, textStatus ) {
	  		console.log( "Request failed: " + textStatus );
		});
}

function loadJson(myUrl)
{
	//oggetto = new Object();
	//console.log("carico il file: " + myUrl);
	$.ajax({
		url: myUrl,
		dataType: 'text'
		//dataType: 'json'
		})
		.done(function( jsonCrypt )
		{

			//devo decriptarlo
			var jsonStr=decode(jsonCrypt);
			var json=JSON.parse(jsonStr);

			console.log("file json caricato");
			//layout
			console.log (JSON.stringify(json));
			//$( "#footerMsg" ).css("background-color",json.layout.coloreGenerale);
			//$( "#footerMsg" ).css("color",json.layout.coloreTesto);
			
			var col1=convertHex(json.layout.coloreGenerale,80);
			var col2=convertHex(json.layout.coloreGenerale,40);
			var col3=convertHex(json.layout.coloreGenerale,0);
			var col4=convertHex(json.layout.coloreGenerale,60);

			$( "#header" ).css("background","linear-gradient(to bottom, "+col1+", "+col2+"");
			$( "#subHeader" ).css("background","linear-gradient(to bottom, "+col2+", "+col3+"");
			$( "#footer" ).css("background","linear-gradient(to top, "+col4+", "+col3+"");

			//$( "#header" ).css("background-color",json.layout.coloreGenerale);
			$( "#header" ).css("color",json.layout.coloreTesto);
			$( "#loadingMessage" ).css("color",json.layout.coloreTesto);
			if (json.layout.testataNome=="") $( "#footerMsg" ).html(json.layout.nome + " AR App v1.0");
			else $( "#footerMsg" ).html(json.layout.testataNome + " - " + json.layout.nome + " AR App v1.0");
			//$( "#footer" ).show();
			$( "#textContainer" ).css("border","2px solid " + json.layout.coloreGenerale);
			$( "#videoContainer" ).css("border","2px solid " + json.layout.coloreGenerale);
			$( "#barraScansione" ).css("background-color",json.layout.coloreGenerale).css("-webkit-box-shadow","0px 0px 30px 2px "+json.layout.coloreGenerale).css("-moz-box-shadow","0px 0px 30px 2px "+json.layout.coloreGenerale).css("box-shadow","0px 0px 30px 2px "+json.layout.coloreGenerale);

			oggetto.coloreGenerale=json.layout.coloreGenerale;
			oggetto.gradimento=json.gradimento;
			oggetto.testataSocket=json.layout.testataSocket;

			//resizeBasedOnResolution(RA_GENERALE);

			var indice=0;
			//wtc
			var wtcPath=json.progetti[indice].wtcPath;
			wtcPathSplit=wtcPath.split("/");
			var wtcName=wtcPathSplit[wtcPathSplit.length-1];
			wtcName = wtcName.replace(".wtc", ".mac");

			oggetto.wtc=wtcPathSplit[wtcPathSplit.length-2]+"/"+wtcName;
			oggetto.idProgetto=json.progetti[indice].prjId;

			oggetto.target=json.progetti[indice].target;
			oggetto.moments_icons = json.moments_icons;
			// e creo l'array delle immagini da utilizzare
			oggetto.moments_icons.array=new Array();
			oggetto.moments_icons.array.push(contenutiUrl + "/" + oggetto.moments_icons.like);
			oggetto.moments_icons.array.push(contenutiUrl + "/" + oggetto.moments_icons.icona_1);
			oggetto.moments_icons.array.push(contenutiUrl + "/" + oggetto.moments_icons.icona_2);
			oggetto.moments_icons.array.push(contenutiUrl + "/" + oggetto.moments_icons.icona_3);
			oggetto.moments_icons.array.push(contenutiUrl + "/" + oggetto.moments_icons.icona_4);
			oggetto.moments_icons.array.push(contenutiUrl + "/" + oggetto.moments_icons.icona_5);
			oggetto.moments_icons.array.push(contenutiUrl + "/" + oggetto.moments_icons.icona_6);
			oggetto.moments_icons.array.push(contenutiUrl + "/" + oggetto.moments_icons.icona_7);



			// e creo anche la barra di icone in basso
			//aggiungo le icone di moments
			var html='';
			html += '<img id="momentsLikes_1" src="'+contenutiUrl + "/" + oggetto.moments_icons.icona_1 +'" class="momentsIcons" onclick="likeClick(1)"></img>';
			html += '<p id="momentsValues_1" style="text-align: center;" class="momentsValue">0</p>';
			html += '<img id="momentsLikes_2" src="'+contenutiUrl + "/" + oggetto.moments_icons.icona_2 +'" class="momentsIcons" onclick="likeClick(2)"></img>';
			html += '<p id="momentsValues_2" style="text-align: center;" class="momentsValue">0</p>';
			html += '<img id="momentsLikes_3" src="'+contenutiUrl + "/" + oggetto.moments_icons.icona_3 +'" class="momentsIcons" onclick="likeClick(3)"></img>';
			html += '<p id="momentsValues_3" style="text-align: center;" class="momentsValue">0</p>';
			html += '<img id="momentsLikes_4" src="'+contenutiUrl + "/" + oggetto.moments_icons.icona_4 +'" class="momentsIcons" onclick="likeClick(4)"></img>';
			html += '<p id="momentsValues_4" style="text-align: center;" class="momentsValue">0</p>';
			html += '<img id="momentsLikes_5" src="'+contenutiUrl + "/" + oggetto.moments_icons.icona_5 +'" class="momentsIcons" onclick="likeClick(5)"></img>';
			html += '<p id="momentsValues_5" style="text-align: center;" class="momentsValue">0</p>';
			html += '<img id="momentsLikes_6" src="'+contenutiUrl + "/" + oggetto.moments_icons.icona_6 +'" class="momentsIcons" onclick="likeClick(6)"></img>';
			html += '<p id="momentsValues_6" style="text-align: center;" class="momentsValue">0</p>';
			html += '<img id="momentsLikes_7" src="'+contenutiUrl + "/" + oggetto.moments_icons.icona_7 +'" class="momentsIcons" onclick="likeClick(7)"></img>';
			html += '<p id="momentsValues_7" style="text-align: center;" class="momentsValue">0</p>';
			$("#momentsIconsDiv").html(html);	


			//console.log("fatto - external json");
			console.log(JSON.stringify(oggetto));
			
			//creo il contentManager che mi gestirà i contenuti visualizzati sul device
			contentManager = new ContentManager(contenutiUrl,contenutiPath,oggetto.coloreGenerale,RA_GENERALE);

			//prima del world.init, mi instanzio tutte le classi dinamiche
			var classToInit = new Array();
			for (i=0;i<oggetto.target.length;i++)
			{
				for (t=0;t<oggetto.target[i].punti.length;t++)
				{
					if (oggetto.target[i].punti[t].tipo==1000) //dinamico ()
					{
						var config=JSON.parse(oggetto.target[i].punti[t].config);
						if (typeof config.contenuto != 'undefined')
						{
							for (var x=0;x<config.contenuto.length;x++)
							{
								if (classToInit.indexOf(config.contenuto[x].tipo) == -1) classToInit.push(config.contenuto[x].tipo);
							}
						}
						else //nuova modalità
						{
							console.log("config:" + JSON.stringify(config));
							//il config è un array
							for (var x=0;x<config.length;x++)
							{
								var configInterno=JSON.parse(config[x].config);
								if (typeof configInterno.tipologia != 'undefined')
								{
									if (classToInit.indexOf(configInterno.tipologia) == -1) classToInit.push(configInterno.tipologia);
								}
							}
						}
					}
					else //vedo comunque all'interno, perchè ci potrebbe essere un contenuto dinamico non in prima posizione
					{
						var config=JSON.parse(oggetto.target[i].punti[t].config);
						console.log("config:" + JSON.stringify(config));
						//il config è un array
						for (var x=0;x<config.length;x++)
						{
							var tipo=parseInt(config[x].tipo);
							if (tipo>=1000) //classe dinamica
							{
								var configInterno=JSON.parse(config[x].config);
								if (typeof configInterno.tipologia != 'undefined')
								{
									if (classToInit.indexOf(configInterno.tipologia) == -1) classToInit.push(configInterno.tipologia);
								}
							}
						}

					}
				}
			}

			//moments
			//comunque sia, metto a inizializzare la classe dei commenti, che ci deve essere di default
			if (classToInit.indexOf("Commento") == -1) classToInit.push("Commento");

			console.log("classToInit: " + JSON.stringify(classToInit));

			if (classToInit.length>0)
			{
				doRequire(classToInit,0); //funzione ricorsiva per fare il require di tutte le classi, alla fine chiama la world.init
			}
			else
			{
				console.log("faccio partire Wiki....");
				World.init();
			}
		})
		.fail(function( jqXHR, textStatus ) {
	  		console.log( "Request failed: " + textStatus );
		});

}


function scegliOverlay(itemRilevato)
{
	console.log("sono in scegliOverlay, con nome: "+itemRilevato);

	//se ci sta qualcosa in esecuzione (video, ecc) non continuo
	if (($("#videoContainer").is(":visible")) || ($("#textContainer").is(":visible")) || ($("#imageContainer").is(":visible")) || ($("#swiperDiv").is(':visible')))
	{
		return;
	}

	//se ci sta il popup di contenuti multipli non continuo
	if (popupOnScreen)
	{	
		return;
	}

	stopBarAnim=true;
	stopBarAnimFn();

	//nome da far comparire in alto
	var nome="";
	var gradimento;
	var idImmagine;
	var scaricabile;
	var imagePath;
	var tipo;
	var autopartente = null; //se incontra un hotspot autopartente, memorizzerà qui target, punto e indice
	var videoOverlay = false;
 
	for (i=0;i<oggetto.target.length;i++)
	{
		if (oggetto.target[i].targetName==itemRilevato)
		{
			console.log(JSON.stringify(oggetto.target[i]));
			nome=oggetto.target[i].imageName;
			gradimento=oggetto.target[i].gradimento;
			voto=oggetto.target[i].voto;
			voti=oggetto.target[i].voti;
			idImmagine=oggetto.target[i].targetName;
			scaricabile=oggetto.target[i].scaricabile;
			imagePath=oggetto.target[i].imagePath;
			tipo=oggetto.target[i].tipo;
			// e comunico all'esterno che ha rilevato l'immagine
			openContent(idImmagine,nome);			
			
			//cerco eventuale autopartente
			for (t=0;t<oggetto.target[i].punti.length;t++)
			{
				var oggettoConfig=JSON.parse(oggetto.target[i].punti[t].config);
				for (z=0;z<oggettoConfig.length;z++)
				{
					console.log("considero l'indice "+z);
					if (oggettoConfig[z] != null)
					{					
						if (typeof oggettoConfig[z].autostart != 'undefined')
						{
							if (oggettoConfig[z].autostart==1)
							{
								autopartente = new Object();
								autopartente.immagine=i;
								autopartente.punto=t;						
								autopartente.indice=z;						
							}
						}
					}
				}
			}
		}
	}
	
	console.log("Autopartente: " + JSON.stringify(autopartente));

	//se ci sta solo una pagina riconosciuta mostro il nome, altrimenti niente
	pagineRiconosciute.push(nome);
	aggiornaHeaderMessage();

	var time=0;
	for (i=0;i<arrayOverlay.length;i++)
	{
		if (arrayOverlay[i].targetRef==itemRilevato)
		{
			console.log("trovato oggetto da abilitare: " + JSON.stringify(arrayOverlay[i]));
			abilitaOggetto(arrayOverlay[i],time);
			time = time+50;
			//è un video drawable?
			console.log("video overlay:");
			if (typeof arrayOverlay[i].isOverlayVideo != 'undefined')
			{
				console.log("esiste isOverlayVideo");
				console.log(arrayOverlay[i].isOverlayVideo);
				if (arrayOverlay[i].isOverlayVideo==true)
				{
					console.log("ed è anche true");
					arrayOverlay[i].play(-1);					
				}
			}

		}
		else
		{
			//arrayOverlay[i].enabled=false; //ci pensa la procedura di onImageLost a disabilitare
		}
	}
	//check autopartenza
	if (autopartente!=null) showContenutoExecute(autopartente.immagine, autopartente.punto, autopartente.indice);
}

function toggleLikeIcons(idImmagine)
{
	if ($("#momentsIconsDiv").is(":visible"))
	{
		//la nascondo e non faccio più niente
		$("#momentsIconsDiv").css('display', 'none');
		idImmagine4Moments=-1;
	}
	else
	{
		idImmagine4Moments=idImmagine;
		//aggiorno i dati e la mostro
		var likes = [0,0,0,0,0,0,0,0,0,0,0];
        for (var i=0;i<oggetto.likes.length;i++)
		{
			if (oggetto.likes[i].idImmagine==idImmagine)
			{
				if (oggetto.likes[i].like!=null)
				{
					likes[parseInt(oggetto.likes[i].like)]++;
				}
			}
		}
		for (var i=1;i<=7;i++)
		{
			$("#momentsValues_"+i).html(likes[i]);
			//$("#momentsLikes_"+i).unbind('click');
			//$("#momentsLikes_"+i).click(likeClick(idImmagine,i));
			//$("#momentsLikes_"+i).on("click", likeClick(idImmagine,i));
		}
		$("#momentsIconsDiv").css('display', 'flex');
		resizeBasedOnResolution(RA_GENERALE);
	}	
}

function likeClick(like)
{
	console.log("Setto il voto " + like + " per l'immagine " + idImmagine4Moments);
	$("#loader").show();
	setLikes(idImmagine4Moments, like);
}

function setLikesReturn(likes)
{
	console.log("tornato da setLikes, con oggetto likes:");
	console.log(JSON.stringify(likes));
	if (likes==null) return; //non faccio niente, ma non tolgo le cose che già ho

	oggetto.likes=likes;

	updateLikesClapComments();

	$("#momentsIconsDiv").css('display', 'none');
	idImmagine4Moments=-1;
	//tolgo anche le icone dei likes
	$("#loader").hide();

		

}


function addClap(idImmagine)
{
	console.log("aggiungo un clap per l'immagine " + idImmagine);
	$("#loader").show();
	setClap(idImmagine);
}

function setClapReturn(likes)
{
	console.log("tornato da setClap, con oggetto likes:");
	console.log(JSON.stringify(likes));
	if (likes==null) return; //non faccio niente, ma non tolgo le cose che già ho

	oggetto.likes=likes;

	updateLikesClapComments();

	$("#loader").hide();			

}


function getLikesCommentsClapsReturn(obj)
{
	console.log("tornato da getLikesCommentsClapsReturn, con oggetto:");
	console.log(JSON.stringify(obj));
	if (obj==null) return; //non faccio niente, ma non tolgo le cose che già ho

	oggetto.likes=obj.moments_likes;
	oggetto.comments=obj.moments_comments;		
	
	updateLikesClapComments();
	
	$("#loader").hide();			

}


function updateLikesClapComments()
{

	for (i=0;i<arrayOverlay.length;i++)
	{
		//quando arriva su una icona moments devo verificare se l'utente ha votato per questa immagine e nel caso mettere l'immagine giusta
		if (typeof arrayOverlay[i].momentsType != 'undefined')
		{
			console.log("ho trovato un overlay moments");
			console.log(JSON.stringify(arrayOverlay[i]));

			if (arrayOverlay[i].momentsType==0) //icona like
			{

				console.log("è una icona");
				var indiceImmagineIndicatore=0;
				//l'utente ha votato per questa immagine?
		        for (var likeIndex=0;likeIndex<oggetto.likes.length;likeIndex++)
		        {
					if (oggetto.likes[likeIndex].idImmagine==arrayOverlay[i].targetRef)
					{
						if (oggetto.likes[likeIndex].like!=null)
						{
							if (oggetto.likes[likeIndex].deviceId==deviceId)
							{
								indiceImmagineIndicatore=parseInt(oggetto.likes[likeIndex].like);
							}
						}
					}
				}
				console.log("indice immagine da mettere: " + indiceImmagineIndicatore);

				//ho l'immagine da mettere all'hs di like
				//var imgTemp = new AR.ImageResource(immagineIndicatore);
				arrayOverlay[i].imageResource = arrayImageResourceLike[indiceImmagineIndicatore];

			}
			else if (arrayOverlay[i].momentsType==1) //valore likes totali
			{
				console.log("sono i likes totali");
		        var totalLikes=0;
		        for (var likeIndex=0;likeIndex<oggetto.likes.length;likeIndex++)
		        {
		          if (oggetto.likes[likeIndex].idImmagine==arrayOverlay[i].targetRef)
		          {
		            if (oggetto.likes[likeIndex].like!=null)
		            {
		              totalLikes++;
		            }
		          }
				}

				console.log("Like per l'immagine " + arrayOverlay[i].targetRef + ": " + totalLikes);

				var totalLikeStr=" " + totalLikes + " " ;
				arrayOverlay[i].text = totalLikeStr;
			}
			else if (arrayOverlay[i].momentsType==2) //valore clap
			{
				console.log("è il valore del clap");

		        var totalClap=0;
		        for (var likeIndex=0;likeIndex<oggetto.likes.length;likeIndex++)
		        {
		          if (oggetto.likes[likeIndex].idImmagine==arrayOverlay[i].targetRef)
		          {
		            totalClap=totalClap+parseInt(oggetto.likes[likeIndex].clap);
		          }
				}

				console.log("Clap per l'immagine " + arrayOverlay[i].targetRef + ": " + totalClap);

				var totalClapStr=" " + totalClap + " " ;
				arrayOverlay[i].text = " " + totalClapStr + " ";
			}
			else if (arrayOverlay[i].momentsType==3) //totale commenti
			{
				console.log("è il numero dei commenti");

		        var totalComments=0;
		        for (var commentIndex=0;commentIndex<oggetto.comments.length;commentIndex++)
		        {
		          if (oggetto.comments[commentIndex].idImmagine==arrayOverlay[i].targetRef)
		          {
		            totalComments=parseInt(oggetto.comments[commentIndex].totale);
		          }

				};

				console.log("Commenti per l'immagine " + arrayOverlay[i].targetRef + ": " + totalComments);

				var totalCommentsStr=" " + totalComments + " " ;
				arrayOverlay[i].text = totalCommentsStr;

			}




		}

	}


}

function mostraCommenti(idImmagine)
{
	console.log("mostro i commenti l'immagine " + idImmagine);
	oggettoAttesaConnessione = new Object();
	oggettoAttesaConnessione.tipo = 1020; //commento moments
	oggettoAttesaConnessione.immagine = idImmagine;
	$("#loader").show();
	requestConnectionStatus();
}


function paginaPersa(itemPerso)
{
	var nome="";
	for (i=0;i<oggetto.target.length;i++)
	{
		if (oggetto.target[i].targetName==itemPerso) nome=oggetto.target[i].imageName;
	}

	var index = pagineRiconosciute.indexOf(nome);
	if (index > -1) pagineRiconosciute.splice(index, 1);
	//se ci sta il popup dei contenuti multipli, non aggiorno l'header
	if (!popupOnScreen) aggiornaHeaderMessage();

}
 
function aggiornaHeaderMessage()
{
	if (timerHeader) clearInterval(timerHeader);
	if (pagineRiconosciute.length==0)
	{
		indiceHeaderPaginaMostrata=0;
		document.getElementById('loadingMessage').innerHTML = getLabel("INQUADRA_PAGINA","Inquadra una pagina.....");
	}
	else if (pagineRiconosciute.length==1)
	{
		indiceHeaderPaginaMostrata=0;
		document.getElementById('loadingMessage').innerHTML = pagineRiconosciute[0] ;
	}
	else
	{
		timerHeader = setInterval(function() {
			document.getElementById('loadingMessage').innerHTML = pagineRiconosciute[indiceHeaderPaginaMostrata];
			indiceHeaderPaginaMostrata++;
			if (indiceHeaderPaginaMostrata>=pagineRiconosciute.length) indiceHeaderPaginaMostrata=0; //per ricominciare
		}, 1000);
	}

}

function abilitaOggetto(obj,time)
{
	console.log("sono in abilitaOggetto, con time=" + time);
	setTimeout(function() {
		console.log(JSON.stringify(obj));

		obj.enabled=true
		//obj.scale=1;
		if (obj.customType==1)
		{
			if (typeof obj.rotationAnimation != 'undefined')
			{
				obj.rotationAnimation.start(-1);
			}
		}
	}, time);
}

function showContenuto(immagine, punto)
{
	contenutoScelto=true;

	console.log("sono in showContenuto, con immagine: "+immagine+ " e punto: " + punto);

	//nel config ci sarà il configExt, se non nullo, altrimenti il config vecchio in forma di configExt
	//è da attivare la finestra con i contenuti multipli, ma solo se ci sono più di un contenuto
	var oggettoConfigExt=JSON.parse(oggetto.target[immagine].punti[punto].config);
	if (oggettoConfigExt.length>1)
	{
		//quando viene mostrato il popup, il riconoscimento della RA deve essere disabilitato
		popupOnScreen=true;
		creaPopupSceltaContenuto(RA_GENERALE, oggettoConfigExt, oggetto.target[immagine].punti[punto].label, immagine, punto);
	}
	else // solo un contenuto, inutile mostrare la lista
	{
		showContenutoExecute(immagine, punto, 0);		
	}
}

function showContenutoExecute(immagine, punto, indice)
{
	accendiLed (false);

	console.log("sono in showContenutoExecute, con immagine: "+immagine+ " punto: " + punto + " e indice: " + indice);
	if (popupOnScreen)
	{
		popupOnScreen=false;
		aggiornaHeaderMessage();
		$("#infoContainer").hide();
	}

	//se passo null come indice, vuol dire che considero il vecchio config
	//altrimenti considero il nuovo e mi rappresenta l'indice all'interno dell'array dei contenuti	
	var oggettoPunto;
	var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
	oggettoPunto=oggettoContenuti[indice];

	//per alcuni conteuti è necessaria la verifica della connessione
	//in quel caso, mi memorizzo i dati che verranno usati al ritorno della funzione
	console.log("tipo: " + oggettoPunto.tipo);
	switch (parseInt(oggettoPunto.tipo)) {
	    case 1: //video
	    case 2: //slideshow
	    case 5: //elemento scaricabile
	    case 7: //audio
	    case 1001: //elemento collaborativo - votazione, gradimento
	    case 1002: //elemento collaborativo - commento
	    case 1003: //elemento collaborativo - condivisione
			console.log("sono nel primo case");
			oggettoAttesaConnessione = new Object();
			oggettoAttesaConnessione.tipo = oggettoPunto.tipo;
			oggettoAttesaConnessione.immagine = immagine;
			oggettoAttesaConnessione.punto = punto;
			oggettoAttesaConnessione.indice = indice;
			$("#loader").show();
			requestConnectionStatus();
	        break;
	    case 1010: //calcolo matriciale - dinamico
			var oggettoDinamico;
			oggettoDinamico=JSON.parse(oggettoPunto.config);
			var tipologia=oggettoDinamico.tipologia;
			actualClass==null;
			console.log("tipologia da cercare: " + tipologia);
			for (var i=0;i<arrayDynamicClasses.length;i++)
			{
				console.log("classe "+i+" chiSono: " + arrayDynamicClasses[i].chiSono());
				if (arrayDynamicClasses[i].chiSono()==tipologia) actualClass=arrayDynamicClasses[i];
			}
			//ho una classe non nulla?
			if (actualClass==null) return;
			//disabilito temporaneamente la camera
			AR.hardware.camera.enabled = false;
			// e disabilito tutto
			disabilitatutto();
			// e faccio partire la classe dinamica
			actualClass.renderMainExt(immagine, punto, indice, deviceId);
	        break;
	    case 3: //testo
			console.log("sono nel secondo case");
			oggettoTesto=JSON.parse(oggettoPunto.config);
			contentManager.setTesto(oggettoTesto);
	        break; 
	    case 4: //html zip
			console.log("sono nel terzo case");
			//per questo tipo di contenuto devo prima scaricare il file, scompattarlo e dopo eseguirlo...
			$("#loader").show();
			scaricaHtmlZip(oggettoPunto.config);
	        break;
	    case 6: //link esterno
			console.log("sono nel quarto case");
			oggettoLink=JSON.parse(oggettoPunto.config);
			
			//ci sono dei parametri usciti fuori da qualche classe dinamica?
			var qs="";
			if ((typeof queryStringArray != 'undefined') && (queryStringArray.length>0))
			{
				qs="?i=0"; // serve solo per far mattere si seguito l'&
				for (var i=0;i<queryStringArray.length;i++)
				{
					qs+="&"+queryStringArray[i];
				}
			}
			
			console.log("URL completo: " + oggettoLink.url + qs);
			openExternalLink(oggettoLink.url + qs);
			//e dico che non ha scelto nessun contenuto, in modo che ritornando ricompaia la barra di scansione
			back();
			/*
			contenutoScelto=false;
			// e se ci stava un popup di scelta, lo tolgo
			if (popupOnScreen)
			{
				popupOnScreen=false;
				aggiornaHeaderMessage();
			}
			*/
	        break; 
	    case 8: //immagine panoramica
			console.log("sono nel case immagine panoramica");
			oggettoAttesaConnessione = new Object();
			oggettoAttesaConnessione.tipo = oggettoPunto.tipo;
			oggettoAttesaConnessione.immagine = immagine;
			oggettoAttesaConnessione.punto = punto;
			oggettoAttesaConnessione.indice = indice;
			oggettoAttesaConnessione.preName = idProgetto+"_"+immagine+"_"+punto+"_"+indice+"_";

			$("#loader").show();
			requestConnectionStatus();
	        break;
	    default: 
			console.log("sono nel case default");
	        //default, mai eseguito;
	}	

}

function checkConnessione(status)
{
	//alert("status: " + status);
	$("#loader").hide();
	console.log("tornato da check connessione, con status: " + status);
	console.log("oggettoAttesaConnessione.tipo: " + oggettoAttesaConnessione.tipo);


	if (status==true) //connessione presente
	{
		if (oggettoAttesaConnessione.tipo==1) //video
		{
			var audio = document.getElementById('audio');
			if (audio.currentSrc != "") pausaAudio(); //in caso un audio è in esecuzione

			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var oggettoVideo;

			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			oggettoVideo=JSON.parse(oggettoPunto.config);
			contentManager.setVideo(oggettoVideo);
		}
		else if (oggettoAttesaConnessione.tipo==2) //slideshow
		{
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var arrayOggettoImmagine;

			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			arrayOggettoImmagine=JSON.parse(oggettoPunto.config);
			contentManager.setSlideshow(arrayOggettoImmagine, true, oggetto.target[immagine].punti[punto].id); //connessione presente
		}
		else if (oggettoAttesaConnessione.tipo==5) //elemento scaricabile
		{
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var oggettoScaricabile;

			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			oggettoScaricabile=JSON.parse(oggettoPunto.config);

			contentManager.setElementoScaricabile(oggettoScaricabile);
		}
		if (oggettoAttesaConnessione.tipo==7) //audio
		{
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var oggettoAudio;

			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			oggettoAudio=JSON.parse(oggettoPunto.config);
			contentManager.setAudio(oggettoAudio);
		}
		else if (oggettoAttesaConnessione.tipo==8) //immagine panoramica
		{
			$("#loader").show();
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var oggettoPano;

			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			oggettoPano=JSON.parse(oggettoPunto.config);
			oggettoPano.connectionPresent=true;
			oggettoPano.preName=oggettoAttesaConnessione.preName;

			contentManager.setPano(oggettoPano);

			/*
			var oggettoPano = new Object();
			//oggettoPano.path="contenuti/1234_1_1234567890.jpg";
			//oggettoPano.type="";
			
			oggettoPano.path="http://thiago.me/image-360/Polie_Academy_53.JPG";
			oggettoPano.type="LINK";

			oggettoPano.connectionPresent=true;
			oggettoPano.preName="3210_2_1_";

			contentManager.setPano(oggettoPano); 
*/

		}
		else if ((oggettoAttesaConnessione.tipo==1001) || (oggettoAttesaConnessione.tipo==1002) || (oggettoAttesaConnessione.tipo==1003))//votazione, commenta, share
		{
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			var indice=oggettoAttesaConnessione.indice;
			var oggettoDinamico;
			
			var oggettoContenuti=JSON.parse(oggetto.target[immagine].punti[punto].config);
			var oggettoPunto=oggettoContenuti[indice];
			oggettoDinamico=JSON.parse(oggettoPunto.config);
			var tipologia=oggettoDinamico.tipologia;
			actualClass==null;
			console.log("tipologia da cercare: " + tipologia);
			for (var i=0;i<arrayDynamicClasses.length;i++)
			{
				console.log("classe "+i+" chiSono: " + arrayDynamicClasses[i].chiSono());
				if (arrayDynamicClasses[i].chiSono()==tipologia) actualClass=arrayDynamicClasses[i];
			}
			//ho una classe non nulla?
			if (actualClass==null) return;
			//disabilito temporaneamente la camera
			AR.hardware.camera.enabled = false;
			// e disabilito tutto
			disabilitatutto();
			// e faccio partire la classe dinamica
			actualClass.renderMainExt(immagine, punto, indice, deviceId);
		}
		else if (oggettoAttesaConnessione.tipo==1020)// commenta moments
		{
			var immagine=oggettoAttesaConnessione.immagine;
			var tipologia="Commento";
			actualClass==null;
			console.log("tipologia da cercare: " + tipologia);
			for (var i=0;i<arrayDynamicClasses.length;i++)
			{
				console.log("classe "+i+" chiSono: " + arrayDynamicClasses[i].chiSono());
				if (arrayDynamicClasses[i].chiSono()==tipologia) actualClass=arrayDynamicClasses[i];
			}
			//ho una classe non nulla?
			if (actualClass==null) return;
			//disabilito temporaneamente la camera
			AR.hardware.camera.enabled = false;
			// e disabilito tutto
			disabilitatutto();
			// e faccio partire la classe dinamica
			actualClass.renderMainExt(immagine, deviceId);
		}
		else if (oggettoAttesaConnessione.tipo==1000)
		{
			//ci sta la connessione, posso mostrare il contenuto dinamico
			
			var time=0;
			//abilito a seconda dell'oggetto
			for (i=0;i<arrayOverlay.length;i++)
			{
				if (arrayOverlay[i].targetRef==oggettoAttesaConnessione.itemRilevato)
				{
					if (arrayOverlay[i].customType==1000)
					{	
						console.log("abilito oggetto: " + JSON.stringify(arrayOverlay[i]));
						abilitaOggetto(arrayOverlay[i],time);
						time = time+50;
					}		
				}
			}
		}
		console.log("cancello oggettoAttesaConnessione");
		oggettoAttesaConnessione = null;
		delete oggettoAttesaConnessione;
	}
	else
	{
		//alert("Nessuna connessione disponibile");
		//ma per lo slideshow potrei aver cacheato i files
		if (oggettoAttesaConnessione.tipo==2)
		{
			//ora devo verificare se qualcosa era stato salvato in precedenza
			var immagine=oggettoAttesaConnessione.immagine;
			var punto=oggettoAttesaConnessione.punto;
			console.log("vado a existSlideshow, con oggettoAttesaConnessione: " + JSON.stringify(oggettoAttesaConnessione));
			existSlideshow(oggetto.target[immagine].punti[punto].id);
			//e aspetto una risposta, non cancellando oggettoAttesaConnessione che mi potrà servire...
		}
		else if (oggettoAttesaConnessione.tipo==1000) //contenuto dinamico.. non faccio niente
		{
			console.log("non posso abilitare gli oggetti dinamici");
			console.log("cancello oggettoAttesaConnessione");
			oggettoAttesaConnessione = null;
			delete oggettoAttesaConnessione;		
		}
		else
		{
			$("#messaggioGenerico p").html(getLabel("NO_CONNESSIONE","Nessuna connessione disponibile"));
			$("#messaggioGenerico").show();

			contenutoScelto=false;

			setTimeout(function() {
				$("#messaggioGenerico").hide();
			}, 4000);

			console.log("cancello oggettoAttesaConnessione");
			oggettoAttesaConnessione = null;
			delete oggettoAttesaConnessione;			
		}
	}

}

function checkConnessioneSlideshow(status) //richiamata quando non ci stava connessione e voglio vedere se lo lideshow esiste in locale, salvato in precedenza
{
	console.log("tornato da existSlideshow, con oggettoAttesaConnessione: " + JSON.stringify(oggettoAttesaConnessione));
	if (status==true) //qualche immagine dello slideshow è presente
	{
		var immagine=oggettoAttesaConnessione.immagine;
		var punto=oggettoAttesaConnessione.punto;

		var arrayOggettoImmagine=JSON.parse(oggetto.target[immagine].punti[punto].config)
		contentManager.setSlideshow(arrayOggettoImmagine, false, oggetto.target[immagine].punti[punto].id); //connessione non presente
	}
	else
	{
		//alzo definitivamente bandiera bianca
		$("#messaggioGenerico p").html(getLabel("NO_CONNESSIONE","Nessuna connessione disponibile"));
		$("#messaggioGenerico").show();

		contenutoScelto=false;

		setTimeout(function() {
			$("#messaggioGenerico").hide();
		}, 4000);	
	}
	console.log("cancello oggettoAttesaConnessione");
	oggettoAttesaConnessione = null;
	delete oggettoAttesaConnessione;	
}

function panoReturn(downloadOK, filename) 
{
	console.log("tornato su panoReturn con downloadOK: " + downloadOK + " e filename: " + filename);
	if (downloadOK==true) 
	{
		contentManager.creaPano(filename);
	}
	else
	{
		//comunico errore
		$("#loader").hide();
	}
}

function back() //quando si chiudono elementi come video o slideshow, o pupup contenuti multipli
{
	if (popupOnScreen) //ci stava il popup
	{
		popupOnScreen=false;
		aggiornaHeaderMessage();
	}

	backUtils(RA_GENERALE);
}



$(document).ready(function()
{

	setTimeout(function() {
		
		resizeBasedOnResolution(RA_GENERALE);
	}, 1000);	


	/*
	//aggiungo le icone di moments
	var html='';
	html += '<img id="momentsLikes_1" src="http://livengine.mediasoftonline.com/gradimentoIcons/icona_1.png" class="momentsIcons" onclick="likeClick()"></img>';
	html += '<p id="momentsValues_1" style="text-align: center;" class="momentsValue">1</p>';
	html += '<img src="http://livengine.mediasoftonline.com/gradimentoIcons/icona_2.png" class="momentsIcons" onclick="likeClick()"></img>';
	html += '<p style="text-align: center;" class="momentsValue">2</p>';
	html += '<img src="http://livengine.mediasoftonline.com/gradimentoIcons/icona_3.png" class="momentsIcons" onclick="likeClick()"></img>';
	html += '<p style="text-align: center;" class="momentsValue">3</p>';
	html += '<img src="http://livengine.mediasoftonline.com/gradimentoIcons/icona_4.png" class="momentsIcons" onclick="likeClick()"></img>';
	html += '<p style="text-align: center;" class="momentsValue">4</p>';
	html += '<img src="http://livengine.mediasoftonline.com/gradimentoIcons/icona_5.png" class="momentsIcons" onclick="likeClick()"></img>';
	html += '<p style="text-align: center;" class="momentsValue">5</p>';
	html += '<img src="http://livengine.mediasoftonline.com/gradimentoIcons/icona_6.png" class="momentsIcons" onclick="likeClick()"></img>';
	html += '<p style="text-align: center;" class="momentsValue">6</p>';
	html += '<img src="http://livengine.mediasoftonline.com/gradimentoIcons/icona_7.png" class="momentsIcons" onclick="likeClick()"></img>';
	html += '<p style="text-align: center;" class="momentsValue">6</p>';
	$("#momentsIconsDiv").html(html);
*/



/*
	setTimeout(function() {
		stopBarAnimFn();
	}, 20000);	
*/

/*
	setTimeout(function() {
		AR.hardware.sensors.enabled=false;
		console.log("sensors.enabled now: " + AR.hardware.sensors.enabled);
	}, 35000);	

	setTimeout(function() {
		resizeBasedOnResolution(RA_GENERALE);
	}, 1000);
*/
	

/* PANO TEST
	contentManager = new ContentManager("","","#ff0000");

	var oggettoPano = new Object();
	oggettoPano.path="1234_1_1234567890.jpg";
	oggettoPano.type="";
	contentManager.setPano(oggettoPano); 

	
	//panoViewer = new Kaleidoscope.Image({source: './js/kaleidoscope/testPano.jpg', containerId: '#panoContainer'});
	panoViewer = new Kaleidoscope.Image({source: 'http://thiago.me/image-360/Polie_Academy_53.JPG', 
										containerId: '#panoContainer',
										initialYaw: 90});
	panoViewer.render();
	//panoViewer.setSize({height: 667, width: 375});
*/

	
	/*
	setTimeout(function() {
		var color=$("body").css("background");
		var color2=$("body").css("background-color");

		alert(color + " - " + color2);
	}, 3000);	
*/



	return;
	$("#backButton").show();
	
	$("#loadingMessage").html("Attendere...");
	$("#messaggiDownload p").html("Attendere, scaricamento contenuto in corso");
	$("#testoInquadraCopertina").html("Inquadra una pagina...");
		
	//return;	
	var col1=convertHex("#FF0000",80);
	var col2=convertHex("#FF0000",40);
	var col3=convertHex("#FF0000",0);
	var col4=convertHex("#FF0000",60);

	$( "#header" ).css("background","linear-gradient(to bottom, "+col1+", "+col2+"");
	$( "#subHeader" ).css("background","linear-gradient(to bottom, "+col2+", "+col3+"");
	$( "#footer" ).css("background","linear-gradient(to top, "+col4+", "+col3+"");

	$("#badgeUtenti p").html("25");
	$("#badgeUtenti").show();

	resizeBasedOnResolution(RA_GENERALE);

	
	contentManager = new ContentManager("","","#ff0000");
	
	//var url=decodeURIComponent("http://livengine.mediasoftonline.com/test/Mazinga.mp3");
	var url="http://livengine.mediasoftonline.com/test/Mazinga.mp3";
	//var url=decodeURIComponent("https://www.yt2mp3s.me/@download/256-5ac4aa1fcafb1-6496000-203/mp3/D05mceE9CHk/Mazinga%2BZ%2Bsigla%2Bil%2BGrande%2BMazinger.mp3");

	//contentManager.creaAudio(url,"LINK");

	setTimeout(function() {
	    var audio = document.getElementById('audio');
	    console.log(audio.currentSrc);


		if (audio.currentSrc != "") pausaAudio(); //in caso un audio è in esecuzione
		contentManager.creaVideo("H4f2A4LlgX8","YT",1.777778);

	}, 5000);


	return;
	contentManager = new ContentManager("","","#ff0000");
	
	//var url=decodeURIComponent("http://livengine.mediasoftonline.com/test/Mazinga.mp3");
	var url="http://livengine.mediasoftonline.com/test/Mazinga.mp3";
	//var url=decodeURIComponent("https://www.yt2mp3s.me/@download/256-5ac4aa1fcafb1-6496000-203/mp3/D05mceE9CHk/Mazinga%2BZ%2Bsigla%2Bil%2BGrande%2BMazinger.mp3");

	contentManager.creaAudio(url,"LINK");


	setTimeout(function() {
		
		var url=decodeURIComponent("https://www.yt2mp3s.me/@download/256-5ac4aa1fcafb1-6496000-203/mp3/D05mceE9CHk/Mazinga%2BZ%2Bsigla%2Bil%2BGrande%2BMazinger.mp3");
		contentManager.creaAudio(url,"LINK");

	}, 500000);


	//solo funzioni per testare nel browser

	//var oggettoConfigExt=JSON.parse('[{"tipo":1,"label":"un bel video","config":"{ \'path\':\'http%3A%2F%2Fvideoupload.mondadori.it%2Fserver%2Fphp%2Ffiles%2Fftp%2FCHI%2FSICILY.mp4\',\'nome\':\'Video\',\'type\':\'LINK\'}"},{"tipo":2,"label":"uno Slideshow","config":"[{\'ordine\':\'0\',\'path\':\'contenuti%2F710_3_1506328303_internazionale_h_00_ok.jpg\',\'nome\':\'internazionale_h_00_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'1\',\'path\':\'contenuti%2F710_3_1506328322_internazionale_h_07_ok.jpg\',\'nome\':\'internazionale_h_07_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'2\',\'path\':\'contenuti%2F710_3_1506328317_internazionale_h_08_ok.jpg\',\'nome\':\'internazionale_h_08_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'3\',\'path\':\'contenuti%2F710_3_1506328307_internazionale_h_04_ok.jpg\',\'nome\':\'internazionale_h_04_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'4\',\'path\':\'contenuti%2F710_3_1506328305_internazionale_h_09_ok.jpg\',\'nome\':\'internazionale_h_09_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'5\',\'path\':\'contenuti%2F710_3_1506328304_internazionale_h_01_ok.jpg\',\'nome\':\'internazionale_h_01_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'6\',\'path\':\'contenuti%2F710_3_1506328308_internazionale_h_02_ok.jpg\',\'nome\':\'internazionale_h_02_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'7\',\'path\':\'contenuti%2F710_3_1506328309_internazionale_h_03_ok.jpg\',\'nome\':\'internazionale_h_03_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'8\',\'path\':\'contenuti%2F710_3_1506328312_internazionale_h_05_ok.jpg\',\'nome\':\'internazionale_h_05_ok.jpg\',\'type\':\'LOCAL\'},{\'ordine\':\'9\',\'path\':\'contenuti%2F710_3_1506328314_internazionale_h_06_ok.jpg\',\'nome\':\'internazionale_h_06_ok.jpg\',\'type\':\'LOCAL\'}]"},{"tipo":3,"label":"Un testo","config":"{\'text\':\'<b>STRUTTURA</b><br>Il Montaperti Hotel è situato a circa 15 km da Siena ed è il perfetto connubio tra il design moderno e il paesaggio rurale delle colline senesi. Immersa tra i profumi e i colori dell’incontaminata natura toscana, è la struttura ideale per trascorrere un soggiorno in relax e per godersi la natura.<br><br><b>LA QUOTA COMPRENDE</b><br>Mezza pensione con colazione a buffet e cena con servizio al tavolo con menù di 2 portate (un primo e un dolce), utilizzo della piscina riscaldata con idromassaggio (coperta nei mesi invernali), utilizzo del fitness center e del centro benessere che comprende sauna, bagno turco, docce emozionali, doccia a diluvio con secchiello, cascata di ghiaccio (ingresso al centro benessere non consentito il sabato), sconto del 10% per le Terme di Rapolano, connessione Wi-Fi, parcheggio secondo disponibilità, culla su richiesta alla prenotazione secondo disponibilità.<br><br><b>LA QUOTA NON COMPRENDE</b><br>Bevande ai pasti, kit benessere con accappatoio e ciabattine (a pagamento in loco), eventuale tassa di soggiorno applicata in loco, extra in genere e tutto quanto non specificato nel paragrafo \'La quota comprende\'.<br><br><b>RIDUZIONI</b><br>Riduzioni 3° letto (valide con almeno 2 persone paganti quota intera):<br>• 0 -6 anni non compiuti GRATIS<br>• 6 - 13 anni non compiuti 50%<br>• da 13 anni in poi 25%<br><br><b>ORARIO CHECK-IN/OUT</b><br>Check-in dalle ore 14:00 Check-out entro ore 11:00<br><br><b>CAMERE</b><br>Le camere Comfort (circa 18-20 mq), situate al piano terra con affaccio sulla corte, sono dotate di servizi privati con vasca o doccia, asciugacapelli, aria condizionata, TV Sat, telefono, cassaforte, frigobar e connessione Wi-Fi.  SERVIZI A disposizione degli ospiti reception 24h, ascensore, ristorante, bar, internet point, connessione Wi-Fi (gratuita), A disposizione degli ospiti reception 24h, ascensore, ristorante, bar, internet point, connessione Wi-Fi (gratuita), piscina esterna attrezzata con lettini secondo disponibilità, deposito bagagli, servizio in camera, servizio lavanderia, area fumatori, quotidiani, parcheggio privato secondo disponibilità (gratuito), culla su richiesta al call center al momento della prenotazione (gratuita). Inoltre l\'Hotel mette a disposizione un moderno centro benessere con piscina coperta riscaldata con idromassaggio, sauna, bagno turco, doccia a secchio e cascata di ghiaccio, fitness corner e terrazza solarium (l\'accesso al centro benessere è consentito dalle 10:00 alle 19:00).<br><br><b>AMICI A 4 ZAMPE</b> Non ammessi.\'}"},{"tipo":5,"label":"Download","config":"{\'path\':\'contenuti/202_0.jpg?t=1491475851551\',\'type\':\'LOCAL\',\'descr\':\'Palinsesto 3 aprile\'}"},{"tipo":6,"label":"Sito Web","config":"{\'url\':\'http%3A%2F%2Fwww.lecceprima.it%2Fristoranti%2Fla-staffa-ristorante-bar-pizzeria-tavola-calda-1870736.html\'}"}]');
	//console.log(oggettoConfigExt);
	//creaPopupSceltaContenuto(RA_GENERALE, oggettoConfigExt, "Ciao Ciao", 100, 2);

/*
	contentManager = new ContentManager("","","#ff0000");
	var arrayImages=new Array();
	arrayImages.push("");
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638560_slide4.jpg";
	arrayImages.push(img);
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638562_slide3.jpg";
	arrayImages.push(img);
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638567_slide1.jpg";
	arrayImages.push(img);
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638568_slide2.jpg";
	arrayImages.push(img);
	fullscreenSwiper=true;
	contentManager.creaSlideshowNew(arrayImages,true);
*/

/*
	contentManager = new ContentManager("","","#ff0000");
	fullscreenVideo=true;
	contentManager.creaVideo("H4f2A4LlgX8","YT",1.777778);
	//contentManager.creaVideo("http://livengine.mediasoftonline.com/test/chi_ventura.mp4","LINK",0);
*/
	//resizeBasedOnResolution(RA_GENERALE);
	//$("#backButton").show();
	//getUsersNumber(4);
	/*
	$("body").css("background-color","#000000");
	resizeBasedOnResolution(RA_GENERALE);
	var arrayImages=new Array();
	arrayImages.push("");
	var img = "http://www.cliccascienze.it/files/isola.jpg";
	arrayImages.push(img);
	img = "https://www.greenme.it/immagini/viaggiare/eco-turismo/isole_cuore/isole_a_forma_di_cuore.jpg";
	arrayImages.push(img);
	
	creaSlideshowNewTEST(arrayImages);
*/
	return;

});
