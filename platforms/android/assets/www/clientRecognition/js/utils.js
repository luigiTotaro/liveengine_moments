var RA_COPERTINA = "ra_copertina";
var RA_GENERALE = "ra_generale";
var RA_POI = "ra_poi";

function convertHex(hex,opacity){
    hex = hex.replace('#','');
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);

    result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
    return result;
}

function decode(s) 
{
      //se riesco a fare un JSON.parse, vuol dire che quello che sto tentando di decodificare è già in chiaro
      //(possibile decodifica di un vecchio file in chiaro in assenza di rete, per esempio)

      try {
          JSON.parse(s);
      } catch (e) {

        var k = "123";
        var enc = "";
        var str = "";
        //console.log("key: " + k);
        // make sure that input is string
        str = s.toString();
        for (var i = 0; i < s.length; i++) {
          // create block
          var a = s.charCodeAt(i);
          // bitwise XOR
          var b = a ^ k;
          enc = enc + String.fromCharCode(b);
        }
        return enc;

      }
      return s;

}


/*
  var panorama, viewer;

  //panorama = new PANOLENS.ImagePanorama( 'https://photoblogstop.com/wp-content/uploads/2012/07/Sierra_HDR_Panorama_DFX8048_2280x819_Q40_wm_mini.jpg' );
  panorama = new PANOLENS.ImagePanorama( 'Sierra_HDR_Panorama_DFX8048_2280x819_Q40_wm_mini.jpgs' );

  viewer = new PANOLENS.Viewer();
  viewer.add( panorama );
*/

/*
var canvas = document.getElementById('canvas4Track');
var context = canvas.getContext('2d');

var FastTracker = function() {
  FastTracker.base(this, 'constructor');
};

tracking.inherits(FastTracker, tracking.Tracker);
tracking.Fast.THRESHOLD = 2;

FastTracker.prototype.threshold = tracking.Fast.THRESHOLD;

FastTracker.prototype.track = function(pixels, width, height) {
  //stats.begin();
  var gray = tracking.Image.grayscale(pixels, width, height);
  var corners = tracking.Fast.findCorners(gray, width, height);
  //stats.end();
  this.emit('track', {
    data: corners
  });
};

var tracker = new FastTracker();

tracker.on('track', function(event) {
  context.clearRect(0, 0, canvas.width, canvas.height);
  var corners = event.data;
  for (var i = 0; i < corners.length; i += 2) {
    context.fillStyle = '#f00';
    context.fillRect(corners[i], corners[i + 1], 2, 2);
  }
});

tracking.track('#video4Track', tracker, { camera: true });

*/


function getLabel(codice,defaultLabel)
{
  console.log("getLabel - " + codice + " - " + defaultLabel);
  var ret = defaultLabel;
  //cerco il codice nell'oggetto
  for (var i=0;i<labelsObj.length;i++)
  {
    if (labelsObj[i].codice==codice)
    {
      var token=labelsObj[i];
      ret = token[language] ? token[language] : token[language + 'Default'];

      /*
      switch(lingua) {
          case "it":
              if ((labelsObj[i].it==null) || (labelsObj[i].it=="")) ret=labelsObj[i].itDefault;
              else ret=labelsObj[i].it;
              break;
          case "en":
              if ((labelsObj[i].en==null) || (labelsObj[i].en=="")) ret=labelsObj[i].enDefault;
              else ret=labelsObj[i].en;
              break;
      }
      */
    }
  }
  return ret;
}


function startAnimationBar(tipo)
{
    console.log("startAnimationBar");
    if (tipo==RA_GENERALE)
    {
      //potrebbe essere arrivato qui da una perdita di immagine, ma con un contenuto in visualizzazione, per cui non deve partire la barra
      if (($("#videoContainer").is(":visible")) || ($("#textContainer").is(":visible")) || ($("#imageContainer").is(":visible")) || ($("#swiperDiv").is(':visible')) || ($("#panoContainer").is(":visible")))
      {
          return;
      }
      console.log("Nessun contenuto in visualizzazione");
      //ma anche se sta partendo un contentuo, non deve visualizzare la barra (un video ci mette un po' e il suo container non è iummedistamanete visibile...)
      if (contenutoScelto==true) return;

      console.log("Nessun contenuto scelto");

      $("#barraScansione").show();
      $("#messaggioIniziale").show();
      $("#barraScansione").css("left","0px");
    }

    stopBarAnim=false;
    goRight(tipo);
    showHideMessage(tipo);
}


function goRight(tipo) {
    //console.log("goRight");
    var larghezza=$( window ).width();
    var larghezzaBarra=$("#barraScansione").width();
    
    $("#barraScansione").animate({
    left: (larghezza-larghezzaBarra)
  }, 3000, function() {
        //console.log("Finito goRight");
        //console.log(tipo);

     if (stopBarAnim==false) goLeft(tipo);
     else
     {
        if (tipo==RA_COPERTINA) creaImmagineCopertina(idCopertinaRilevata);
     }
  });
}

function goLeft(tipo) {
    //console.log("goLeft");

    $("#barraScansione").animate({
    left: 0
  }, 3000, function() {
        //console.log("Finito goLeft");
        //console.log(tipo);

     if (stopBarAnim==false) goRight(tipo);
     else
     {
        if (tipo==RA_COPERTINA) creaImmagineCopertina(idCopertinaRilevata);
     } 
  });
}

function showHideMessage(tipo) {
    var newOpacity;
    var actualOpacity=$("#messaggioIniziale").css("opacity");
    var easing;
    if (actualOpacity==1)
    {
      newOpacity=0;
      easing="easeInCubic";
    }
    else
    {
      newOpacity=1;
      easing="easeOutCubic";
    }

    if (tipo==RA_GENERALE)
    {
      $("#messaggioIniziale").animate({
        opacity: newOpacity
        }, 1500, easing, function() {
         if (newOpacity==0) showHideMessage(tipo); //comunque continuo
         else //è arrivato a dx o a sx... ora controllo
         {
           if (stopBarAnim==false) showHideMessage(tipo);
           else
           {
            $("#messaggioIniziale").hide();
           }
         } 
      });
    }
    else
    {
      $("#messaggioIniziale").animate({
        opacity: newOpacity
        }, 1500, easing, function() {
         if (stopBarAnim==false) showHideMessage(tipo);
         else //potrebbe stare a metà.. devo comunque arrivare alla fine..
         {
          if (newOpacity==0) showHideMessage(tipo);
         }
       });
      
      $("#users_container").animate({
        opacity: newOpacity
        }, 1500, easing, function() {
      });      
    }
}

function stopBarAnimFn() {
    console.log("stopBarAnim");
    
    $("#messaggioIniziale").stop( false, false ).fadeOut();
    $("#barraScansione").stop( false, false ).fadeOut();


}


function resizeBasedOnResolution(tipo)
{
  var larghezza=$( window ).width();
  var altezza=$( window ).height();

  console.log( "larghezza:" + larghezza );
  console.log( "altezza:" + altezza );

  //$("#video4Track").attr("width",larghezza).attr("height",altezza);
  //$("#canvas4Track").attr("width",larghezza).attr("height",altezza);

  if (larghezza>altezza)
  {
    //landscape
    if ((tipo==RA_GENERALE) || (tipo==RA_POI))
    {
      if (larghezza<=640)
      {
        $("#labelChiudi").css("left","5%");
      }
      else
      {
        $("#labelChiudi").css("left","3%");
      }
    }
    else if (tipo==RA_COPERTINA)
    {
      var larghDiv="70%";
      var margDiv="15%";
      if (larghezza<600)
      {
        larghDiv="80%";
        margDiv="10%";
      }
      $("#bottoneNonRiesco").css("width",larghDiv).css("margin-left",margDiv).css("margin-top",((altezza-(altezza/2))/4)+"px");
      $("#messaggioIniziale").css("width",larghDiv).css("margin-left",margDiv).css("margin-top",((altezza-(altezza/3))/3)+"px");
    }
  }
  else
  {
    if ((tipo==RA_GENERALE) || (tipo==RA_POI))
    {
      if (larghezza<=420)
      {
        $("#labelChiudi").css("left","7%");
      }
      else
      {
        $("#labelChiudi").css("left","5%");
      }
    }
    else if (tipo==RA_COPERTINA)
    {
      $("#bottoneNonRiesco").css("width","80%").css("margin-left","10%").css("margin-top",((altezza-(altezza/2))/2)+"px");
      $("#messaggioIniziale").css("width","80%").css("margin-left","10%").css("margin-top",((altezza-(altezza/4))/2)+"px");
    }
  }

  $("#barraScansione").css("height",altezza+"px");

  if (tipo==RA_COPERTINA)
  {
    var altezzaBadgeUtenti=$("#badgeUtenti")[0].clientHeight+2; //aggiunto il bordo
    $("#users_container p").css("font-size",altezzaBadgeUtenti*0.8+"px");
    $("#users_container").css("margin-left","0px");
    var larghezzaUsersContainer = $("#users_container").width();
    $("#users_container").css("margin-left",(larghezza-larghezzaUsersContainer)/2+"px");
    $("#numeroUtenti").css("font-size",altezzaBadgeUtenti*0.65+"px");
    
    if ($("#imageContainer").is(":visible"))
    {
      $("#imageContainer").css("width","90%").css("left","5%");
      $("#imageContainerImg").css("width","100%");
      $("#imageContainerImg").css("height","initial");

      var altezzaImmagineMax=altezza*0.5; //altezza massima immagine
      var altezzaImg=$("#imageContainerImg")[0].height;

      var posizione;
      if (altezzaImg<altezzaImmagineMax)
      {
        //tutto ok, la devo solo centrare
        posizione=(altezza-altezzaImg)/2;
      }
      else
      {
        //deve comandare l'altezza massima
        $("#imageContainerImg").css("height",altezzaImmagineMax+"px").css("width","initial");
        //vedo la larghezza effettiva
        var larghImg=$("#imageContainerImg")[0].width;
        // e setto il contenitore
        $("#imageContainer").css("width",larghImg+"px").css("left",((larghezza-larghImg)/2)+"px");
        posizione=(altezza-altezzaImmagineMax)/2;
        altezzaImg=$("#imageContainerImg")[0].height;
      }
      $("#imageContainer").css("top",posizione+"px");
    }
  }

  //override
  //controllo iphonex
  var isIphoneX=false;
  //alert (window.devicePixelRatio + " - " + window.screen.width + " - " + window.screen.height);
  if ((window.devicePixelRatio==3) && (window.screen.width==375) && (window.screen.height==812)) isIphoneX=true;

  //isIphoneX=true;

  //alert("isIphoneX: " + isIphoneX);



  altezzaHeader=43;
  altezzaFooter=86; //ma è hide....

  var marginTopLoadingMessage="8px";
  var topBadgeUtenti="2px";
  if (isIphoneX)
  {
    altezzaHeader=63;
    $("#backButton").css("margin-top",20);
    marginTopLoadingMessage="28px";
    topBadgeUtenti="22px";
    if (tipo==RA_POI)
    {
      topBadgeUtenti="1px";
      marginTopLoadingMessage="4px";
      $("#backButton").css("margin-top",-2);
      altezzaHeader=43;
    }

  }
  
  $("#header").height(altezzaHeader+"px");
  $("#subHeader").height(altezzaHeader+"px");
  $("#subHeader").css("top",altezzaHeader+"px");
  $("#footer").height(altezzaFooter+"px");
  //$("#footerMsg").height(altezzaFooter+"px");
  //$("#footerMsg").css("font-size",altezzaFooter*0.8);

  $("#lampSpenta").css("font-size","26px");
  $("#lampAccesa").css("font-size","26px");
  $("#audioOn").css("font-size","26px");
  $("#audioPause").css("font-size","26px");



  if (tipo==RA_COPERTINA)
  {
    return; //per la copertina ho finito, non serve altro
  }

  $("#loadingMessage").css("font-size","22px");
  $("#loadingMessage").css("margin-top",marginTopLoadingMessage);

  $("#imgChiudi").css("font-size","26px");
  $("#imgChiudi").css("margin-top","11px");
  $("#imgChiudi").css("margin-left","15px");

  if (tipo==RA_POI)
  {
    $("#imgChiudi").css("font-size","32px");
    $("#imgChiudi").css("margin-top","11px");
    $("#imgChiudi").css("margin-left","15px");
  }

  $("#badgeUtenti").css("top",topBadgeUtenti);
  $("#badgeUtenti p").css("font-size","24px");
  if (tipo==RA_GENERALE)
  {
    $("#badgeUtenti p").css("margin-top","12px");
    $("#badgeUtenti p").css("margin-right","8px");
    $("#badgeUtenti i").css("font-size","20px");
    $("#badgeUtenti i").css("margin-top","13px");
    $("#badgeUtenti i").css("margin-right","6px");
  }
  
  $("#backButton").css("height",altezzaHeader);
  $("#backButton").css("width",larghezza*0.2);



  if (larghezza>altezza)
  {
    //landscape
    $("#messaggioIniziale").css("width","50%").css("margin-left","25%").css("margin-top",((altezza-(altezza/3))/3)+"px"); //.css("height",altezza/3+"px")
    $("#videoContainer").css("width","60%").css("right","20%");
    $("#messaggioGenerico").css("width","50%").css("margin-left","25%").css("margin-top",(altezza/10)+"px"); //css("height",altezza/3+"px")
  }
  else
  {
    $("#messaggioIniziale").css("width","80%").css("margin-left","10%").css("margin-top",((altezza-(altezza/4))/2)+"px"); //.css("height",altezza/4+"px")
    $("#videoContainer").css("width","90%").css("right","5%");
    $("#messaggioGenerico").css("width","80%").css("margin-left","10%").css("margin-top",(altezza/4)+"px"); //.css("height",altezza/4+"px")
  }

  $("#dynamicClassContainer").css("width",larghezza).css("height",altezza).css("top",0);
  if ($("#dynamicClassContainer").is(":visible"))
  {
    if (actualClass != null) actualClass.resizeBasedWindows();
  }


  //icone moments
  if (larghezza>altezza)
  {
    //landscape
    $('.momentsIcons').each(function(index, currentElem){
      $(currentElem).css("width","5%");
      var larghezzaElemento=$(currentElem)[0].width;
      $(currentElem).css("height",larghezzaElemento+"px");
    });
    $('#momentsIconsDiv').css("padding-top","1%").css("padding-bottom","1%");

  }
  else
  {
    $('.momentsIcons').each(function(index, currentElem){
        $(currentElem).css("width","7%");
        var larghezzaElemento=$(currentElem)[0].width;
        $(currentElem).css("height",larghezzaElemento+"px");
    });
    $('#momentsIconsDiv').css("padding-top","2%").css("padding-bottom","2%");
  }

  

  
  //info
  if ($("#infoContainer").is(":visible"))
  {
    var altezzaHeader = $("#header").height();
    var altezzaFooter = $("#footerMsg").height();
    var altezza=$( window ).height();
    var altezzaDisponibile=altezza-altezzaHeader-altezzaFooter;
    var altezzaDiv=altezzaDisponibile*0.7; //è l'altezza massima
    var extra=1+16+0.5; //border, padding, peLLuCiSape

    if (larghezza>altezza)
    {
      //$("#infoContainer").css("width","70%").css("height",(altezzaDisponibile*0.9)+"px").css("right","15%").css("top",altezzaHeader+(altezzaDisponibile*0.05) + "px");
      $("#infoContainer").css("width","80%").css("right","10%").css("top",altezzaHeader+(altezzaDisponibile*0.10) + "px");
      //i pulsanti devono essere grandi la metà dello spazio e messi a 2 a 2
      var larghezzaInfoPulsanti=($("#infoContainer")[0].clientWidth*0.9*0.5)-(extra*2);
      console.log(larghezzaInfoPulsanti);
      $('#infoPulsanti').children().each(function(index, currentElem){
          $(currentElem).css("width",larghezzaInfoPulsanti+"px").css("margin-left","0px");
      });
      var larghezzaTotalePulsanti=larghezzaInfoPulsanti+(extra*2);
      console.log(larghezzaTotalePulsanti);

      var larghezzaMarginePulsanti=($("#infoContainer")[0].clientWidth*0.5-(larghezzaTotalePulsanti))/2 ;
      $('#infoPulsanti').children().each(function(index, currentElem){
          $(currentElem).css("margin-left",larghezzaMarginePulsanti+"px").css("margin-right",larghezzaMarginePulsanti+"px");
      });

    }
    else
    {
      //$("#infoContainer").css("width","80%").css("height",(altezzaDisponibile*0.8)+"px").css("right","10%").css("top",altezzaHeader+(altezzaDisponibile*0.10) + "px");
      $("#infoContainer").css("width","80%").css("right","10%").css("top",altezzaHeader+(altezzaDisponibile*0.10) + "px");
      //i pulsanti devono essere grandi quanto tutto lo spazio
      var larghezzaInfoPulsanti=($("#infoContainer")[0].clientWidth*0.9)-(extra*2);
      console.log(larghezzaInfoPulsanti);
      $('#infoPulsanti').children().each(function(index, currentElem){
          $(currentElem).css("width",larghezzaInfoPulsanti+"px").css("margin-left","0px");
      });
      var larghezzaTotalePulsanti=larghezzaInfoPulsanti+(extra*2);
      console.log(larghezzaTotalePulsanti);

      var larghezzaMarginePulsanti=($("#infoContainer")[0].clientWidth-(larghezzaTotalePulsanti))/2 ;
      $('#infoPulsanti').children().each(function(index, currentElem){
          $(currentElem).css("margin-left",larghezzaMarginePulsanti+"px");
      });
    

    }

  }



  //video
  if ($("#videoContainer").is(":visible"))
  {
    //var larghezzaDiv=$("#videoContainer").width();
    if ($("#videoInterno").is(":visible"))
    {
      if (fullscreenVideo==false)
      {
        $("#btnCloseVideo").show();
        $("#videoContainer").css("position","absolute").css("border","2px solid "+oggetto.coloreGenerale).css("bottom", "initial").css("min-width", "0px").css("min-height", "0px").css("height", "initial");   
        var larghezzaDiv=$("#videoContainer").width();
        
        $("#videoInterno").width(larghezzaDiv*1);
        $("#videoInterno").css("margin-top","0px");

        var altezzaVideo = $("#videoInterno")[0].clientHeight;
          var larghezzaVideo = $("#videoInterno")[0].clientWidth;
          var rapportoVideo=altezzaVideo/larghezzaVideo;
          
          var altezzaContenitore=altezzaVideo/larghezzaVideo*larghezzaDiv;

          //var altezzaContenitore=$("#videoContainer").height();
        var altezzaUtile=(altezza-altezzaHeader)*0.80;
        var posizione=(altezza-altezzaHeader-altezzaContenitore-altezzaFooter)/2 + altezzaHeader;

        if (altezzaContenitore>altezzaUtile)
        {
          //è un video molto alto.. deve comandare l'altezza, non la larghezza
          var newAltezzaVideo=altezzaUtile;
          var newLarghezzaVideo=newAltezzaVideo/rapportoVideo;
          //console.log("newAltezzaVideo: " + newAltezzaVideo);
          //console.log("newLarghezzaVideo: " + newLarghezzaVideo);
          $("#videoContainer").css("height",newAltezzaVideo+"px").css("width",newLarghezzaVideo+"px").css("right",((larghezza-newLarghezzaVideo)/2)+"px");
          $("#videoInterno").css("height",(newAltezzaVideo*0.95)+"px").css("width",(newLarghezzaVideo*0.95)+"px");
          posizione=(altezza-altezzaHeader-newAltezzaVideo)/2;
        }

        $("#videoContainer").css("top",posizione+"px");
      }
      else
      {

        //fullscreen
        //devo comunque nascondere il pulsante di chiusura
        //$("#btnCloseVideo").hide();
        $("#videoContainer").css("border", "0px").css("position", "fixed").css("right", "0").css("bottom", "0").css("top", "0").css("min-width", "100%").css("min-height", "100%").css("width", larghezza+"px").css("height", "auto").css("z-index", "999");
        var larghezzaDiv=$("#videoContainer").width();

        //console.log("larghezzaDiv: " + larghezzaDiv);

        $("#videoInterno").width(larghezzaDiv*1);
        //ora che l'ho portato al 100% di larghezza, quanto è l'altezza?
          var altezzaVideo100 = $("#videoInterno")[0].clientHeight;

        //console.log("altezzaVideo100: " + altezzaVideo100);

        //porto il video al 100%
        //se lo porto al 100% come width, è più alto o più basso dello schermo?
        
        if (altezzaVideo100==altezza) //il video è preciso come altezza e larghezza, va bene così
        {
          $("#videoInterno").css("margin-top","0px");
        } 
        else if (altezzaVideo100<altezza) //il video è più piccolo, lo metto al 100% di larghezza e lo centro
        {
          var margine=(altezza-altezzaVideo100)/2;
          //console.log("video meno alto, margine: " + margine);

          $("#videoInterno").css("margin-top",margine+"px").css("margin-left","0px");

        }     
        else  //il video è troppo alto, faccio comandare l'altezza
        {
          var aspectRatio=larghezzaDiv/altezzaVideo100;
          var newLarghezzaVideo100=altezza*aspectRatio;
          var margine=(altezza-altezzaVideo100)/2;
          $("#videoInterno").css("width",newLarghezzaVideo100+"px").css("margin-top","0px").css("margin-left",margine+"px");
          console.log("video più alto, margine sx: " + margine);

        }     
      
      }

    }
    else
    {
      if (fullscreenVideo==false)
      {
        $("#btnCloseVideo").show();
        $("#videoContainer").css("position","absolute").css("border","2px solid "+oggetto.coloreGenerale).css("bottom", "initial").css("min-width", "0px").css("min-height", "0px").css("height", "initial");   
        var larghezzaDiv=$("#videoContainer").width();
        var larghezzaYT=larghezzaDiv*1;
        var altezzaYT=larghezzaYT/aspectRationPlayerYT;
        $("#playerYT").width(larghezzaYT).height(altezzaYT);
        $("#playerYT").css("width","100%").css("height","100%").css("margin-top","0px");
        //e poi lo centro
        var altezzaContenitore=larghezzaDiv/aspectRationPlayerYT;
        var altezzaFooter = $("#footerMsg").height();
        var posizione=(altezza-altezzaHeader-altezzaContenitore-altezzaFooter)/2 + altezzaHeader;

        $("#videoContainer").css("top",posizione+"px").css("height",altezzaContenitore+"px");
      }
      else
      {

        //fullscreen
        //devo comunque nascondere il pulsante di chiusura
        //$("#btnCloseVideo").hide();
        $("#videoContainer").css("border", "0px").css("position", "fixed").css("right", "0").css("bottom", "0").css("top", "0").css("min-width", "100%").css("min-height", "100%").css("width", "auto").css("height", "auto").css("z-index", "999");
        //porto il video al 100%
        //se lo porto al 100% come width, è più alto o più basso dello schermo?
        var larghezza=$( window ).width();
        var larghezzaVideo100=larghezza;
        var altezzaVideo100=Math.round(larghezza/aspectRationPlayerYT);

        //console.log("larghezza: " + larghezza);
        //console.log("larghezzaVideo100: " + larghezzaVideo100);
        //console.log("altezzaVideo100: " + altezzaVideo100);

        if (altezzaVideo100==altezza) //il video è preciso come altezza e larghezza, va bene così
        {
          $("#playerYT").css("width","100%").css("height","100%").css("margin-top","0px");
        } 
        else if (altezzaVideo100<altezza) //il video è più piccolo, lo metto al 100% di larghezza e lo centro
        {
          $("#playerYT").css("width","100%").css("height",altezzaVideo100+"px").css("margin-top",((altezza-altezzaVideo100)/2)+"px");

        }     
        else  //il video è troppo alto, faccio comandare l'altezza
        {
          var newLarghezzaVideo100=altezza*aspectRationPlayerYT;
          $("#playerYT").css("height",altezza+"px").css("width",newLarghezzaVideo100+"px").css("margin-top","0px");

        }     
      }


    }

  }

  //testo
  if ($("#textContainer").is(":visible"))
  {
    $("#casellaTesto").css("height","initial");

    var altezzaHeader = $("#header").height();
    var altezzaFooter = $("#footerMsg").height();
    var altezza=$( window ).height();
    var altezzaDisponibile=altezza-altezzaHeader-altezzaFooter;
    var altezzaTesto=altezzaDisponibile*0.7; //è l'altezza massima

    var posizione;
    //vedo se l'altezza, con questo testo, è > o < della massima altezza possibile
    var altezzaCorrente=$("#textContainer")[0].clientHeight;
    if (altezzaCorrente>altezzaTesto)
    {
      //limito
      $("#casellaTesto").css("height",altezzaTesto+"px");
      posizione=(altezzaDisponibile*0.1) + altezzaHeader;
    }
    else
    {
      $("#casellaTesto").css("height",altezzaCorrente+"px");
      //ricalcolo
      var altezzaCorrente2=$("#textContainer")[0].clientHeight;
      posizione=(altezza-altezzaHeader-altezzaFooter-altezzaCorrente2)/2 + altezzaHeader;
    }
    $("#textContainer").css("top",posizione+"px");
  }

  //immagine scaricabile
  if ($("#imageContainer").is(":visible"))
  {
    //metto il contenitore alla grandezza di default
    $("#imageContainer").css("width","90%").css("left","5%");
    // e anche l'immagine interna
    $("#imageContainerImg").css("width","100%");
    $("#imageContainerImg").css("height","initial");

    var altezzaHeader = $("#header").height();
    var altezzaFooter = $("#footerMsg").height();

    var altezzaImmagineMax=(altezza-altezzaHeader-altezzaFooter)*0.8; //altezza massima immagine
    //console.log("altezza massima: " + altezzaImmagineMax);

      //l'immagine riesce ad entrare nell'altezza massima?
    var altezzaImg=$("#imageContainerImg")[0].height;
    //console.log("altezza effettiva immagine: " + altezzaImg);

    var posizione;
    if (altezzaImg<altezzaImmagineMax)
    {
      //tutto ok, la devo solo centrare
      posizione=(altezza-altezzaHeader-altezzaFooter-altezzaImg)/2 + altezzaHeader;
    }
    else
    {
      //deve comandare l'altezza massima
      $("#imageContainerImg").css("height",altezzaImmagineMax+"px").css("width","initial");
      //vedo la larghezza effettiva
      var larghImg=$("#imageContainerImg")[0].width;
      // e setto il contenitore
      $("#imageContainer").css("width",larghImg+"px").css("left",((larghezza-larghImg)/2)+"px");
      posizione=(altezza-altezzaHeader-altezzaFooter-altezzaImmagineMax)/2 + altezzaHeader;
      altezzaImg=$("#imageContainerImg")[0].height;

    }
    $("#imageContainer").css("top",posizione+"px");

    //aggiusto l'icona di download
    var altezzaIcona=$("#iconaScarica")[0].clientWidth; //è più o meno quadrata... faccio la larghezza perchè magari non l'ha ancora caricata
    $("#iconaScarica").css("top",((altezzaImg-altezzaIcona)/2)+"px");
  }

  //panorama
  if ($("#panoContainer").is(":visible"))
  {
    //metto il contenitore alla grandezza di default
    $("#panoContainer").css("width","100%").css("left","0%");
    $("#panoContainer").css("height",altezza+"px").css("top","0%");
    //aggiusto il pulsante
    var offset=altezza/30;
    if (altezza>larghezza) offset=larghezza/30;
    $("#panoContainer").children("img").css("top",offset+"px").css("right",offset+"px");
    panoViewer.setSize({height: altezza, width: larghezza});
  }

  //slideShow
  if ($("#swiperDiv").is(':visible'))
  {
    var larghezza=$( window )[0].innerWidth;
    var altezza=$( window )[0].innerHeight;
    var altezzaHeader = $("#header").height();
    var altezzaFooter = $("#footerMsg").height();

    var larghezzaSlider;
    var altezzaSlider;
    var top;
    var left;

    if (fullscreenSwiper==false)
    {
      if (larghezza>altezza) //landscape
      {
        larghezzaSlider=larghezza*0.8;
        altezzaSlider=(altezza-altezzaHeader)*0.8;
        top=(altezza-altezzaSlider + altezzaHeader)/2;
        left=larghezza*0.1;
      }
      else //portrait
      {
        larghezzaSlider=larghezza*0.8;
        altezzaSlider=(altezza-altezzaHeader)*0.4;
        top=(altezza-altezzaSlider + altezzaHeader)/2;
        left=larghezza*0.1;
      }

      $( ".swiper-container" ).css("position","absolute").css("top",top+"px").css("left",left+"px").css("border","1px solid red").css("width",larghezzaSlider+"px").css("height",altezzaSlider+"px").css("border","2px solid "+oggetto.coloreGenerale).css("margin","0px");
      $("#closeSwiper").css("top",(top-24)+"px").css("right",(left-32)+"px").css("z-index","9999").show();
      $("#fullscreenSwiper").css("top",(top+altezzaSlider-28)+"px").css("right",(left-2)+"px").css("z-index","9999");

      setTimeout(function() {

        $( ".swiper-wrapper div img" ).each(function( index ) {
          //console.log(index + ": " + $( this ));
          //console.log($( this ));
          if ($( this )[0].clientHeight>(altezzaSlider*0.9))
          {
            //console.log("entrato nel primo if");
            $( this ).css("width","initial").css("height","90%");
          }
          else if ($( this )[0].clientWidth>(larghezzaSlider*0.9))
          {
            //console.log("entrato nel secondo if");
            $( this ).css("width","90%").css("height","initial");
          }
        });
      }, 1000);
    }
    else //fullscreen
    {
      larghezzaSlider=larghezza-4;
      altezzaSlider=altezza-4;
      top=0;
      left=0;

      $( ".swiper-container" ).css("position","absolute").css("top",top+"px").css("left",left+"px").css("width",larghezzaSlider+"px").css("height",altezzaSlider+"px").css("border","2px solid "+oggetto.coloreGenerale).css("margin","0px");
      $("#closeSwiper").css("top",(top-5)+"px").css("right",(left-11)+"px").css("z-index","9999");
      //$("#fullscreenSwiper").css("top",(top+altezzaSlider-28)+"px").css("right",(left-2)+"px").css("z-index","9999");

      
      $( ".swiper-wrapper div img" ).each(function( index ) {
        $( this ).css("opacity","0.0"); //in modo da renderle invisibili mentre faccio il resize
      });
      

      setTimeout(function() {

        $( ".swiper-wrapper div img" ).each(function( index ) {
          //console.log(index + ": " + $( this ));
          //console.log($( this ));
          //console.log("clientHeight: "+$( this )[0].clientHeight);
          //console.log("clientWidth: "+$( this )[0].clientWidth);
          //console.log("altezzaSlider: "+altezzaSlider);
          //console.log("larghezzaSlider: "+larghezzaSlider);
          var widthOriginal=$( this )[0].naturalWidth;
          var heightOriginal=$( this )[0].naturalHeight;
          //console.log("naturalWidth: "+widthOriginal);
          //console.log("naturalHeight: "+heightOriginal);
          //provo a mettere l'altezza come l'altezza del device, e vedo cosa esce con la larghezza
          var rapporto=altezzaSlider/heightOriginal;
          //console.log("rapporto: "+rapporto);
          //in larghezza entrerebbe?
          //console.log("la larghezza sarebbe: "+widthOriginal*rapporto);
          if ((widthOriginal*rapporto)<=larghezzaSlider)
          {
            //ok, va bene
            //console.log("entra");
            $( this ).css("width","initial").css("height","100%");
          }
          else
          {
            //non va bene
            //console.log("Non entra");
            //faccio allora comandare la larghezza
            $( this ).css("width","100%").css("height","initial");
          }
          $( this ).css("opacity","1.0");
        });
      }, 100);


      setTimeout(function() {

        $( ".swiper-wrapper div img" ).each(function( index ) {
          var widthOriginal=$( this )[0].naturalWidth;
          var heightOriginal=$( this )[0].naturalHeight;
          var rapporto=altezzaSlider/heightOriginal;
          if ((widthOriginal*rapporto)<=larghezzaSlider)
          {
            $( this ).css("width","initial").css("height","100%");
          }
          else
          {
            $( this ).css("width","100%").css("height","initial");
          }
          $( this ).css("opacity","1.0");
        });
      }, 1000);


      //console.log("chiudo lo swiper");
      //closeSwiper();
      //creaSlideshowNew(arrayImagesSwiper, true);
    }

  }

}


function creaPopupSceltaContenuto (tipo, config, descrizione, immagine, punto)
{
  $("#infoText").html(descrizione);

  //devo mettere i pulsanti, a seconda dei contenuti che ha scelto
  var html="";
  var labelTemp="";
  var icon="";
  
  if (tipo==RA_POI) //aggiungo il "naviga verso di default"
  {
    html+="<div class='buttonPoi ui-btn ui-shadow' style='float:left;' onClick='navigaVerso("+punto+");'><i class='menuIconAwesome fa fa-map-o' aria-hidden='true' style='margin-right: 20px;'></i>"+getLabel("NAVIGA_VERSO","it","Naviga verso")+"....</div>";
  }

  for (var i=0;i<config.length;i++)
  {
    var label="";
    if (typeof config[i].label != 'undefined') label=config[i].label;

    if (config[i].tipo==1) //video
    {
      labelTemp="Video";
      icon="fa fa-video";
    }
    else if (config[i].tipo==2) //slideshow
    {
      labelTemp="Slideshow";
      icon="far fa-image";
    }
    else if (config[i].tipo==3) //testo
    {
      labelTemp="Testo";
      icon="far fa-file-alt";
    }
    else if (config[i].tipo==5) //Elemento scaricabile
    {
      labelTemp="Scarica";
      icon="fa fa-download";
    }
    else if (config[i].tipo==6) //link esterno
    {
      labelTemp="Link";
      icon="fa fa-globe";
    }
    else if (config[i].tipo==1001) //votazione, gradimento
    {
      labelTemp="Gradimento";
      icon="far fa-star";
    }
    else if (config[i].tipo==1002) //commento
    {
      labelTemp="Commento";
      icon="far fa-comments";
    }
    else if (config[i].tipo==1003) //social
    {
      labelTemp="Social";
      icon="fa fa-share-alt";
    }

    if (typeof config[i].label != 'undefined') label=config[i].label;
    else label= labelTemp;

    if (tipo==RA_GENERALE)
    {
      html+="<div class='buttonPoi ui-btn ui-shadow' style='float:left;' onClick='showContenutoExecute("+immagine+","+punto+","+i+");'><i class='"+icon+"' aria-hidden='true' style='margin-right: 20px;'></i>"+label+"</div>";
    }
    else
    {
      html+="<div class='buttonPoi ui-btn ui-shadow' style='float:left;' onClick='mostraContenutoPoi("+punto+","+i+");'><i class='menuIconAwesome "+icon+"' aria-hidden='true' style='margin-right: 20px;'></i>"+label+"</div>";
    }


  }
  html+="<div style='clear:both;'></div>";

  $("#infoPulsanti").html(html);
  $("#infoContainer").show();
  resizeBasedOnResolution(tipo);

}

function disabilitatutto()
{
  console.log("sono in disabilitatutto");
  for (i=0;i<arrayOverlay.length;i++) arrayOverlay[i].enabled=false;
}


function nasconditutto()
{
  console.log("sono in nasconditutto");
  for (i=0;i<arrayOverlay.length;i++) arrayOverlay[i].opacity=0;
}

function mostratutto()
{
  console.log("sono in mostratutto");
  for (i=0;i<arrayOverlay.length;i++) arrayOverlay[i].opacity=1;
}


function backUtils(tipo) //quando si chiudono elementi come video o slideshow, o il contenitore di contenuti
{
  mostratutto();
  //ha premuto back... cosa ci stava sullo schermo? un video o lo slideshow?
  if ($("#swiperDiv").is(':visible'))
  {
    closeSwiper();
  }

  if ($("#panoContainer").is(':visible'))
  {
    panoViewer.destroy();
    $("#panoContainer").hide();

    setTimeout(function() {
      AR.hardware.camera.enabled = true;
    }, 500);
  }

  var htmlToAdd="";
  $("#videoContainer").html(htmlToAdd);
  $("#videoContainer").hide();

  $("#infoPulsanti").html(htmlToAdd);
  $("#infoContainer").hide();

  //in caso fosse visibile
  $('#bottoneScarica').click(function() { return false; });
  $("#bottoneScarica").hide();
  $("#sfondoAlfato").hide();

  //$("#image").attr("src","");
  $("#imageContainer").html(htmlToAdd);
  $("#imageContainer").hide();
  $("#textContainer").hide();

  $("#backButton").show();

  contenutoScelto=false;

  //sono uscito da un contenuto.... se correntemente ci sta un elemento riconosciuto, non faccio niente
  //altrimenti metto la barra di scansione
  if (tipo==RA_GENERALE)
  {
    if (pagineRiconosciute.length==0) startAnimationBar(RA_GENERALE);
  }
}

function goTo(target)
{
  if (target=="home")
  {
    //cancello tutte le eventuali classi dinamiche instanziate
    if (arrayDynamicClasses!= null)
    {
      if (arrayDynamicClasses.length>0)
      {
        dynamicPosClass = null;
        delete dynamicPosClass;
      }
      for (var i=0;i<arrayDynamicClasses.length;i++)
      {
        //console.log("cancello la classe "+ arrayDynamicClasses[i].chiSono());
        //arrayDynamicClasses[i] = null;
        delete arrayDynamicClasses[i];
      }
      delete arrayDynamicClasses;
    }
    console.log("esco");
    $("#loader").show();
    setTimeout(function() {
      exitRA();
    }, 1000);
  }
}

function closeSwiper()
{
  console.log("elimino lo swiper");
  if (swiper!=null)
  {
    swiper.removeAllSlides();
    swiper.destroy(true, true);
    swiper = null;
  }
  $( "#swiperDiv" ).hide();
}

function addLabels2Poi(labels, oggetto)
{
  for (var i=0;i<labels.length;i++)
  {
    var label = new AR.Label(labels[i].text, 0.5, {
        scale: 1,
        enabled: true,
        opacity : 0.5,
        zOrder: 15,
        style : {
            textColor : labels[i].color,
            backgroundColor : '#FFFFFF'
        }
    });
    label.onClick = MarkerIndoor.prototype.mostraContenuti(oggetto.poiData.id);
    label.customType=-1;

    var offsetGeneraleInizialeX=0;
    var offsetGeneraleInizialeY=0;

    if (oggetto.poiData.labelPosition=="up")
    {
        // a seconda dell'indice lo devo mettere più o meno sopra
        label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
        label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
        label.translate.x=offsetGeneraleInizialeX;
        label.translate.y=offsetGeneraleInizialeY+(oggetto.poiData.dimensioneIndicatore*0.6)+(0.6*(labels.length-i-1));
    }
    else if (oggetto.poiData.labelPosition=="down")
    {
        label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
        label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
        label.translate.x=offsetGeneraleInizialeX;
        label.translate.y=offsetGeneraleInizialeY-(oggetto.poiData.dimensioneIndicatore*0.6)-(0.6*(labels.length-i-1));
    }
    else if (oggetto.poiData.labelPosition=="sx")
    {
        label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
        label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
        label.translate.x=offsetGeneraleInizialeX-(oggetto.poiData.dimensioneIndicatore*0.6);
        label.translate.y=offsetGeneraleInizialeY;
    }
    else if (oggetto.poiData.labelPosition=="dx")
    {
        var displY=0;
        if (labels.length % 2 == 0) //pari
        {
          var centralElement=parseInt(labels.length/2); 
          displY=0.6*(labels.length-1-centralElement-i)+0.3;
        }
        else
        {
          var centralElement=parseInt(labels.length/2); 
          displY=0.6*(labels.length-1-centralElement-i);
        }
        label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
        label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
        label.translate.x=offsetGeneraleInizialeX+(oggetto.poiData.dimensioneIndicatore*0.6);
        label.translate.y=offsetGeneraleInizialeY+displY;
    }
    else if (oggetto.poiData.labelPosition=="upDx")
    {
        label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
        label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
        label.translate.x=offsetGeneraleInizialeX+(oggetto.poiData.dimensioneIndicatore*0.6);
        label.translate.y=offsetGeneraleInizialeY+(oggetto.poiData.dimensioneIndicatore*0.6)+(0.6*(labels.length-i-1));
    }
    else if (oggetto.poiData.labelPosition=="downDx")
    {
        label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
        label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
        label.translate.x=offsetGeneraleInizialeX+(oggetto.poiData.dimensioneIndicatore*0.6);
        label.translate.y=offsetGeneraleInizialeY-(oggetto.poiData.dimensioneIndicatore*0.6)-(0.6*(labels.length-i-1));
    }
    else if (oggetto.poiData.labelPosition=="downSx")
    {
        label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
        label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
        label.translate.x=offsetGeneraleInizialeX-(oggetto.poiData.dimensioneIndicatore*0.6);
        label.translate.y=offsetGeneraleInizialeY-(oggetto.poiData.dimensioneIndicatore*0.6)-(0.6*(labels.length-i-1));
    }
    else if (oggetto.poiData.labelPosition=="upSx")
    {
        label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
        label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
        label.translate.x=offsetGeneraleInizialeX-(oggetto.poiData.dimensioneIndicatore*0.6);
        label.translate.y=offsetGeneraleInizialeY+(oggetto.poiData.dimensioneIndicatore*0.6)-(0.6*(labels.length-i-1));
    }
    else //di default a destra
    {
        label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
        label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
        label.translate.x=offsetGeneraleInizialeX+(oggetto.poiData.dimensioneIndicatore*0.6);
        label.translate.y=offsetGeneraleInizialeY;
    }
    oggetto.markerObject.drawables.addCamDrawable(label); 
  }

}

function modificaLabel (immagine, punto, testo)
{
  console.log("ModificaLabel - immagine: " + immagine + " - punto: " + punto + " - testo: " + testo);

  for (i=0;i<arrayOverlay.length;i++)
  {
    if ((arrayOverlay[i].immagineRef==immagine) && (arrayOverlay[i].puntoRef==punto))
    {
      arrayOverlay[i].text=testo; 
      console.log("Aggiornata label con testo: " + testo);
    }
  }

}

function accendiLed (daAccendere)
{
  if (daAccendere)
  {
    $("#lampSpenta").hide();
    $("#lampAccesa").show();
    AR.hardware.camera.flashlight = true;
  }
  else
  {
    $("#lampSpenta").show();
    $("#lampAccesa").hide();
    AR.hardware.camera.flashlight = false;

  }
}

function pausaAudio ()
{
    document.getElementById("audio").pause();
    $("#audioOn").hide();
    $("#audioPause").show();
}

function startAudio ()
{
    document.getElementById("audio").play();
    $("#audioOn").show();
    $("#audioPause").hide();
}
