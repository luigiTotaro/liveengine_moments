angular.module('LiveEngine.Wiki.Services', [])

.service('WikiService', function(LoggerService, HelperService, $rootScope, $http, $ionicModal, $ionicPopup, $ionicLoading, GlobalVariables, FileSystemService, SupportServices, LanguageService, LazyLoader, WikiService_configurable, $state) {

    var me = this;

    me.toDownload = new Array();

    me.callbacks = [];


    me.registerCallback = function(key, functionName, callback) {

        if(!me.callbacks[key]) {
            me.callbacks[key] = [];
        }

        delete me.callbacks[key][functionName];

        if(callback) {
            me.callbacks[key][functionName] = callback;
        }

    }

    me.setupCordova = function ()
    {

      if(typeof window.cordova == "undefined") return true;

      try{
        GlobalVariables.wikitudePlugin = cordova.require("com.wikitude.phonegap.WikitudePlugin.WikitudePlugin");
        return true;
      }
      catch(err){
        console.log(err);
        return false;

      }
    };


    me.sendMessageToAR = function(json) {
        var jsonString = JSON.stringify(json);
        //console.log("sendMessageToAR: " + jsonString);
        jsonString = jsonString.replace(/'/g, "|");   

        var encodedJson = encodeURIComponent(jsonString);
        var funzione = "messageForAR('" + encodedJson + "')";
        //console.log("funzione: " + funzione);

        GlobalVariables.wikitudePlugin.callJavaScript(funzione);
    };


    me.parseWikitudeCallback = function(url)
    {
        var ret = new Object();
        ret.value="";
        
        console.log("parseWikitudeCallback: "+ url);
        var parsedUrl= encodeURI(url.substring(15));
        /*
        if (parsedUrl.substring(0,9)=="copertina")
        {
            ret.value="copertina";    
            ret.parameter = parsedUrl.substring(9);
        }
        else if (parsedUrl.substring(0,9)=="nonRilevo")
        {
            ret.value="nonRilevo";    
        }
        */
        if (parsedUrl.substring(0,7)=="storage")
        {
            //ret.value="storage";    
        }
        else if (parsedUrl.substring(0,15)=="scaricaImmagine")
        {
            /*
            ret.value="scaricaImmagine";    
            var value=parsedUrl.substring(16);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        */
        }
        else if (parsedUrl.substring(0,11)=="salvaConfig")
        {
            /*
            ret.value="salvaConfig";    
            var value=parsedUrl.substring(12);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
            */
        }
        else if (parsedUrl.substring(0,11)=="linkEsterno")
        {
            /*
            ret.value="linkEsterno";    
            var value=parsedUrl.substring(12);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=value;
        */
        }
        else if (parsedUrl.substring(0,16)=="checkConnessione")
        {
            //ret.value="checkConnessione";    
        }
        else if (parsedUrl.substring(0,14)=="getUsersNumber")
        {
            //ret.value="getUsersNumber";    
        }
        else if (parsedUrl.substring(0,16)=="scaricaSlideshow")
        {
            /*
            ret.value="scaricaSlideshow";    
            var value=parsedUrl.substring(17);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
            */
        }
        else if (parsedUrl.substring(0,14)=="existSlideshow")
        {
            /*
            ret.value="existSlideshow";    
            ret.parameter=parsedUrl.substring(15);
            */
        }
        else if (parsedUrl.substring(0,11)=="openContent")
        {
            /*
            ret.value="openContent";    
            var value=parsedUrl.substring(12);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        */
        }
        else if (parsedUrl.substring(0,13)=="socialPublish")
        {
            /*
            ret.value="socialPublish";    
            var value=parsedUrl.substring(14);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        */
        }
        else if (parsedUrl.substring(0,12)=="writeComment")
        {
            /*
            ret.value="writeComment";    
            var value=parsedUrl.substring(13);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
            */
        }        
        else if (parsedUrl.substring(0,6)=="naviga")
        {
            
            /*ret.value="naviga";    
            var value=parsedUrl.substring(7);
            value=decodeURIComponent(value);
            value=decodeURIComponent(value);
            ret.parameter=JSON.parse(value);
        */
        }
        else if (parsedUrl.substring(0,13)=="messageFromAr")
        {
            /*
            ret.value="messageFromAr";    
            var value=parsedUrl.substring(13);
            console.log("messaggio1: " + value);
            value=decodeURIComponent(value);
            console.log("messaggio2: " + value);
            value=decodeURIComponent(value);
            console.log("messaggio3: " + value);
            ret.parameter=JSON.parse(value);
        */
        }
        else if (parsedUrl.substring(0,9)=="startFind")
        {
            ret.value="startFind";    
        }
        else if (parsedUrl.substring(0,7)=="endFind")
        {
            ret.value="endFind";    
        }
        else if (parsedUrl.substring(0,7)=="abandon")
        {
            ret.value="abandon";    
        }
        return ret;
    }

  
    me.openArForGame = function(game_key, dati)
    {
        var wtcName=dati.wtc;
        console.log("wtcName: " + wtcName);

        if (wtcName==null)
        {
            console.log("Attenzione, i contenuti per questo gioco non sono stati pubblicati. Riprovare in seguito, grazie");
            HelperService.showUnrecoverableError("Attenzione, i contenuti per questo gioco non sono stati pubblicati.<br>Riprovare in seguito, grazie", true);
            $ionicLoading.hide();
            return;
        }

        var labels="";
        if (typeof dati.labels == "undefined") labels="|||";
        else
        {
            if (typeof dati.labels.abbandona != "undefined") labels+=dati.labels.abbandona;
            labels+="|";
            if (typeof dati.labels.conferma_abbandona != "undefined") labels+=dati.labels.conferma_abbandona;
            labels+="|";
            if (typeof dati.labels.conferma_abbandona_si != "undefined") labels+=dati.labels.conferma_abbandona_si;
            labels+="|";
            if (typeof dati.labels.conferma_abbandona_no != "undefined") labels+=dati.labels.conferma_abbandona_no;
        }
        //queste labels potrebbero avere un ' che fa incasinare tutto, devo bonificarlo....
        console.log("Labels prima: " + labels);
        labels= labels.replace(/\'/g,'***');
        console.log("Labels dopo: " + labels);

        //debug
        //labels="vattene via|sei sicuro di volere abbandonare?ma proprio proprio??|si, me ne vado|no, rimango";

        var uri = encodeURI(GlobalVariables.baseUrl + "/"+wtcName);
        var name = wtcName.substring(16);
        var localpath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;
        var fullName=wtcName;

        var parameter = { "path": "www/clientRecognition/indexConfigurable.html", "requiredFeatures": ["image_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localpath, "wtc":fullName, "game_key": game_key, "labels": labels, "language": GlobalVariables.systemLanguage};

        //è presente la connessione?
        console.log("Check connessione: " + HelperService.isNetworkAvailable());
        if (HelperService.isNetworkAvailable()==false)
        {
            console.log("Nessuna connessione, vedo se il file è comunque presente");
            FileSystemService.fileExists({
                directory: SupportServices.BUFFER_DIRECTORY + '/wtcConfigurable',
                fileName: name,
                onComplete: function(err, isAvailable) {

                    if(err) {
                        //errore, getto la spugna
                        console.log("Errore, esco");
                        HelperService.showUnrecoverableError("Non riesco a scaricare i dati, possibile problema di connessione.<br>Riprovare in seguito.", true);

                    } else {

                        if(!isAvailable) {
                             //non esiste, getto la spugna
                            console.log("Non Esiste, esco");
                            HelperService.showUnrecoverableError("Non riesco a scaricare i dati, possibile problema di connessione.<br>Riprovare in seguito.", true);

                        } else {
                            //esiste, vado avanti
                            console.log("Esiste, vado avanti");
                            console.log(JSON.stringify(parameter));
                            WikiService_configurable.setMyParent(me);
                            WikiService_configurable.loadARchitectWorld(parameter);
                        }
                    }
                }
            });
        }
        else
        {

            //console.log("Provo a scaricare " + uri);

            FileSystemService.downloadFile({
                url: uri,
                directory: SupportServices.BUFFER_DIRECTORY + '/wtcConfigurable',
                fileName: name,
                onComplete: function(err) {

                    if(err) {
                      console.log("download error: " + err);
                      console.log("Controllo se è comunque presente sul device");
                      //non è riuscito, vedo se ho comunque il file sul device
                      FileSystemService.fileExists({
                          directory: SupportServices.BUFFER_DIRECTORY + '/wtcConfigurable',
                          fileName: name,
                          onComplete: function(err, isAvailable) {

                              if(err) {
                                  //errore, getto la spugna
                                  console.log("Errore, esco");
                                  HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);

                              } else {

                                  if(!isAvailable) {
                                       //non esiste, getto la spugna
                                      console.log("Non Esiste, esco");
                                      HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);

                                  } else {
                                      //esiste, vado avanti
                                      console.log("Esiste, vado avanti");
                                       console.log(JSON.stringify(parameter));
                                      WikiService_configurable.setMyParent(me);
                                      WikiService_configurable.loadARchitectWorld(parameter);
                                  }
                              }
                          }
                      });

                    } else {

                      console.log("download completato");
                      console.log(JSON.stringify(parameter));
                      WikiService_configurable.setMyParent(me);
                      WikiService_configurable.loadARchitectWorld(parameter);
                    }
                }
            });
        }

    }

    me.downloadFiles = function(index,idPunto)
    {
        console.log("sono in downloadFiles, con index: " + index + " e idPunto: ", idPunto);

        var path = me.toDownload[index];
        var name = idPunto+"_"+index;
        var dir = "slideshow";
        var uri = encodeURI(path);
        var localpath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;

        console.log("Provo a scaricare " + uri);

        FileSystemService.downloadFile({
            url: uri,
            directory: SupportServices.BUFFER_DIRECTORY + '/' + dir,
            fileName: name,
            onComplete: function(err) {

                if(err) {
                    console.log("download error: " + err);
                    //provo comunque ad andare avanti
                    index++;
                    if (index<me.toDownload.length)
                    {
                    //console.log("index: " + index);
                    me.downloadFiles(index,idPunto);
                    }
                    else
                    {
                    console.log("Finito");
                    }


                } else {

                    console.log("download completato");
                    index++;
                    if (index<me.toDownload.length)
                    {
                    //console.log("index: " + index);
                    me.downloadFiles(index,idPunto);
                    }
                    else
                    {
                    console.log("Finito");
                    }

                }
            }
        });
    }


    me.downloadPanoFile = function(fileName, fileToSave)
    {
        console.log("sono in downloadPanoFile, con fileName: " + fileName + " e fileToSave: " + fileToSave);

        var path = fileName;
        /*
        var pathSplit= path.split("/");
        //il nome è l'ultimo elemento
        var name=pathSplit[pathSplit.length-1];
        */
        var name=fileToSave;
        var dir = "pano";
        var uri = encodeURI(path);
        var localpath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;

        console.log("Provo a scaricare " + uri);

        FileSystemService.downloadFile({
            url: uri,
            directory: SupportServices.BUFFER_DIRECTORY + '/' + dir,
            fileName: name,
            onComplete: function(err) {

                var ret="";
                if(err) {
                    console.log("download error: " + err);
                    //ritorno alla RA con un errore
                    ret= "panoReturn(false,null)";
                } else {
                    console.log("download completato");
                    ret= "panoReturn(true,'"+name+"')";
                }
                console.log("funzione di ritorno: " + ret)
                GlobalVariables.wikitudePlugin.callJavaScript(ret);

            }
        });
    }


    me.showPopupReturn2AR = function(social)
    {
        $rootScope.closePopupScreen = function() {
            $rootScope.popupScreen.remove();
            GlobalVariables.wikitudePlugin.show();
        };

        $rootScope.popupScreen = $ionicModal.fromTemplate(


          '<ion-modal-view padding="true" cache-view="false" id="appCreditsScreen" style="width: 100%;height: 100%;top: 0px;left: 0px;">' +

              '<ion-content scroll="true" padding="false" >' +

                  '<div id="logoSocialBox">' +

                  (social == "fb" ? '<img src="img/fbLogo.png" style="width:100%;"></img>' : '') +
                  (social == "tw" ? '<img src="img/twitterLogo.png" style="width:100%;"></img>' : '') +
                  (social == "insta" ? '<img src="img/instagramLogo.png" style="width:100%;"></img>' : '') +
                  
                  (social == "insta_error" ? '<img src="img/instagramLogo.png" style="width:100%;"></img>' : '') +
                  (social == "tw_error" ? '<img src="img/twitterLogo.png" style="width:100%;"></img>' : '') +
                  (social == "fb_error" ? '<img src="img/fbLogo.png" style="width:100%;"></img>' : '') +

                  '</div>'+

                  '<div id="socialInfoBox" style="">' +
                  	(social == "insta_error" ? LanguageService.getLabel('SOCIAL_INSTAGRAM_ERROR')+'<br><br>' : '') +
                  	(social == "tw_error" ? LanguageService.getLabel('SOCIAL_TWITTER_ERROR')+'<br><br>' : '') +
                  	(social == "fb_error" ? LanguageService.getLabel('SOCIAL_FB_ERROR')+'<br><br>' : '') +

                  	'<strong>'+LanguageService.getLabel('RETURN_TO_RA')+'</strong>' +

                  '</div>'+

              '</ion-content>' +


              '<ion-footer-bar align-title="left" class="splashScreenFooter">' +
              	'<button ng-click="closePopupScreen()" class="button button-full button-positive cst-button" style="background-color: #b41223;margin-top: auto;">'+LanguageService.getLabel('CHIUDI')+'</button>' +
              '</ion-footer-bar>' +

          '</ion-modal-view>',

          {
              scope: $rootScope,
              focusFirstInput: true,
              animation :'none',
              hardwareBackButtonClose: false
          }

        );

        $rootScope.popupScreen.show();
    };

    me.showPopupWriteComment = function(oggetto)
    {

        $rootScope.comment_data = {
          tmp_nickname: oggetto.nickname,
          tmp_comment: "",
          show_warn_nickname: false,
          show_warn_comment: false,
          show_warn_nickname_comment: false
        };

        $rootScope.inviaCommento = function() {
            //controllo il nickname e il commento
            
            //tolgo i ritorni a capo
            var commento=$rootScope.comment_data.tmp_comment;
            console.log($rootScope.comment_data.tmp_comment);
            $rootScope.comment_data.tmp_comment = $rootScope.comment_data.tmp_comment.replace(/(\r\n|\n|\r)/gm," ");
            console.log($rootScope.comment_data.tmp_comment);

            var reg = /[^-A-Za-z0-9 .,!?&:_@$^;|<>"'=+*°#()èéòàùìÈÉÀÁÌÍÒÓÙÚ]/;
            var testNick = reg.test($rootScope.comment_data.tmp_nickname);    
            var testCommento = reg.test($rootScope.comment_data.tmp_comment);    
            
            $rootScope.comment_data.show_warn_nickname_comment=false;
            $rootScope.comment_data.show_warn_nickname=false;
            $rootScope.comment_data.show_warn_comment=false;

            if ((testNick==true) && (testCommento==true))
            {
                console.log("caratteri non ammessi nel nickname e nel commento");
                $rootScope.comment_data.show_warn_nickname_comment=true;
                setTimeout(function() {
                    $rootScope.comment_data.show_warn_nickname_comment=false;
                }, 4000);       
              return;
            }        
            if (testNick==true)
            {
                console.log("caratteri non ammessi nel nickname");
                $rootScope.comment_data.show_warn_nickname=true;
                setTimeout(function() {
                    $rootScope.comment_data.show_warn_nickname=false;
                }, 4000);       
              return;
            }        
            if (testCommento==true)
            {
                console.log("caratteri non ammessi nel commento");
                $rootScope.comment_data.show_warn_comment=true;
                setTimeout(function() {
                    $rootScope.comment_data.show_warn_comment=false;
                }, 4000);       
              return;
            } 
            
            var return_oggetto = new Object();
            return_oggetto.type = "SEND";
            return_oggetto.nick = $rootScope.comment_data.tmp_nickname;
            return_oggetto.commento = $rootScope.comment_data.tmp_comment;

            $rootScope.popupScreen.remove();
            //alert($rootScope.comment_data.tmp_nickname);
            //alert($rootScope.comment_data.tmp_comment);

            var jsonString = JSON.stringify(return_oggetto);
            //tolgo eventuali apici, sostituendoli...
            jsonString = jsonString.replace(/'/g, "|");   

            var encodedJson = encodeURIComponent(jsonString);
            var funzione = "commentReturn('" + encodedJson + "')";
            //console.log("funzione: " + funzione);

            GlobalVariables.wikitudePlugin.show();
            GlobalVariables.wikitudePlugin.callJavaScript(funzione);
            
        };

        $rootScope.annullaCommento = function()
        {
            var return_oggetto = new Object();
            return_oggetto.type = "CANCEL";
            return_oggetto.nick = null;
            return_oggetto.commento = null;

            $rootScope.popupScreen.remove();

            var jsonString = JSON.stringify(return_oggetto);
            var encodedJson = encodeURIComponent(jsonString);
            var funzione = "commentReturn('" + encodedJson + "')";
            //console.log("funzione: " + funzione);

            GlobalVariables.wikitudePlugin.show();
            GlobalVariables.wikitudePlugin.callJavaScript(funzione);    
        };

        $rootScope.popupScreen = $ionicModal.fromTemplate(


          '<ion-modal-view padding="true" cache-view="false" id="appCommentScreen" style="width: 100%;height: 100%;top: 0px;left: 0px;">' +

              '<ion-content scroll="true" padding="false" >' +


        	    '<p style="color:#000000;text-align:left;margin-left:5%;">'+LanguageService.getLabel('SOCIAL_COMMENTA_INSERISCI')+'</p>' + 
        	    (oggetto.nickname == "" ? '<p style="color:#000000;text-align:left;margin-left:5%;">'+LanguageService.getLabel('SOCIAL_COMMENTA_NICKNAME')+'</p>' : '') +
        	    '<p style="color:#000000;text-align:left;margin-left:5%;">'+LanguageService.getLabel('NICKNAME')+'</p>' + 
        	    '<input type="text" ng-model="comment_data.tmp_nickname" id="comment_nickname" maxlength="20" style="border:1px solid;color:#000000;text-align:left;margin-left:5%;width:50%;" placeholder="" value="'+oggetto.nickname+'">' + 
        	    '<p style="color:#000000;text-align:left;margin-left:5%;">'+LanguageService.getLabel('COMMENTO')+'</p>' + 
        	    '<textarea rows="10" ng-model="comment_data.tmp_comment" style="color:#000000;text-align:left;margin-left:5%;width:90%;border:1px solid black;" placeholder="" value=""></textarea>' +

    	 	       '<div ng-show="comment_data.show_warn_nickname" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>'+LanguageService.getLabel('SOCIAL_COMMENTO_ERRORE_NICK')+'</p></div>' +
    		        '<div ng-show="comment_data.show_warn_comment" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>'+LanguageService.getLabel('SOCIAL_COMMENTO_ERRORE_COMMENTO')+'</p></div>' +
   	 	        '<div ng-show="comment_data.show_warn_nickname_comment" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>'+LanguageService.getLabel('SOCIAL_COMMENTO_ERRORE_COMMENTO_NICK')+'</p></div>' +


              '</ion-content>' +




              '<div class="row" style="position:absolute; bottom:5px; padding:0px; left:5px; right:5px; width:auto;">' +

       	     '<div class="col" style="padding: 0px 2.5px 0px 0px;">' +
     	         '<button ng-click="inviaCommento()" class="button button-full button-balanced" style="margin:0px;">'+LanguageService.getLabel('INVIA')+'</button>' +
     	       '</div>' +

    	        '<div class="col" style="padding: 0px 0px 0px 2.5px;">' +
    	            '<button ng-click="annullaCommento()" class="button button-full button-stable" style="margin:0px;">'+LanguageService.getLabel('ANNULLA')+'</button>' +
     	       '</div>' +

              '</div>' +


          '</ion-modal-view>',

          {
              scope: $rootScope,
              focusFirstInput: true,
              animation :'none',
              hardwareBackButtonClose: false
          }

        );

        $rootScope.popupScreen.show();
    };



})
