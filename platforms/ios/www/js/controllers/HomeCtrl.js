angular.module('LiveEngine.Home.Controller', [])


.controller('HomeCtrl', function(GlobalVariables, SupportServices, $scope
, $rootScope, $state, $ionicModal, $http, LazyLoader, HelperService, $ionicLoading
, $ionicPopup, LoggerService, $timeout, FileSystemService, $q, SocketService, $stateParams, $httpParamSerializerJQLike, DeviceLogService, LanguageService, WikiService_copertina, WikiService, GeoLocationService, PushServices, GameCenter) {

  var me = this;

  $scope.data = {
    testate: [],
    labels: {
      title: LanguageService.getLabel('INSERISCI_IL_TUO_CODICE'),
      scanQr: LanguageService.getLabel('HO_QR_CODE'),
      Vai: LanguageService.getLabel('VAI'),
      presenta: LanguageService.getLabel('PRESENTA'),
      apriAlbum: LanguageService.getLabel('APRI_ALBUM'),
      RitornaA: LanguageService.getLabel('RITORNA_A'),
      NonSaiCosaEmoments: LanguageService.getLabel('NON_SAI_COSA_E_MOMENTS')
    },
    timer: null,
    slideTimer: null,
    //dalla pagina TestataDetail, non piu' presente ora
    testata: null,
    disableBackButton: false,
    layout: null,
    layoutCaricato: false,
    classiDinamicheCaricate: false,
    classiDinamicheARCaricate: false,
    numClassiTotaliCaricate: 0,
    numClassiTotaliARCaricate: 0,
    nodeIpAddresses: [],
    myslider: {
      options: null,
      slide_list: []
    },
    splashScreenFotografo: null

  };

  $scope.goAlbum = function(codice) {
      
      
      if ((typeof codice == 'undefined') || (codice == ''))
      {
        //codice="15EM374B6E";
        HelperService.showUnrecoverableError(LanguageService.getLabel('INSERIRE_CODICE_ALBUM'), true);
        return;
      }

      if(!window.cordova) {
          $scope.goAlbum_afterCheck(codice);

      } else {

        //controllo permesso fotocamera ios
        if (ionic.Platform.isIOS())
        {
          cordova.plugins.diagnostic.getCameraAuthorizationStatus(
              function(status){
                      if (status==cordova.plugins.diagnostic.permissionStatus.DENIED)
                      {
                        var messaggio=LanguageService.getLabel('PERMESSO_FOTOCAMERA_REVOCATO');
                        messaggio = messaggio.replace("XXXXXX", "Mediasoft AR");
                        //è stato revocato, dall'utente o per altre ragioni.... devo richiedrlo nuovamente
                        HelperService.showGenericMessage(messaggio, true); 
                        DeviceLogService.log("ERROR","PERMESSO_FOTOCAMERA_REVOCATO");
                      }
                      else
                      {
                        $scope.goAlbum_afterCheck(codice);
                      }
                      //alert("Camera grant is: " + status);
              }, function(error){
                  //faccio partire comunque
                  $scope.goAlbum_afterCheck(codice);
              }, false
          );                     
        }
        else //android
        {
          $scope.goAlbum_afterCheck(codice);
        }
    }
  };

  $scope.goMomentsWebsite = function() {
      if(window.cordova && window.cordova.InAppBrowser) {
          //alert("inapp");
          window.cordova.InAppBrowser.open("https://www.moments-ar.it/", "_system");
      } else {
          //alert("non inapp");
          window.open("https://www.moments-ar.it/",'_blank');
      }    
  };


  $scope.goScanQR = function() {
      
      console.log("Scansiono codice");
      cordova.plugins.barcodeScanner.scan(
          function (result) {
            if(!result.cancelled){
                   // In this case we only want to process QR Codes
                   if(result.format == "QR_CODE"){
                        var value = result.text;
                        //alert("QR Code: " + value);
                        $scope.goAlbum(value);
                   }else{
                      //alert("Sorry, only qr codes this time ;)");
                      HelperService.showGenericMessage(LanguageService.getLabel('NO_MOMENTS_CODE'), true);
                   }
            }else{
              //alert("The user has dismissed the scan");
            }
          },
          function (error) {
              //alert("An error ocurred: " + error);
              HelperService.showGenericMessage(LanguageService.getLabel('ERRORE_SCANSIONE_QRCODE'), true);
          },
          {
              preferFrontCamera : false, // iOS and Android
              showFlipCameraButton : false, // iOS and Android
              showTorchButton : true, // iOS and Android
              torchOn: false, // Android, launch with the torch switched on (if available)
              saveHistory: false, // Android, save scan history (default false)
              prompt : "Inquadra il tuo QR Code", // Android
              resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
              formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
              //orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
              disableAnimations : true, // iOS
              disableSuccessBeep: false // iOS and Android
          }

        /*
        function (result) {
            alert("We got a barcode\n" +
                  "Result: " + result.text + "\n" +
                  "Format: " + result.format + "\n" +
                  "Cancelled: " + result.cancelled);

            $scope.goAlbum
        },
        function (error) {
            alert("Scanning failed: " + error);
        }


           */
      ); 
  };


  $scope.goAlbum_afterCheck = function(codice) {
      //console.log(testata);

      if(!window.cordova) {
          $scope.getAlbumInfo(codice);

      } else {

        cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, function( status ){
          if ( status.hasPermission ) {
            console.log("WRITE_EXTERNAL_STORAGE Yes :D ");
            $scope.getAlbumInfo(codice);
          }
          else {
            console.warn("WRITE_EXTERNAL_STORAGE No :( ");
            var messaggio=LanguageService.getLabel('PERMESSO_SCRITTURA_NEGATO');
            messaggio = messaggio.replace("XXXXXX", "Mediasoft");
            HelperService.showGenericMessage(messaggio, true); 
            DeviceLogService.log("ERROR","PERMESSO_SCRITTURA_NEGATO");
          }
        });
      }
      
  };


  $scope.labelReload = function() {
      console.log("Ricarico le label - Home");

      $scope.data.labels = {
          title: LanguageService.getLabel('INSERISCI_IL_TUO_CODICE'),
          scanQr: LanguageService.getLabel('HO_QR_CODE'),
          Vai: LanguageService.getLabel('VAI'),
          presenta: LanguageService.getLabel('PRESENTA'),
          apriAlbum: LanguageService.getLabel('APRI_ALBUM'),
          RitornaA: LanguageService.getLabel('RITORNA_A'),
          NonSaiCosaEmoments: LanguageService.getLabel('NON_SAI_COSA_E_MOMENTS')
      };
      
  };


  $scope.getAlbumInfo = function(codice) {

      //tutti i check ok, vedo l'id del progetto associato
      console.log("getAlbumInfo: " + codice);

      SupportServices.getAlbumByCode({
          code: codice,
          onComplete: function(err, json) {

              console.log("on complete: " + JSON.stringify(err) + " - " + JSON.stringify(json));
              if(err) {

                  HelperService.showUnrecoverableError(err, true);
                  $ionicLoading.hide();

              } else {

                  $timeout(function() {
                    if (json.errore)
                    {
                      //l'unico errore per adesso ammesso è quello di non trovare l'album

                      HelperService.showUnrecoverableError(LanguageService.getLabel('NESSUN_ALBUM_CODICE'), true);
                      $ionicLoading.hide();                      
                    }
                    else
                    {
                      $scope.data.testata = json.testata;
                      $scope.data.idProgetto = json.idProgetto;
                      $scope.data.tipologia = json.tipo;
                      
                      //memorizzo l'ultimo codice usato
                      window.localStorage.setItem("lastCodiceProgetto", codice);  

                      $scope.gotoDetailPageOK();
                    }
                  });

              }

          }

      });
  };


  $scope.gotoDetailPageOK = function() {


      GlobalVariables.application.currentTestata = $scope.data.testata;
      GlobalVariables.application.applicationUrl = $scope.data.testata.socketUrl;
      //var numeroUtenti=testata.utentiTestata;
      clearInterval($scope.data.timer);
      clearInterval($scope.data.slideTimer);

      //parte spostata dalla pagina TestataDetail
      $ionicLoading.show({
          template: '<div style="padding:10px 5px;">'+LanguageService.getLabel('ATTENDERE')+'</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>'
      });

      /* for testing only  */
      //GlobalVariables.application.applicationUrl = 'http://192.170.5.25:3030/' + GlobalVariables.application.applicationUrl.substring( GlobalVariables.application.applicationUrl.lastIndexOf("/") + 1);

      console.log(GlobalVariables.application.applicationUrl);

      //$scope.data.testata = GlobalVariables.application.currentTestata;
      $scope.data.disableBackButton = GlobalVariables.application.disableBackButtonToHome;

      SupportServices.getLayoutTestata({
          idTestata: $scope.data.testata.idTestata,
          onComplete: function(err, jsonLayout) {

              console.log("on complete di getLayoutTestata");
              console.log(jsonLayout);
              if(err) {

                  HelperService.showUnrecoverableError(err, true);
                  $ionicLoading.hide();

              } else {

                  $timeout(function() {
                    jsonLayout.homeLogo = GlobalVariables.baseUrl + "/" + jsonLayout.homeLogo;
                    jsonLayout.homeSfondo = GlobalVariables.baseUrl + "/" + jsonLayout.homeSfondo;
                    jsonLayout.homeSplash = GlobalVariables.baseUrl + "/" + jsonLayout.homeStart;

                    GlobalVariables.application.currrentLayoutTestata = jsonLayout;
                    $scope.data.layout = jsonLayout;

                    $scope.checkCaricamentoClassi (true, null, null, null,null);
                  });

              }

          }

      });


      SupportServices.getLibrariesToLoad({
          idTestata: $scope.data.testata.idTestata,
          onComplete: function(err, jsonLibs) {
              if(!err) {
                console.log("on complete di getLibrariesToLoad");
                console.log(jsonLibs);

                _.each(jsonLibs, function(libName) {

                      console.log(libName);
                      var className = libName.replace(".js", "");

                      LazyLoader.loadLibrary({

                          fileName: libName,

                          onComplete: function(err) {

                              if(!err) {

                                  try {

                                      console.log("Provo initialize di " + className);
                                      window[className].Initialize({
                                          ionicModal: $ionicModal,
                                          ionicPopup: $ionicPopup,
                                          ionicLoading: $ionicLoading,
                                          http: $http,
                                          rootScope: $rootScope,
                                          GlobalVariables: GlobalVariables,
                                          HelperService: HelperService,
                                          LoggerService: LoggerService,
                                          FileSystemService: FileSystemService,
                                          WikiService: WikiService,
                                          ionicLoading: $ionicLoading,
                                          SocketService: SocketService,
                                          LanguageService: LanguageService,
                                          GameCenter: GameCenter,
                                          RootScope: $rootScope,
                                          LazyLoader: LazyLoader,
                                          myWindow: window,
                                          SerializerJQLike: $httpParamSerializerJQLike
                                      });
                                      
                                      $scope.checkCaricamentoClassi (null, jsonLibs.length, true, null,null);



                                  } catch(err) {

                                    console.log(className + " - error loading " + err);

                                  }

                              }
                          }

                      });

                });
              }
              else
              {
                HelperService.showUnrecoverableError(err, true);
                $ionicLoading.hide();
              }
          }

      });

      SupportServices.getARLibrariesToLoad({
          idTestata: $scope.data.testata.idTestata,
          onComplete: function(err, jsonLibs) {
              if(!err) {
                
                console.log("on complete di getARLibrariesToLoad");
                console.log(jsonLibs);

                _.each(jsonLibs, function(libName) {

                      console.log(libName);
                      var className = libName.replace(".js", "");

                      LazyLoader.loadARLibrary({

                          fileName: libName,

                          onComplete: function(err) {

                              if(!err) {
                                //console.log("caricato: " + libName);
                                $scope.checkCaricamentoClassi (null, null, null, jsonLibs.length, true);
                                //console.log("cazzo...");
                              }
                          }

                      });

                });
              }
              else
              {
                console.log("sono nell'error durante il caricamento delle classi dinamiche AR");
                $scope.checkCaricamentoClassi (null, null, null, 1, true); //così do per caricate le classi dinamiche AR

              }
          }

      });

      $scope.connect2Socket();
      //$state.go('testata_detail');


  };

  $scope.checkCaricamentoClassi = function(layout, numClassiTotali, classeCaricata, numClassiARTotali,classeARCaricata) {
    console.log("sono in checkCaricamentoClassi ("+layout+","+numClassiTotali+","+classeCaricata+","+numClassiARTotali+","+classeARCaricata+")");
    //ha caricato il layout?
    if (layout==true)
    {
      $scope.data.layoutCaricato=true;
      console.log("Layout caricato");
    }
    if (classeCaricata==true)
    {
      $scope.data.numClassiTotaliCaricate++;
      if ($scope.data.numClassiTotaliCaricate==numClassiTotali) $scope.data.classiDinamicheCaricate=true;
      console.log("Classe dinamica "+$scope.data.numClassiTotaliCaricate+" caricata");
    }
    if (classeARCaricata==true) 
    {
      $scope.data.numClassiTotaliARCaricate++;
      if ($scope.data.numClassiTotaliARCaricate==numClassiARTotali) $scope.data.classiDinamicheARCaricate=true;
      console.log("Classe dinamica AR "+$scope.data.numClassiTotaliARCaricate+" caricata");
   }

    //controllo
    if (($scope.data.layoutCaricato==true) && ($scope.data.classiDinamicheCaricate==true) && ($scope.data.classiDinamicheARCaricate==true))
    {
      console.log("Caricato tutto!");
      $ionicLoading.hide();
      $scope.vaiAlProgetto();
    }
  }

  $scope.goPoi = function() {

      $state.go('poi_detail');
  }

  $scope.vaiAlProgetto = function() {

      GlobalVariables.application.currentProgetto = {
        idProgetto: $scope.data.idProgetto,
        tipologia: $scope.data.tipologia
      };

      $ionicLoading.show({
          template: LanguageService.getLabel('ATTENDERE')
      });

      SupportServices.sceltaProgetto({
          onComplete: function(err, json) {

              $ionicLoading.hide();
              if(err) {

                  var alertPopup = $ionicPopup.alert({
                     title: LanguageService.getLabel('ATTENZIONE'),
                     template: LanguageService.getLabel('PROBLEMI_RETE')
                   });

                  return;
              }

              
              //memorizzo l'ultimo album usato
              window.localStorage.setItem("lastNomeProgetto", GlobalVariables.application.currentProgetto.detail.prjName);  

              console.log("lastCodiceProgetto: " + window.localStorage.getItem("lastCodiceProgetto"));
              console.log("lastNomeProgetto: " + window.localStorage.getItem("lastNomeProgetto"));

              $scope.showSplash();
              
              //console.log("vado al progetto");
              //$state.go('progetto_detail');

          }

      });
  };



    $scope.$on("$ionicView.beforeEnter", function(event, data) {

        console.log("beforeEnter.....");
        console.log($scope.data.timer);
        console.log($scope.data.slideTimer);

        // elimino il tema grafico corrente così da poterlo ricaricare
        // dopo aver selezionato una testata
        GlobalVariables.application.currrentLayoutTestata = null;

        $scope.data.testate = GlobalVariables.application.listaProgetti;
        $scope.data.poi = GlobalVariables.application.poi;

        for(var j in $scope.data.testate) {
            $scope.data.testate[ j ].backgroundLastNumber = GlobalVariables.baseUrl + "/" + $scope.data.testate[j].immagineProgetto;
            $scope.data.testate[ j ].utentiTestata=0;
        
            //mi creo un array di indirizzi node a cui spedire l'aggiornamento della push id
            var nodeAddress=$scope.data.testate[j].socketUrl;
            nodeAddress=nodeAddress.substring(0, nodeAddress.lastIndexOf("/"));
            //console.log("recuperato un indirizzo node: " + nodeAddress);
            //se non esiste nell'array, lo inserisco
            if ($scope.data.nodeIpAddresses.indexOf(nodeAddress) == -1)
            {
              //console.log("non esiste nell'array, lo inserisco");
              $scope.data.nodeIpAddresses.push(nodeAddress);
            }
            //console.log("array socket aggiornato: ");
            //console.log($scope.data.nodeIpAddresses);


        }
        console.log("finito - array socket finale: ");
        console.log($scope.data.nodeIpAddresses);

        //console.log($scope.data.testate);

        $scope.getUser4Socket(0); //primo elemento
        //istanzio un timer che mi fa vedere le variazioni, ma solo una volta
        $scope.data.timer = setTimeout($scope.getUser4Socket, (GlobalVariables.intervalloUtentiTestate*1000));

        //se non ho aggiornato il server node con il push id di questo device, lo faccio
        if (!GlobalVariables.application.sendPushId2ServerSent)
        {
          GlobalVariables.application.sendPushId2ServerSent=true;
          

          //mando la richiesta al server
          //alert(PushServices.PUSH_NOTIFICATION_REGISTRATION_ID)
          //alert(GlobalVariables.deviceUUID)
          //alert(GlobalVariables.packageName)
          //alert("Mando la richiesta al server..")


          SupportServices.sendPushId2Server({
            url: $scope.data.nodeIpAddresses[0],
            //url: "http://192.170.5.25:3030",
            pushId: PushServices.PUSH_NOTIFICATION_REGISTRATION_ID,
            uid: GlobalVariables.deviceUUID,
            packageName: GlobalVariables.packageName,
            onComplete: function(err, json_data) {
              
              if(err) {
                console.log("Errore dal setPushId: ");
                console.log(err);
              }
              else
              {
                console.log("Tutto OK dal setPushId: ")
                console.log(json_data);
              }

            }

          });


        }
        else
        {
          //alert("Richiesta già mandata al server..")
        } 


        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide01.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide02.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide03.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide04.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide05.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide06.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide07.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide08.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide09.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide10.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide11.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide12.jpg");


        var setupSlider = function() {
          console.log("setupSlider");

          //some options to pass to our slider
          $scope.data.sliderOptions = {
            initialSlide: 0,
            direction: 'vertical', //or vertical
            speed: 300, //0.3s transition,
            autoplay: 1500,
            pagination: false
          };

          //create delegate reference to link with slider
          $scope.data.sliderDelegate = null;
          $scope.data.sliderDirectionForward = true;
          

          //watch our sliderDelegate reference, and use it when it becomes available
          $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
            //$scope.data.sliderDelegate.lockSwipes(false);
            
            if (newVal != null) {
              //$scope.data.sliderDelegate.slideTo($scope.data.sliderDelegate.activeIndex+1,2000)
              $scope.data.sliderDelegate.on('slideChangeEnd', function() {
                $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
                //use $scope.$apply() to refresh any content external to the slider
                $scope.$apply();
              });
            }
          });


          //ogni tot secondi cambio slide
          $scope.data.slideTimer = setInterval(function(){

            if ($scope.data.sliderDelegate.isEnd)
            {
              $scope.data.sliderDirectionForward=false;
            }
            if ($scope.data.sliderDelegate.isBeginning)
            {
              $scope.data.sliderDirectionForward=true;
            }

            if ($scope.data.sliderDirectionForward)
            {
              $scope.data.sliderDelegate.slideTo($scope.data.sliderDelegate.activeIndex+1,2000)
            }
            else
            {
              $scope.data.sliderDelegate.slideTo($scope.data.sliderDelegate.activeIndex-1,2000)
            }
            /*
              $scope.data.sliderDelegate.slideTo(0,4000);
            else $scope.data.sliderDelegate.slideTo(3,4000);
            //else $scope.data.sliderDelegate.slideNext(2000);
            console.log($scope.data.sliderDelegate.isEnd);
*/
          }, 5000);


        };    


        setupSlider();     

        console.log("Pronto - check1");

/*
        $scope.data.sliderOptions = {
          initialSlide: 1,
          autoplay: 1500,
          direction: 'vertical', //or vertical
          speed: 300 //0.3s transition
        };

        $scope.data.sliderDelegate = null;

        $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
          console.log("data.sliderDelegate: " + newVal + " - " + oldVal);

          if (newVal != null) {
            $scope.data.sliderDelegate.on('slideChangeEnd', function() {
              $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
              //use $scope.$apply() to refresh any content external to the slider
              $scope.$apply();
            });
          }
        });


        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide1.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide2.jpg");
        $scope.data.myslider.slide_list.push("http://livengine.mediasoftonline.com/momentsStuff/slide3.jpg");
        
        console.log("Dati slider ok");

        $timeout(function(){
          $scope.data.sliderDelegate.update();
          $scope.data.sliderDelegate.slideTo(1); //this is to go to the slide when the slider is initiated 
        },100);

*/

    });

    $scope.showMessage = function() {
        if (GlobalVariables.application.comingFrom == "START")
        {
          if (GlobalVariables.message2show_text!="") HelperService.showGenericMessage(GlobalVariables.message2show_text, true);
        }
        GlobalVariables.application.comingFrom == "";   
    }

    $scope.$on("$ionicView.afterEnter", function(event, data) {
        
        //console.log("Immagini Carrello");
        //console.log(window.localStorage.getItem("immaginiCarrello"));

        //var pp=[];
        //window.localStorage.setItem("immaginiCarrello", JSON.stringify(pp));  
        //console.log(window.localStorage.getItem("immaginiCarrello"));


        $scope.data.lastCodiceProgetto = window.localStorage.getItem("lastCodiceProgetto");
        $scope.data.lastNomeProgetto = window.localStorage.getItem("lastNomeProgetto");

        console.log("lastCodiceProgetto: " + $scope.data.lastCodiceProgetto);
        console.log("lastNomeProgetto: " + $scope.data.lastNomeProgetto);




        $rootScope.labelReload = $scope.labelReload;

        $timeout($scope.showMessage,1000);

        LoggerService.triggerAction({
            action: LoggerService.ACTION.ENTER,
            state: LoggerService.CONTROLLER_STATES.HOME,
            data: null
        });


        //prova di invio file di log nel db
        DeviceLogService.sendLogFile({
            onComplete: function(err, res) {

                if(err) {
                    //console.log("Errore, esco");
                    //console.log(err);
                } else {
                    //console.log("tutto ok");
                    //console.log(res);

                }

                /*
                //aspetto almeno che abbia scritto il file di log, prima di scrivere altri log...
                DeviceLogService.log("INFO","Entrato nella home - Test INFO");
                setTimeout(function() {
                  DeviceLogService.log("WARN","Entrato nella home - Test WARN");
                }, 1000);                
                setTimeout(function() {
                  DeviceLogService.log("ERROR","Entrato nella home - Test ERROR");
                }, 2000);
                */
    
            }
        });


      /*
      console.log("chiedo i permessi");
      cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.CAMERA, function( status ){
        if ( status.hasPermission ) {
          console.log("Camera Yes :D ");
        }
        else {
          console.warn("Camera No :( ");
        }
      });
      
      cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, function( status ){
        if ( status.hasPermission ) {
          console.log("WRITE_EXTERNAL_STORAGE Yes :D ");
        }
        else {
          console.warn("WRITE_EXTERNAL_STORAGE No :( ");
        }
      });

      cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.SEND_SMS, function( status ){
        if ( status.hasPermission ) {
          console.log("SEND_SMS Yes :D ");
        }
        else {
          console.warn("SEND_SMS No :( ");
        }
      }); 


      cordova.plugins.permissions.requestPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, function success( status ) {
        if( !status.hasPermission ) console.warn('non ha dato il permesso');
        else (console.warn('Ha dato il permesso'))
      }, function error() {
        console.warn('errore durante il request permission');
      });  
      */ 

    });

    $scope.$on("$ionicView.enter", function(event, data) {


        //$scope.showMessage();
    });


    $scope.uploadImages = function(array) {

      for (var i = 0; i < array.length; i++) {
          console.log('Pronto a uploadare: ' + array[i]);

          FileSystemService.uploadFile({
            fileName: array[i],
            baseUrl: GlobalVariables.baseUrl,
            devID: GlobalVariables.deviceUUID,
            nick: "pippo",
            prjID: 1308 
          });

      }      

    }

    $scope.$on("$ionicView.beforeLeave", function(event, data) {
        
        console.log("esco.....");

        $ionicLoading.hide();

        LoggerService.triggerAction({
            action: LoggerService.ACTION.LEAVE,
            state: LoggerService.CONTROLLER_STATES.HOME,
            data: null
        });
    });
    
    $scope.$on("$ionicView.afterLeave", function(event, data) {
        
        console.log("afterLeave.....");

    });

    $scope.$on("$ionicView.leave", function(event, data) {
        
        console.log("leave.....");

    });

    $scope.getUser4Socket = function(index) {
      
      if (index==null) index=0;
      //console.log("sono in getUser4Socket con index=" + index);
      var actualSocket=$scope.data.testate[index].socketUrl;
      //console.log("prima del cambio=" + actualSocket);
     

      /* for testing only  */
      //actualSocket = 'http://192.170.5.25:3030/' + actualSocket.substring( actualSocket.lastIndexOf("/") + 1);
      //console.log("dopo il cambio=" + actualSocket);

      //console.log(actualSocket);

      SupportServices.getUsersBySocket({
          url: actualSocket,
          onComplete: function(err, json_data) {
            
            if(err) {

            }
            else
            {
              //console.log(json_data.totale);
              $scope.data.testate[index].utentiTestata=json_data.totale;
            }

            //console.log($scope.data.testate);
            index++;
            //console.log($scope.data.testate.length);
            //console.log(index);

            
            if (index>=$scope.data.testate.length)
            {
              //console.log("ridisegno");
            }
            else
            {
              //console.log("vado avanti");
              $scope.getUser4Socket(index);
            }


          }

      });

    }

    $scope.connect2Socket = function() {

            LoggerService.triggerAction({
            action: LoggerService.ACTION.ENTER,
            state: LoggerService.CONTROLLER_STATES.DETAIL_TESTATA,
            data: null
        });

        // apertura di una connessione socket. il socket coincide, lato server, con una istanza socket con namespace dedicato alla rivista
        SocketService.openSocket(function() {

            /*
            // quando arrivo in questa schermata notifico un login
            SocketService.emit('message','set_login', {}, {
                uid: GlobalVariables.deviceUUID,
                os: window.cordova ? window.device.platform.toLowerCase() : 'Browser',
                version: window.cordova ? window.device.version : 'Browser'
            });

            if(GeoLocationService.getCurrentGeodecodedData()) {
                SocketService.emit('message','position', {}, GeoLocationService.getCurrentGeodecodedData());
            }

            if(GlobalVariables.application.currentProgetto) {
                SocketService.emit('message','has_rejoined_progetto', {}, {
                    id: GlobalVariables.application.currentProgetto.idProgetto
                });
            }
            */

            // se il device ha già un id apn/gcm la callback è eseguita immediatamente
            // altrimenti verrà eseguita quando l'utente autorizza le push notification
            PushServices.setOnRegisteredCallback(function(pushId) {

                setTimeout(function() {

                    SocketService.emit('message','set_device_data', {}, {
                        uid: GlobalVariables.deviceUUID,
                        pushId: window.cordova ? pushId : 'Browser'
                    });

                }, 5000);

            });


            var msg = {};

            msg.set_login = {
                uid: GlobalVariables.deviceUUID,
                os: window.cordova ? window.device.platform.toLowerCase() : 'Browser',
                version: window.cordova ? window.device.version : 'Browser',
                appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            };

            if(GeoLocationService.getCurrentGeodecodedData()) {
                msg.position = GeoLocationService.getCurrentGeodecodedData();
            }

            if(GlobalVariables.application.currentProgetto) {
                msg.progetto = {
                    id: GlobalVariables.application.currentProgetto.idProgetto
                };
            }

            SocketService.emit('message','has_open_or_reopen_socket', {}, msg);

        });



        GeoLocationService.setCallback("updateSocketGps", function(position) {

            // un timer recupera la posizione del device on X minuti. se questo valore è valido lo comunico al server
            //GlobalVariables.gpsPosition=position;
            if(position) {
                GeoLocationService.reverseGeocode(function(err, geodecoded_data) {
                    //GlobalVariables.geodecodedData=geodecoded_data;
                    if(geodecoded_data) {
                        SocketService.emit('message','position', {}, geodecoded_data);
                    }
                });
            }


        });

    }

  $scope.splashScreenFotografo_remove = function() {
    console.log("splashScreenFotografo.remove()");
    console.log("vado al progetto");
    $state.go('progetto_detail');
    $scope.splashScreenFotografo.remove();
  }; 

  $scope.showCreditsFotografo = function() {
    $scope.data.showInfoFotografo=!$scope.data.showInfoFotografo;
  };   



  $scope.showSplash = function() {

        console.log("-----");
        console.log(GlobalVariables.application.currentProgetto);

        $scope.splashScreenFotografo = $ionicModal.fromTemplate(

          '<ion-modal-view padding="true" cache-view="false" id="appSplashScreen">' +

              '<ion-content scroll="true" padding="false" id="splashContent">' +

                  '<div id="splashTitle">' +
                    $scope.data.layout.testataNome +
                  '</div>' +

                  '<div id="logoSplashBox">' +
                    '<img src="'+$scope.data.layout.homeSplash+'" style="width:100%;"></img>'+
                    '<a ng-click="showCreditsFotografo()" class="ion-ios-information-outline iconaInfo"></a>' +

                    '<div ng-if="data.showInfoFotografo == true" id="creditsFotografo" style="">' +
                      '<a href="" style="color:#000000;text-decoration: none;" class="ion-ios-home">&nbsp{{data.infoFotografo.indirizzo}}</a>' +
                      '<br><br><a href="tel:{{data.infoFotografo.telefono}}" style="color:#000000;text-decoration: none;" class="ion-ios-telephone">&nbsp{{data.infoFotografo.telefono}}</a>' +
                      '<br><br><a href="mailto:{{data.infoFotografo.mail}}" style="color:#000000;text-decoration: none;" class="ion-ios-email">&nbsp{{data.infoFotografo.mail}}</a>' +
                    '</div>'+

                  '</div>'+



                  $scope.data.labels.presenta +

                  '<div id="nomeSplashBox">' +
                    GlobalVariables.application.currentProgetto.detail.prjName +
                  '</div>' +

                  '<button ng-click="splashScreenFotografo_remove()" class="button button-positive cst-button" style="background-color: #9acf16;width:50%;">'+$scope.data.labels.apriAlbum+'</button>' +


              '</ion-content>' +


          '</ion-modal-view>',

          {
              scope: $scope,
              focusFirstInput: true,
              animation :'scale-in',
              hardwareBackButtonClose: false
          }

      );

      $scope.data.showInfoFotografo=false;

      $scope.data.infoFotografo={
        indirizzo: "",
        telefono: "",
        mail: ""
      }

      if ($scope.data.layout.infoMoments != null)
      {
        try {
          var info = JSON.parse($scope.data.layout.infoMoments);
          $scope.data.infoFotografo.indirizzo=info.indirizzo;
          $scope.data.infoFotografo.telefono=info.telefono;
          $scope.data.infoFotografo.mail=info.mail;

        } catch(err) {

        }        
      }

      $scope.splashScreenFotografo.show();
      clearInterval($scope.data.slideTimer);




  }


});
