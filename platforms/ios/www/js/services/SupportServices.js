angular.module('LiveEngine.Support.Services', [])

.service('SupportServices', function(GlobalVariables, LoggerService, DeviceLogService, CryptoService, FileSystemService, SocketService, $rootScope, $http, $ionicModal, $ionicPopup, LanguageService, $httpParamSerializerJQLike) {

    var me = this;
    me.HTTP_TIMEOUT = 15000; //era 2000
    me.BUFFER_DIRECTORY = GlobalVariables.directory_main + "/bufferDirectory" + GlobalVariables.directory_trail;
    me.OLD_BUFFER_DIRECTORY_TO_DELETE = "bufferDirectory" + GlobalVariables.directory_trail;
    //me.BUFFER_DIRECTORY = "bufferDirectory" + GlobalVariables.directory_trail;
    GlobalVariables.bufferDirectory = me.BUFFER_DIRECTORY; //da poter usare in altri services, senza dover includere questo, per evitare dipendenze circolari
    

    me.createRootDir = function(parameters) {

      FileSystemService.createRootDir({
          directory: GlobalVariables.directory_main,
          dirBuffer2delete: me.OLD_BUFFER_DIRECTORY_TO_DELETE,
          dirDinamic2delete: "dinamic_classes" + GlobalVariables.directory_trail,
          dirGameCenter2delete: "GameCenter" + GlobalVariables.directory_trail,
          onComplete: function(err, operationCompleted) {
              //console.log("on complete di createRootDir in SupportService, con err: " + err);

              
              if((err) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                  parameters.onComplete(err, null);
                  return;
              }

              parameters.onComplete(null, null);

          }
      });

    }

    /*
    me.getLabel = function(codice, lingua) {

        //override della lingua
        lingua=GlobalVariables.systemLanguage;

        var returnValue = "no-value";

        var token = _.find(GlobalVariables.systemLabels, function(item) { return item.codice == codice; });

         if(token) {
            returnValue = token[lingua] ? token[lingua] : token[lingua + 'Default'];
        }

        return returnValue;

    };

*/


    me.getGuideImageLinks = function(parameters) {

        var BUFFER_FILE_NAME = "guida_" + GlobalVariables.idCliente + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveDataGetConfig.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              action: 'getGuideData',
              idCliente: GlobalVariables.idCliente
            }
        })
        .then(function(json) {

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: JSON.stringify(json.data),
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        /* alert("config saving failed: " + err); */
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            DeviceLogService.log("ERROR","getGuideImageLinks: " + JSON.stringify(error));
            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        DeviceLogService.log("ERROR","getGuideImageLinks - fileRead: " + JSON.stringify(err));
                        parameters.onComplete("ERRORE - TESTO IN FUNZIONE CHIAMANTE", null);
                    } else {
                        parameters.onComplete(null, JSON.parse(readData));
                    }

                }
            });

        });

    };



    me.getLibrariesToLoad = function(parameters) {

      var BUFFER_FILE_NAME = "libs_testata_" + parameters.idTestata + ".json";

      $http({
          url: GlobalVariables.application.applicationUrl + "/getLibraries",
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {

          var bufferData2Write = JSON.stringify(json.data);
          var encoded = CryptoService.encode(bufferData2Write);          

          FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: encoded,
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      /* alert("config saving failed: " + err); */
                  }

                  parameters.onComplete(null, json.data);

              }
          });

      }, function(error, xhr) {

          DeviceLogService.log("ERROR","getLibrariesToLoad: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      DeviceLogService.log("ERROR","getLibrariesToLoad - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

      });

    };

    me.getARLibrariesToLoad = function(parameters) {

      var BUFFER_FILE_NAME = "libsAR_testata_" + parameters.idTestata + ".json";

      //console.log("sono in getARLibrariesToLoad, con BUFFER_FILE_NAME: " + BUFFER_FILE_NAME);
      //console.log("url: " + GlobalVariables.application.applicationUrl + "/getARLibraries");


      $http({
          url: GlobalVariables.application.applicationUrl + "/getARLibraries",
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {

          //console.log("sono nel Then");
          
          var bufferData2Write = JSON.stringify(json.data);
          var encoded = CryptoService.encode(bufferData2Write);          

          FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: encoded,
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      /* alert("config saving failed: " + err); */
                  }

                  parameters.onComplete(null, json.data);

              }
          });

      }, function(error, xhr) {

          DeviceLogService.log("ERROR","getARLibrariesToLoad - fileRead: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  //console.log("onComplete del fileRead, con dir: " + me.BUFFER_DIRECTORY + " e filename: " + BUFFER_FILE_NAME + " e err: " + err + " e readData: " + readData);
                  if(err || !readData) {
                      DeviceLogService.log("ERROR","getARLibrariesToLoad - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

      });

    };


    me.getUsersByPrjId = function(parameters) {
      
      //console.log(GlobalVariables.application.applicationUrl + "/usersCountByPrjId");

      $http({
          url: GlobalVariables.application.applicationUrl + "/usersCountByPrjId",
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {
          
          parameters.onComplete(null, json.data);

      }, function(error, xhr) {

          parameters.onComplete("ERRORE - TESTO NON IMPORTANTE, NON VERRA MOSTRATO", null);

      });

    };

    me.getUsersBySocket = function(parameters) {
      
      $http({
          url: parameters.url + "/usersCount",
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {
          
          parameters.onComplete(null, json.data);

      }, function(error, xhr) {

          parameters.onComplete("ERRORE - TESTO NON IMPORTANTE, NON VERRA MOSTRATO", null);

      });

    };

    me.sendPushId2Server = function(parameters) {
      
      var serverUrl=parameters.url+"/"+parameters.packageName+"/"+parameters.uid+"/"+parameters.pushId+"/setPushId";
      console.log("Spedisco al server: " + serverUrl);
      
      $http({
          url: serverUrl,
          responseType: 'json',
          method: 'GET',
          timeout: 3000
      })
      .then(function(json) {
          
          parameters.onComplete(null, json.data);

      }, function(error, xhr) {

          parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);

      });

    };



    me.getLayoutTestata = function(parameters) {

        var BUFFER_FILE_NAME = "layout_testata_" + parameters.idTestata + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveDataLayoutTestata.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idTestata: parameters.idTestata
            }
        })
        .then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: encoded,
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        /* alert("config saving failed: " + err); */
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            DeviceLogService.log("ERROR","getLayoutTestata: " + JSON.stringify(error));
            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        DeviceLogService.log("ERROR","getLayoutTestata - fileRead: " + JSON.stringify(err));
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        var decoded = CryptoService.decode(readData);
                        parameters.onComplete(null, JSON.parse(decoded));
                    }

                }
            });

        });

    };






    me.getConfiguration = function(parameters) {

        var BUFFER_FILE_NAME = "configuration.json";

        $http({

            url: GlobalVariables.baseUrl + "/liveDataSystemMetadataCliente.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idCliente: GlobalVariables.idCliente
            }

        }).then(function(json) {

            console.log("Ho preso la configurazione");
            var bufferData2Write = JSON.stringify(json.data);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: encoded,
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        /* alert("config saving failed: " + err); */
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            DeviceLogService.log("ERROR","getConfiguration: " + JSON.stringify(error));
            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        DeviceLogService.log("ERROR","getConfiguration - fileRead: " + JSON.stringify(err));
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        
                        var decoded = CryptoService.decode(readData);
                        parameters.onComplete(null, JSON.parse(decoded));
                    }

                }
            });


        });

    };



    // questo metodo restituisce i percorsi per i poi del cliente
    me.getPercorsi = function(parameters) {

        var BUFFER_FILE_NAME = "percorsi.json";

        $http({

            url: GlobalVariables.baseUrl + "/liveDataPercorsi.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idCliente: GlobalVariables.idCliente,
              uid: GlobalVariables.deviceUUID,
              appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            }

        }).then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: encoded,
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        // alert("config saving failed: " + err);
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            DeviceLogService.log("ERROR","getPercorsi: " + JSON.stringify(error));
            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        DeviceLogService.log("ERROR","getPercorsi - fileRead: " + JSON.stringify(err));
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        var decoded = CryptoService.decode(readData);
                        parameters.onComplete(null, JSON.parse(decoded));
                    }

                }
            });

        });

    }

    // questo metodo restituisce i poi del cliente, da usare per vedere se si devono scaricare HS custom
    me.getPoi = function(parameters) {

        var BUFFER_FILE_NAME = "poi.json";

        $http({

            url: GlobalVariables.baseUrl + "/liveDataPoi.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idPercorso: parameters.idPercorso,
              idCliente: GlobalVariables.idCliente,
              flagHS: "true"
            }

        }).then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: encoded,
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        // alert("config saving failed: " + err);
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            DeviceLogService.log("ERROR","getPoi: " + JSON.stringify(error));
            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        DeviceLogService.log("ERROR","getPoi - fileRead: " + JSON.stringify(err));
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        var decoded = CryptoService.decode(readData);
                        parameters.onComplete(null, JSON.parse(decoded));
                    }

                }
            });

        });

    }


    // questo metodo restituisce le copertine
    me.getListaCopertine = function(parameters) {

        var BUFFER_FILE_NAME = "copertine.json";

        $http({

            //url: GlobalVariables.baseUrl + "/liveDataProgettiTestata4Demo.php",
            url: GlobalVariables.baseUrl + "/liveDataProgettiTestata.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
              idCliente: GlobalVariables.idCliente,
              uid: GlobalVariables.deviceUUID,
              appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            }

        }).then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            //console.log(bufferData2Write);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: encoded,
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        // alert("config saving failed: " + err);
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

            DeviceLogService.log("ERROR","getListaCopertine: " + JSON.stringify(error));
            FileSystemService.fileRead({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                onComplete: function(err, readData) {

                    if(err || !readData) {
                        DeviceLogService.log("ERROR","getListaCopertine - fileRead: " + JSON.stringify(err));
                        parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                    } else {
                        var decoded = CryptoService.decode(readData);
                        //console.log("--- DECODIFICATO ---");
                        //console.log(decoded);
                        parameters.onComplete(null, JSON.parse(decoded));
                    }

                }
            });

        });

    }



    me.getNumeriTestata = function(parameters) {

        var BUFFER_FILE_NAME = "numeri_testata_" + parameters.idTestata + "_" + parameters.start + "_" + parameters.limit + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveDataCopertineTestata.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                idTestata: parameters.idTestata,
                anno: (parameters.anno ? parameters.anno : ""),
                start: (parameters.start ? parameters.start : ""),
                limit: (parameters.limit ? parameters.limit : ""),
                uid: GlobalVariables.deviceUUID
            }
        })
        .then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
                directory: me.BUFFER_DIRECTORY,
                fileName: BUFFER_FILE_NAME,
                bufferData: encoded,
                onComplete: function(err, operationCompleted) {

                    if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                        // alert("config saving failed: " + err);
                    }

                    parameters.onComplete(null, json.data);

                }
            });

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","getNumeriTestata: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      DeviceLogService.log("ERROR","getNumeriTestata - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

        });

    };


    me.getDettaglioProgetto = function(parameters) {

        //console.log("getDettaglioProgetto");
        //console.log(GlobalVariables.version+"_"+GlobalVariables.build);

        var BUFFER_FILE_NAME = "dettaglio_progetto_" + parameters.idProgetto + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveDataNew.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                idProgetto: parameters.idProgetto,
                uid: GlobalVariables.deviceUUID,
                appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            }
        })
        .then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            //console.log(bufferData2Write);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: encoded,
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      // alert("config saving failed: " + err);
                  }

                  parameters.onComplete(null, json.data);

              }
          });

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","getDettaglioProgetto: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      DeviceLogService.log("ERROR","getDettaglioProgetto - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

        });

    };

    
    me.sceltaProgetto = function(parameters) {

      console.log("scelta progetto");
      me.getDettaglioProgetto({
          idProgetto: GlobalVariables.application.currentProgetto.idProgetto,
          onComplete: function(err, json) {

              
              console.log("eccolo");
              console.log(json);
              if(err) {

                  var alertPopup = $ionicPopup.alert({
                     title: LanguageService.getLabel('ATTENZIONE'),
                      template: LanguageService.getLabel('PROBLEMI_RETE')
                   });
                  parameters.onComplete("TESTO NON IMPORTANTE, VERRA USATO QUELLO DEL CHIAMANTE", null); 
              }

              GlobalVariables.application.currentProgetto.detail = json.progetti[0];
              GlobalVariables.application.currentProgetto.gradimento = json.gradimento;
              GlobalVariables.application.currentProgetto.moments_icons = json.moments_icons;

              //console.log(GlobalVariables.application.currentProgetto.moments_likes);

              for(var j=0; j<GlobalVariables.application.currentProgetto.detail.target.length; j++) {

                  GlobalVariables.application.currentProgetto.detail.target[ j ].imagePath =
                    GlobalVariables.baseUrl + "/" + GlobalVariables.application.currentProgetto.detail.target[ j ].imagePath;

              }


              // ################################ //
              // evento 'apertura di una rivista' //
              // ################################ //
              SocketService.emit('message','has_view_progetto', {}, {
                  uid: GlobalVariables.deviceUUID,
                  id: GlobalVariables.application.currentProgetto.idProgetto,
                  description: GlobalVariables.application.currentProgetto.detail.prjDescr,
                  name: GlobalVariables.application.currentProgetto.detail.prjName
              });
              // ################################ //
              // evento 'apertura di una rivista' //
              // ################################ //

              //ritorno
              parameters.onComplete(null, null); 
              
          }

      });


    };

    me.getFotosferaData = function(parameters) {

        var BUFFER_FILE_NAME = "dettaglio_fotosfera_" + parameters.idProgetto + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveDataFotosfera.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                idProgetto: parameters.idProgetto,
                uid: GlobalVariables.deviceUUID,
                appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            }
        })
        .then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            //console.log(bufferData2Write);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: encoded,
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      // alert("config saving failed: " + err);
                  }

                  parameters.onComplete(null, json.data);

              }
          });

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","getFotosferaData: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      DeviceLogService.log("ERROR","getFotosferaData - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

        });

    };



    me.getAlbumByCode = function(parameters) {

        $http({
            url: GlobalVariables.baseUrl + "/liveData_moments_AlbumCode.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                code: parameters.code,
                uid: GlobalVariables.deviceUUID,
                appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            }
        })
        .then(function(json) {
            parameters.onComplete(null, json.data); 

        }, function(error, xhr) {
            parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null); 

        });

    };

    me.getLikes = function(parameters) {

        console.log("getLikes");

        var BUFFER_FILE_NAME = "likes_" + parameters.idProgetto + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveData_moments_like.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                idProgetto: parameters.idProgetto,
                action: "getLikes"
            }
        })
        .then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            //console.log(bufferData2Write);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: encoded,
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      // alert("config saving failed: " + err);
                  }

                  parameters.onComplete(null, json.data);

              }
          });

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","getLikes: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      DeviceLogService.log("ERROR","getLikes - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

        });

    };

    me.setLikes = function(parameters) {

        console.log("setLikes");

        var BUFFER_FILE_NAME = "likes_" + parameters.idProgetto + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveData_moments_like.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                idProgetto: parameters.idProgetto,
                uid: GlobalVariables.deviceUUID,
                idImage: parameters.idImage,
                like: parameters.like,
                coords: parameters.coords,
                action: "setLikes"
            }
        })
        .then(function(json) {

            //console.log("Ritornato dalla setLike con json: ");
            //console.log(json);

            var bufferData2Write = JSON.stringify(json.data);
            //console.log(bufferData2Write);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: encoded,
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      // alert("config saving failed: " + err);
                  }

                  parameters.onComplete(null, json.data);

              }
          });

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","setLikes: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      DeviceLogService.log("ERROR","setLikes - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

        });

    };

    me.setClap = function(parameters) {

        console.log("setClap");

        var BUFFER_FILE_NAME = "likes_" + parameters.idProgetto + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveData_moments_like.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                idProgetto: parameters.idProgetto,
                uid: GlobalVariables.deviceUUID,
                idImage: parameters.idImage,
                coords: parameters.coords,
                action: "setClap"
            }
        })
        .then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            //console.log(bufferData2Write);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: encoded,
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      // alert("config saving failed: " + err);
                  }

                  parameters.onComplete(null, json.data);

              }
          });

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","setClap: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      DeviceLogService.log("ERROR","setClap - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

        });

    };  


    me.getComments = function(parameters) {

        console.log("getComments");

        var BUFFER_FILE_NAME = "comments_" + parameters.idImmagine + ".json";

        $http({
            url: GlobalVariables.baseUrl + "/liveData_moments_commento.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                idImmagine: parameters.idImmagine,
                deviceId: GlobalVariables.deviceUUID
            }
        })
        .then(function(json) {

            var bufferData2Write = JSON.stringify(json.data);
            //console.log(bufferData2Write);
            var encoded = CryptoService.encode(bufferData2Write);

            FileSystemService.writeFile({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              bufferData: encoded,
              onComplete: function(err, operationCompleted) {

                  if((err || !operationCompleted) && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {
                      // alert("config saving failed: " + err);
                  }

                  parameters.onComplete(null, json.data);

              }
          });

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","getComments: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      DeviceLogService.log("ERROR","getComments - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

        });

    };

    me.setCommento = function(parameters) {

        console.log("setCommento");

        console.log(parameters);

        var BUFFER_FILE_NAME = "comments_" + parameters.idImmagine + ".json";

        var xsrf = $httpParamSerializerJQLike({
                idImmagine: parameters.idImmagine,
                deviceId: GlobalVariables.deviceUUID,
                commento: parameters.commento,
                nickname: parameters.nickname
            });

        $http({
            url: GlobalVariables.baseUrl + "/setCommentoMoments.php",
            responseType: 'json',
            method: 'POST',
            timeout: me.HTTP_TIMEOUT,
            data: xsrf,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}            

        })
        .then(function(json) {

            me.getComments({
                idImmagine: parameters.idImmagine,
                onComplete: function(err, json) {

                      parameters.onComplete(err, json);

                }
            });

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","setCommento: " + JSON.stringify(error));
          FileSystemService.fileRead({
              directory: me.BUFFER_DIRECTORY,
              fileName: BUFFER_FILE_NAME,
              onComplete: function(err, readData) {

                  if(err || !readData) {
                      DeviceLogService.log("ERROR","setCommento - fileRead: " + JSON.stringify(err));
                      parameters.onComplete(LanguageService.getLabel('IMPOSSIBILE_RECUPERARE_DATI_RIPROVARE'), null);
                  } else {
                      var decoded = CryptoService.decode(readData);
                      parameters.onComplete(null, JSON.parse(decoded));
                  }

              }
          });

        });

    };  


    me.getDisclaimer = function(parameters) {

        console.log("getDisclaimer");

        $http({
            url: GlobalVariables.baseUrl + "/disclaimers/commentiMoments.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT
        })
        .then(function(json) {

            parameters.onComplete(json.data);

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","getDisclaimer: " + JSON.stringify(error));
          parameters.onComplete("Errore di rete");


        });

    };    


    
    me.getContributiImages = function(parameters) {

        console.log("getContributiImages");

        $http({
            url: GlobalVariables.baseUrl + "/momentsContributi/scandir.php",
            responseType: 'json',
            method: 'GET',
            timeout: me.HTTP_TIMEOUT,
            params: {
                prjID: parameters.prjId,
                devID: parameters.devId
            }
        })
        .then(function(json) {

            parameters.onComplete(null, json.data);

        }, function(error, xhr) {

          DeviceLogService.log("ERROR","getDisclaimer: " + JSON.stringify(error));
          parameters.onComplete("Errore di rete", null);


        });

    };        

})
