function Hotspot(oggetto,wiki_ar,parent){

   this.definizione = oggetto;
   this.arrayOverlay = [];
   this.arrayImageResourceLike = [];
   this.arrayImages2Load = [];
   this.AR=wiki_ar;
   this.world=parent;
   this.imgLike = oggetto.moments_icons.array;
   this.imgClap = "";
}

Hotspot.prototype.create = function (param)
{

	var arrayImageResourceCreated = new Array(); //qui mi memorizzo le imageResource create. Se esiste già, la riutilizzo, invece di crearne una nuova




	//devo crearmi un array di icone Like, da utilizzare quando servono, senza crearne di nuove ogni volta
//	var arrayImageResourceLike = new Array();

	this.arrayImageResourceLike.push(new this.AR.ImageResource(this.imgLike[0])); //generica
	this.arrayImageResourceLike.push(new this.AR.ImageResource(this.imgLike[1]));
	this.arrayImageResourceLike.push(new this.AR.ImageResource(this.imgLike[2]));
	this.arrayImageResourceLike.push(new this.AR.ImageResource(this.imgLike[3]));
	this.arrayImageResourceLike.push(new this.AR.ImageResource(this.imgLike[4]));
	this.arrayImageResourceLike.push(new this.AR.ImageResource(this.imgLike[5]));
	this.arrayImageResourceLike.push(new this.AR.ImageResource(this.imgLike[6]));
	this.arrayImageResourceLike.push(new this.AR.ImageResource(this.imgLike[7]));




	/*
	this.imgLike.push(contenutiUrl + "/" + this.definizione.moments_icons.like);
	this.imgLike.push(contenutiUrl + "/" + this.definizione.moments_icons.icona_1);
	this.imgLike.push(contenutiUrl + "/" + this.definizione.moments_icons.icona_2);
	this.imgLike.push(contenutiUrl + "/" + this.definizione.moments_icons.icona_3);
	this.imgLike.push(contenutiUrl + "/" + this.definizione.moments_icons.icona_4);
	this.imgLike.push(contenutiUrl + "/" + this.definizione.moments_icons.icona_5);
	this.imgLike.push(contenutiUrl + "/" + this.definizione.moments_icons.icona_6);
	this.imgLike.push(contenutiUrl + "/" + this.definizione.moments_icons.icona_7);
	*/
	
	this.imgClap = contenutiUrl + "/" + this.definizione.moments_icons.clap;
	this.imgComment = contenutiUrl + "/" + this.definizione.moments_icons.comment;
	
	var imgClap = new this.AR.ImageResource(this.imgClap);
	var imgComment = new this.AR.ImageResource(this.imgComment);



	var dimensioneIndicatoreStandard=0.03;
	var dimensioneLabelStandard=0.05;

	//ciclo sull'array dei target e metto i punti
	for (i=0;i<this.definizione.target.length;i++)
	{

		var tipoIndicatore=this.definizione.target[i].tipoIndicatore;
		var coloreIndicatore=this.definizione.target[i].coloreIndicatore;

		if ((coloreIndicatore!="rosso") && (coloreIndicatore!="bianco") && (coloreIndicatore!="nero") && (coloreIndicatore!="giallo") && (coloreIndicatore!="blu") && (coloreIndicatore!="verde")) coloreIndicatore="rosso";
		if ((tipoIndicatore!="0") && (tipoIndicatore!="1") && (tipoIndicatore!="2") && (tipoIndicatore!="3") && (tipoIndicatore!="4") && (tipoIndicatore!="5") && (tipoIndicatore!="6") && (tipoIndicatore!="7")) tipoIndicatore="0";

		var immagineIndicatoreStandard="assets/"+"indicatore_"+tipoIndicatore+"_"+coloreIndicatore+".png";
		//console.log("Indicatore standard per questo target: " + immagineIndicatoreStandard);

		for (t=0;t<this.definizione.target[i].punti.length;t++)
		{
			console.log("considero il punto con label: " + this.definizione.target[i].punti[t].label);
			console.log(this.definizione.target[i].punti[t].config);
			//check prima di tutto... è un video in overlay???
			var overlayTrovato=false;
			var config=JSON.parse(this.definizione.target[i].punti[t].config);
			for (var x=0;x<config.length;x++)
			{
				if (typeof config[x].overlay != 'undefined')
				{
					if (config[x].overlay == 1)
					{
						//è un video in overlay
						overlayTrovato=true;
						var oggettoVideo=JSON.parse(config[x].config);
						var url=oggettoVideo.path;
						url=decodeURIComponent(url);
						immagineIndicatore = url;
						overlay = new AR.VideoDrawable(immagineIndicatore, 1, {
						    translate: {
						        x: 0,
						        y: 0
						    },
						    scale: 1,
							enabled: false,
							opacity : 1.0,
							zOrder: 10
						});
						overlay.onClick = this.world.mostraContenuti(i,t);
						overlay.customType=-1;
						overlay.targetRef=this.definizione.target[i].targetName;
						overlay.isOverlayVideo=true;
						this.arrayOverlay.push(overlay);


					}
				}
			}
			if (overlayTrovato) continue; //vado al prossimo hs


			var dimensioneIndicatore=dimensioneIndicatoreStandard;
			var dimTemp = parseInt(this.definizione.target[i].punti[t].dimensione) || 0;
			if (dimTemp != 0) dimensioneIndicatore = dimTemp/100;

			var offsetGeneraleInizialeX=(Number(this.definizione.target[i].punti[t].x)/100)-0.5;
			var offsetGeneraleInizialeY=((Number(this.definizione.target[i].punti[t].y)/100)-0.5)*(-1);
			var aspectRatio = Number(this.definizione.target[i].aspectRatio); //visto che gli sostamenti sono in base all'altezza dell'immagine di riferimento, devo usare l'aspectRatio per correggere lo spostamento sulle x

			offsetGeneraleInizialeX=offsetGeneraleInizialeX*aspectRatio;

			var immagineIndicatore=immagineIndicatoreStandard;

			//questo HS ha un indicatore standard LEGATO al punto e non all'immagine? 
			if (this.definizione.target[i].punti[t].tipoIndicatore!="-1")
			{
				var tipoIndicatorePunto=this.definizione.target[i].punti[t].tipoIndicatore;
				var coloreIndicatorePunto=this.definizione.target[i].punti[t].coloreIndicatore;

				if ((coloreIndicatorePunto!="rosso") && (coloreIndicatorePunto!="bianco") && (coloreIndicatorePunto!="nero") && (coloreIndicatorePunto!="giallo") && (coloreIndicatorePunto!="blu") && (coloreIndicatorePunto!="verde")) coloreIndicatorePunto="rosso";
				if ((tipoIndicatorePunto!="0") && (tipoIndicatorePunto!="1") && (tipoIndicatorePunto!="2") && (tipoIndicatorePunto!="3") && (tipoIndicatorePunto!="4") && (tipoIndicatorePunto!="5") && (tipoIndicatorePunto!="6") && (tipoIndicatorePunto!="7")) tipoIndicatorePunto="0";

				immagineIndicatore="assets/"+"indicatore_"+tipoIndicatorePunto+"_"+coloreIndicatorePunto+".png";
				//console.log("Indicatore standard (legato al punto) per questo target: " + immagineIndicatore);
			}

			//è un indicatore custom o standard (vecchio stile)?
			if (this.definizione.target[i].punti[t].pathIndicatore)
			{
				//console.log("il punto "+t+" ha un indicatore custom");
				immagineIndicatore = contenutiPath + this.definizione.target[i].punti[t].pathIndicatore;
			}

			//è un indicatore custom nuovo stile?
			var overlay;
			if (this.definizione.target[i].punti[t].hsc_tipo)
			{
				//console.log("E' un hotspot custom nuovo stile, di tipo: " + this.definizione.target[i].punti[t].hsc_tipo);
				//console.log(JSON.stringify(this.definizione.target[i].punti[t]));
				
				if (this.definizione.target[i].punti[t].hsc_tipo==1) //modello 3d
				{
					var oggettoNome = this.definizione.target[i].punti[t].hsc_path;
					oggettoNome = oggettoNome.replace(".wt3", ".mao");
					var pathModello = contenutiPath + oggettoNome;
					console.log("path modello: " + pathModello);
					//var dimIniziale=parseFloat(1/this.definizione.target[i].punti[t].hsc_sdu);
					var dimIniziale=(Number(1/this.definizione.target[i].punti[t].hsc_sdu))*dimensioneIndicatore;
					var rotX=Number(this.definizione.target[i].punti[t].hsc_rotx);
					var rotY=Number(this.definizione.target[i].punti[t].hsc_roty);
					var rotZ=Number(this.definizione.target[i].punti[t].hsc_rotz);
					var toRotate=false;
					var rotazionePeriodo=10;
					if (this.definizione.target[i].punti[t].rotazione=="1")
					{
						toRotate=true;
						rotazionePeriodo=parseInt(this.definizione.target[i].punti[t].periodo_rotazione);
					}

					//console.log("dimensione iniziale: " + dimIniziale);
					//console.log("rotation x: " + rotX);
					//console.log("rotation y: " + rotY);
					//console.log("rotation z: " + rotZ);
					//if (toRotate) console.log("periodo rotazione: " + rotazionePeriodo);

					overlay = new this.AR.Model(pathModello, {
						scale: {
							x: dimIniziale,
							y: dimIniziale,
							z: dimIniziale
						},
						rotate: {
							x: rotX,
							y: rotY,
							z: rotZ
						},
						translate: {
							x: offsetGeneraleInizialeX,
							y: offsetGeneraleInizialeY,
							z: 0
						},
						enabled: false
					});
					overlay.onClick = this.world.mostraContenuti(i,t);
					overlay.customType=this.definizione.target[i].punti[t].hsc_tipo;
					overlay.targetRef=this.definizione.target[i].targetName;
					if (toRotate)
					{
						overlay.rotationAnimation = new this.AR.PropertyAnimation(overlay, "rotate.z", 0, 360, rotazionePeriodo*1000);
					}
					this.arrayOverlay.push(overlay);
				}
				else if (this.definizione.target[i].punti[t].hsc_tipo==0) //immagine
				{
					immagineIndicatore = contenutiPath + this.definizione.target[i].punti[t].hsc_path;
					//esiste già questa imageResource?
					var imgTemp=null;
					for (var indexTest=0;indexTest<arrayImageResourceCreated.length;indexTest++)
					{
						if (arrayImageResourceCreated[indexTest].path==immagineIndicatore) imgTemp=arrayImageResourceCreated[indexTest].imageResource;
					}
					if (imgTemp==null) //non trovata, la creo e la metto nell'array per usi futuri
					{
						imgTemp = new this.AR.ImageResource(immagineIndicatore);
						var item = new Object();
						item.imageResource=imgTemp;
						item.path=immagineIndicatore;
						arrayImageResourceCreated.push(item);
						//console.log("Immagine HS non presente, la creo e la memorizzo: " + immagineIndicatore);
					}
					else
					{
						//console.log("Immagine HS già presente, la riutilizzo: " + immagineIndicatore);
					}

					overlay = new this.AR.ImageDrawable(imgTemp, dimensioneIndicatore,
					{
						offsetX: offsetGeneraleInizialeX,
						offsetY: offsetGeneraleInizialeY,
						scale: 1,
						enabled: false,
						opacity : 1.0,
						zOrder: 10
					});
					overlay.onClick = this.world.mostraContenuti(i,t);
					overlay.customType=this.definizione.target[i].punti[t].hsc_tipo;
					overlay.targetRef=this.definizione.target[i].targetName;
					this.arrayOverlay.push(overlay);						
				}
			}				
			else
			{	
				//esiste già questa imageResource?
				var imgTemp=null;

				for (var indexTest=0;indexTest<arrayImageResourceCreated.length;indexTest++)
				{
					if (arrayImageResourceCreated[indexTest].path==immagineIndicatore) imgTemp=arrayImageResourceCreated[indexTest].imageResource;
				}
				if (imgTemp==null) //non trovata, la creo e la metto nell'array per usi futuri
				{
					imgTemp = new this.AR.ImageResource(immagineIndicatore);
					var item = new Object();
					item.imageResource=imgTemp;
					item.path=immagineIndicatore;
					arrayImageResourceCreated.push(item);
					//console.log("Immagine HS non presente, la creo e la memorizzo: " + immagineIndicatore);
				}
				else
				{
					//console.log("Immagine HS già presente, la riutilizzo: " + immagineIndicatore);
				}

				//var imgTemp = new this.AR.ImageResource(immagineIndicatore);
				overlay = new this.AR.ImageDrawable(imgTemp, dimensioneIndicatore,
				{
					offsetX: offsetGeneraleInizialeX,
					offsetY: offsetGeneraleInizialeY,
					scale: 1,
					enabled: false,
					opacity : 1.0,
					zOrder: 10
				});
				overlay.onClick = this.world.mostraContenuti(i,t);
				overlay.customType=-1;
				overlay.targetRef=this.definizione.target[i].targetName;
				this.arrayOverlay.push(overlay);
			}

			var dimensioneLabel=dimensioneLabelStandard;
			var dimTemp = parseInt(this.definizione.target[i].punti[t].labelDimension) || 0;
			if (dimTemp != 0) dimensioneLabel = dimTemp/100;
			var labelColor=this.definizione.target[i].punti[t].labelColor;
			var labelSfondoColor=this.definizione.target[i].punti[t].labelSfondoCol;
			var labelSfondoTrasp=parseInt(this.definizione.target[i].punti[t].labelSfondoTrasp);
			var labelStyle=parseInt(this.definizione.target[i].punti[t].labelStile);
			//creo il colore con l'opacità per lo sfondo della label
			//trasformo il valore 0-100 in 0-255
			var trasp0255=parseInt(255*labelSfondoTrasp/100);
			var hexString = trasp0255.toString(16);
			if (hexString.length==1) hexString="0"+hexString;
			var labelSfondoColor=labelSfondoColor+hexString;
			//alert(labelSfondoColor);
			//stile
			var myFontstyle=AR.CONST.FONT_STYLE.NORMAL;
			if (labelStyle==1) myFontstyle=AR.CONST.FONT_STYLE.BOLD;
			else if (labelStyle==2) myFontstyle=AR.CONST.FONT_STYLE.ITALIC;

			var label = new this.AR.Label(this.definizione.target[i].punti[t].label, dimensioneLabel, {
				//offsetX: this.definizione.immagini[i].elementi[t].placeholderOffsetX+0.10,
				//offsetX: offsetGeneraleInizialeX,
				//offsetY: offsetGeneraleInizialeY-(0.06*1),
				scale: 1,
				enabled: false,
				opacity : 1.0,
				zOrder: 15,
				//horizontalAnchor : this.AR.CONST.HORIZONTAL_ANCHOR.LEFT,
				style : {
					textColor : labelColor,
					backgroundColor : labelSfondoColor,
					fontStyle:myFontstyle
				}

			});
			label.onClick = this.world.mostraContenuti(i,t);
			label.customType=-1;
			label.immagineRef=i;
			label.puntoRef=t;
			label.targetRef=this.definizione.target[i].targetName;

			var labelPosition=this.definizione.target[i].punti[t].labelPosition;
			//console.log("Label: " + this.definizione.target[i].punti[t].label + " - posizione: " + labelPosition);
			if (labelPosition=="up")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.CENTER;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.BOTTOM;
				label.offsetX=offsetGeneraleInizialeX;
				label.offsetY=offsetGeneraleInizialeY+(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="down")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.CENTER;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.TOP;
				label.offsetX=offsetGeneraleInizialeX;
				label.offsetY=offsetGeneraleInizialeY-(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="sx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.MIDDLE;
				label.offsetX=offsetGeneraleInizialeX-(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY;
			}
			else if (labelPosition=="dx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.LEFT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.MIDDLE;
				label.offsetX=offsetGeneraleInizialeX+(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY;
			}
			else if (labelPosition=="upDx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.LEFT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.BOTTOM;
				label.offsetX=offsetGeneraleInizialeX+(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY+(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="downDx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.LEFT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.TOP;
				label.offsetX=offsetGeneraleInizialeX+(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY-(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="downSx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.TOP;
				label.offsetX=offsetGeneraleInizialeX-(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY-(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="upSx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.BOTTOM;
				label.offsetX=offsetGeneraleInizialeX-(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY+(dimensioneIndicatore*0.6);
			}
			else //di default a destra
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.LEFT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.MIDDLE;
				label.offsetX=offsetGeneraleInizialeX+(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY;
			}


			this.arrayOverlay.push(label);
			
		}

		//oltre gli hs standard che il redattore ha messo, devo mettere quelli "fissi" (like, commento, clap)
		//like
		//cerco il numero di like per questa immagine
		//console.log("Considero i Likes");
		//console.log(this.definizione.likes);
		var indexImmagineIndicatore = 0;

        var totalLike=0;
        var totalClap=0;
        for (var likeIndex=0;likeIndex<this.definizione.likes.length;likeIndex++)
        {
          //console.log(this.definizione.likes[likeIndex]);
          if (this.definizione.likes[likeIndex].idImmagine==this.definizione.target[i].targetName)
          {
            totalClap=totalClap+parseInt(this.definizione.likes[likeIndex].clap);
            if (this.definizione.likes[likeIndex].like!=null)
            {
              //item.likes[like.like]++;
              totalLike++;
              if (this.definizione.likes[likeIndex].deviceId==deviceId) indexImmagineIndicatore=parseInt(this.definizione.likes[likeIndex].like);
            }
          }

		};

        var totalComments=0;
        for (var commentIndex=0;commentIndex<this.definizione.comments.length;commentIndex++)
        {
          if (this.definizione.comments[commentIndex].idImmagine==this.definizione.target[i].targetName)
          {
            totalComments=parseInt(this.definizione.comments[commentIndex].totale);
          }

		};

		//console.log("Considero l'immagine " + this.definizione.target[i].targetName);
		//var imgTemp = new this.AR.ImageResource(immagineIndicatore);
		var aspectRatio = Number(this.definizione.target[i].aspectRatio);
		//console.log("aspectRatio " + aspectRatio);
		
		var dimensioneIndicatore=0.15*aspectRatio; //20% della larghezza dell'immagine
		//console.log("dimensioneIndicatore " + dimensioneIndicatore);

		overlay = new this.AR.ImageDrawable(this.arrayImageResourceLike[indexImmagineIndicatore], dimensioneIndicatore,
		{
			offsetX: -0.5*aspectRatio,
			offsetY: -0.5 - (dimensioneIndicatore/2),
			scale: 1,
			enabled: false,
			opacity : 1.0,
			zOrder: 10
		});
		overlay.onClick = this.world.showHideLikeIcons(this.definizione.target[i].targetName);
		overlay.customType=0;
		overlay.momentsType=0; //icona
		overlay.targetRef=this.definizione.target[i].targetName;
		this.arrayOverlay.push(overlay);						

		var dimensioneLabel=dimensioneIndicatore*0.5;
		var labelColor="#000000";
		var labelSfondoColor="#ffffff";
		var labelSfondoTrasp=80;

		//creo il colore con l'opacità per lo sfondo della label
		//trasformo il valore 0-100 in 0-255
		var trasp0255=parseInt(255*labelSfondoTrasp/100);
		var hexString = trasp0255.toString(16);
		var labelSfondoColor=labelSfondoColor+hexString;
		var myFontstyle=AR.CONST.FONT_STYLE.NORMAL;
		
		var totalLikeStr=" " + totalLike + " " ;
		var label = new this.AR.Label(totalLikeStr, dimensioneLabel, {
			horizontalAnchor: this.AR.CONST.HORIZONTAL_ANCHOR.LEFT,
			verticalAnchor: this.AR.CONST.VERTICAL_ANCHOR.MIDDLE,
			offsetX: overlay.offsetX+(dimensioneIndicatore*0.6),
			offsetY: overlay.offsetY,
			scale: 1,
			enabled: false,
			opacity : 1.0,
			zOrder: 15,
			//horizontalAnchor : this.AR.CONST.HORIZONTAL_ANCHOR.LEFT,
			style : {
				textColor : labelColor,
				backgroundColor : labelSfondoColor,
				fontStyle:myFontstyle
			}

		});
		//label.onClick = this.world.mostraContenuti(i,t);
		label.customType=0;
		label.momentsType=1; //valore likes
		label.targetRef=this.definizione.target[i].targetName;
		this.arrayOverlay.push(label);



		//clap
		//var imgTemp = new this.AR.ImageResource(this.imgClap);
		var aspectRatio = Number(this.definizione.target[i].aspectRatio);
		//console.log("aspectRatio " + aspectRatio);
		
		var dimensioneIndicatore=0.15*aspectRatio; //20% della larghezza dell'immagine
		//console.log("dimensioneIndicatore " + dimensioneIndicatore);

		overlay = new this.AR.ImageDrawable(imgClap, dimensioneIndicatore,
		{
			offsetX: 0.5*aspectRatio,
			offsetY: -0.5 - (dimensioneIndicatore/2),
			scale: 1,
			enabled: false,
			opacity : 1.0,
			zOrder: 10
		});
		overlay.onClick = this.world.clap(this.definizione.target[i].targetName);
		overlay.customType=0;
		overlay.targetRef=this.definizione.target[i].targetName;
		this.arrayOverlay.push(overlay);						

		var dimensioneLabel=dimensioneIndicatore*0.5;
		var labelColor="#000000";
		var labelSfondoColor="#ffffff";
		var labelSfondoTrasp=80;

		//creo il colore con l'opacità per lo sfondo della label
		//trasformo il valore 0-100 in 0-255
		var trasp0255=parseInt(255*labelSfondoTrasp/100);
		var hexString = trasp0255.toString(16);
		var labelSfondoColor=labelSfondoColor+hexString;
		var myFontstyle=AR.CONST.FONT_STYLE.NORMAL;
		
		var totalClapStr=" " + totalClap + " " ;
		var label = new this.AR.Label(totalClapStr, dimensioneLabel, {
			horizontalAnchor: this.AR.CONST.HORIZONTAL_ANCHOR.LEFT,
			verticalAnchor: this.AR.CONST.VERTICAL_ANCHOR.MIDDLE,
			offsetX: overlay.offsetX+(dimensioneIndicatore*0.6),
			offsetY: overlay.offsetY,
			scale: 1,
			enabled: false,
			opacity : 1.0,
			zOrder: 15,
			//horizontalAnchor : this.AR.CONST.HORIZONTAL_ANCHOR.LEFT,
			style : {
				textColor : labelColor,
				backgroundColor : labelSfondoColor,
				fontStyle:myFontstyle
			}

		});
		//label.onClick = this.world.mostraContenuti(i,t);
		label.customType=0;
		label.momentsType=2; //valore clap
		label.targetRef=this.definizione.target[i].targetName;
		this.arrayOverlay.push(label);


		//commenti
		//var imgTemp = new this.AR.ImageResource(this.imgComment);
		var aspectRatio = Number(this.definizione.target[i].aspectRatio);
		//console.log("aspectRatio " + aspectRatio);
		
		var dimensioneIndicatore=0.15*aspectRatio; //20% della larghezza dell'immagine
		//console.log("dimensioneIndicatore " + dimensioneIndicatore);

		overlay = new this.AR.ImageDrawable(imgComment, dimensioneIndicatore,
		{
			offsetX: 0,
			offsetY: -0.5 - (dimensioneIndicatore/2),
			scale: 1,
			enabled: false,
			opacity : 1.0,
			zOrder: 10
		});
		overlay.onClick = this.world.comment(this.definizione.target[i].targetName);
		overlay.customType=0;
		overlay.targetRef=this.definizione.target[i].targetName;
		this.arrayOverlay.push(overlay);						

		var dimensioneLabel=dimensioneIndicatore*0.5;
		var labelColor="#000000";
		var labelSfondoColor="#ffffff";
		var labelSfondoTrasp=80;

		//creo il colore con l'opacità per lo sfondo della label
		//trasformo il valore 0-100 in 0-255
		var trasp0255=parseInt(255*labelSfondoTrasp/100);
		var hexString = trasp0255.toString(16);
		var labelSfondoColor=labelSfondoColor+hexString;
		var myFontstyle=AR.CONST.FONT_STYLE.NORMAL;
		
		var totalCommentsStr=" " + totalComments + " " ;
		var label = new this.AR.Label(totalCommentsStr, dimensioneLabel, {
			horizontalAnchor: this.AR.CONST.HORIZONTAL_ANCHOR.LEFT,
			verticalAnchor: this.AR.CONST.VERTICAL_ANCHOR.MIDDLE,
			offsetX: overlay.offsetX+(dimensioneIndicatore*0.6),
			offsetY: overlay.offsetY,
			scale: 1,
			enabled: false,
			opacity : 1.0,
			zOrder: 15,
			//horizontalAnchor : this.AR.CONST.HORIZONTAL_ANCHOR.LEFT,
			style : {
				textColor : labelColor,
				backgroundColor : labelSfondoColor,
				fontStyle:myFontstyle
			}

		});
		//label.onClick = this.world.mostraContenuti(i,t);
		label.customType=0;
		label.momentsType=3; //valore commenti
		label.targetRef=this.definizione.target[i].targetName;
		this.arrayOverlay.push(label);

	}
	

  
}


Hotspot.prototype.getArrayOverlay = function ()
{
	return this.arrayOverlay;
}

Hotspot.prototype.getArrayImages2Load = function ()
{
	return this.arrayImages2Load;
}

Hotspot.prototype.getArrayImageResourceLike = function ()
{
	return this.arrayImageResourceLike;
}


