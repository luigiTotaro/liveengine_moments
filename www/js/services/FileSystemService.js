angular.module('LiveEngine.FileSystem.Services', [])

.service('FileSystemService', function($rootScope, $http, $ionicModal, $ionicPopup, LoggerService,  $q, HelperService) {

      var me = this;

      me.__chunkSize = 5 * 1024 * 1024;
      me.fileSystem = null;


      // ######################################## //
      // inizializzo il filesystem una volta sola //
      // ######################################## //
      me.InitializeFilesystem = function(parameters) {

          try {

            if(!window.cordova) {
                throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
            }

            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {

                me.fileSystem = fs.root;
                parameters.onComplete(null);

              }, function(err) {

                me.fileSystem = null;
                parameters.onComplete(err);

            });

          } catch(err) {

            me.fileSystem = null;
            parameters.onComplete(err);

          }

      };


      // parameters.directory
      // parameters.fileName
      // parameters.onComplete = function(err, isAvailable)
      me.fileExists = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

              //console.log("FileExistClass: 1 - directory: " + parameters.directory + " - fileName: " + parameters.fileName);
              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (dinamicClassesDirEntry) {


                  dinamicClassesDirEntry.getFile(

                      parameters.fileName,

                      { create: false },

                      function(fileEntry) {
                          //console.log("FileExistClass: 2");
                          parameters.onComplete(null, true);
                      },

                      function() {
                          //console.log("FileExistClass: 3");
                          parameters.onComplete(null, false);
                      }

                  );

              }, function(err) {

                  //console.log("FileExistClass: 4");
                  parameters.onComplete(null, false);

              });

          } catch(err) {
              //console.log("FileExistClass: 5");
              parameters.onComplete(err, false);
          }

      };


      
      // parameters.directory
      // parameter.dirBuffer2delete
      // parameter.dirDinamic2delete
      // parameter.dirGameCenter2delete
      // parameters.onComplete = function(err, operationCompleted)
      me.createRootDir = function(parameters) {

        //crea la directory root, e cancella le vecchie directory singole eventualmenti presenti sul device

          console.log("entro in createRootDir in FileSystemService");
          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

                me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

                  //cancello la dir buffer
                  me.fileSystem.getDirectory(parameters.dirBuffer2delete, {create : true, exclusive : false}, function (dirBuff2deleteEntry) {
                      dirBuff2deleteEntry.removeRecursively(function() {
                          console.log("Remove Recursively Succeeded");
                          
                          //cancello la dir dinamic
                          me.fileSystem.getDirectory(parameters.dirDinamic2delete, {create : true, exclusive : false}, function (dirDyn2deleteEntry) {
                              dirDyn2deleteEntry.removeRecursively(function() {
                                  console.log("Remove Recursively Succeeded");
                                  
                                  //cancello la dir game center
                                  me.fileSystem.getDirectory(parameters.dirGameCenter2delete, {create : true, exclusive : false}, function (dirGame2deleteEntry) {
                                      dirGame2deleteEntry.removeRecursively(function() {
                                          console.log("Remove Recursively Succeeded");
                                          

                                          parameters.onComplete(null, false);
                                      }, function(err) {
                                          //anche se non sono riuscito  cancellare vado comunque avanti
                                          //console.log("oncomplete 1");
                                          parameters.onComplete(null, false);
                                      });
                                  }, function(err) {
                                    //console.log("oncomplete 2");
                                    parameters.onComplete(null, false);
                                  });



                              }, function(err) {
                                  //anche se non sono riuscito  cancellare vado comunque avanti
                                  //console.log("oncomplete 3");
                                  parameters.onComplete(null, false);
                              });
                          }, function(err) {
                            //console.log("oncomplete 4");
                            parameters.onComplete(null, false);
                          });

                          
                      }, function(err) {
                          //anche se non sono riuscito  cancellare vado comunque avanti
                          //console.log("oncomplete 5");
                          parameters.onComplete(null, false);
                      });
                  }, function(err) {
                    //console.log("oncomplete 6");
                    parameters.onComplete(null, false);
                  });

    

                }, function(err) {
                  //console.log("createRootDir - error: " + JSON.stringify(err));
                  parameters.onComplete(err, false);
              });

          } catch(err) {
              //console.log("createRootDir - catch: " + JSON.stringify(err));
              parameters.onComplete(err, false);
          }

      };





      // parameters.directory
      // parameters.fileName
      // parameters.bufferData
      // parameters.onComplete = function(err, operationCompleted)
      me.writeFile = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

              //console.log("richiedo la directory: " + parameters.directory);
              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

                  //console.log("richiedo il file: " + parameters.fileName);

                  directoryEntry.getFile(

                      parameters.fileName,

                      { create: true, exclusive: false },

                      function(fileEntry) {

                          fileEntry.createWriter(function(fileWriter) {

                              fileWriter.onwriteend = function(e) {
                                  parameters.onComplete(null, true);
                              };

                              fileWriter.onerror = function(e) {
                                  parameters.onComplete("file write error: " + e.toString(), false);
                              };

                              try {

                                  var blob = new Blob([parameters.bufferData], {type: 'text/plain'});
                                  fileWriter.write(blob);

                              } catch(blob_err) {

                                  try {

                                      var webkitBlob = new WebKitBlobBuilder();                                 
                                      webkitBlob.append(parameters.bufferData);
                                      fileWriter.write(webkitBlob.getBlob('text/plain'));

                                  } catch(webkit_blob_err) {

                                      parameters.onComplete("blob creation error", false);

                                  }
                                  
                              }

                          }, function(err) {
                              parameters.onComplete("file write error: " + JSON.stringify(err), false);

                          });

                      },

                      function() {
                          console.log("getFile error");
                          parameters.onComplete(null, false);
                      }

                  );

              }, function(err) {
                  console.log("getDirectory error: " + JSON.stringify(err));
                  parameters.onComplete(err, false);
              });

          } catch(err) {
              console.log("catch err: " + err);
              parameters.onComplete(err, false);
          }

      };


      // parameters.directory
      // parameters.fileName
      // parameters.onComplete = function(err, bufferData)
      me.fileRead = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

                  directoryEntry.getFile(

                      parameters.fileName,

                      { create: false, exclusive: false },

                      function(fileEntry) {


                          fileEntry.file(function(readedFile) {

                              var reader = new FileReader();

                              reader.onloadend = function(e) {
                                  parameters.onComplete(null, this.result);
                              };

                              reader.readAsText(readedFile);

                          }, function(e) {
                              parameters.onComplete("file read error: " + JSON.stringify(err), null);
                          });

                      }, function() {
                          parameters.onComplete('getFileError', null);
                      }

                  );

              }, function(err) {
                  parameters.onComplete(err, null);
              });

          } catch(err) {
              parameters.onComplete(err, null);
          }

      };


      // parameters.directory
      // parameters.fileName
      // parameters.onComplete = function(err)
      me.fileDelete = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

                  directoryEntry.getFile(

                      parameters.fileName,

                      { create: false, exclusive: false },

                      function(fileEntry) {

                          fileEntry.remove(function() {
                            parameters.onComplete(null);
                          }, function(err) {
                            parameters.onComplete(err);
                          });

                      }, function() {
                          parameters.onComplete('getFileError');
                      }

                  );

              }, function(err) {
                  parameters.onComplete(err);
              });

          } catch(err) {
              parameters.onComplete(err);
          }

      };



      me.getFileContentAsBase64 = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

                  directoryEntry.getFile(

                      parameters.fileName,

                      { create: false, exclusive: false },

                      function(fileEntry) {

                          fileEntry.file(function(readedFile) {

                              var reader = new FileReader();

                              reader.onloadend = function(e) {
                                  parameters.onComplete(null, this.result);
                              };

                              reader.readAsDataURL(readedFile);

                          }, function(e) {
                              parameters.onComplete("file read error: " + JSON.stringify(err), null);
                          });

                      }, function() {
                          parameters.onComplete('getFileError', null);
                      }

                  );

              }, function(err) {
                  parameters.onComplete(err, null);
              });

          } catch(err) {
              parameters.onComplete(err, null);
          }



      };

      // parameters.url
      // parameters.directory
      // parameters.fileName
      // parameters.onComplete -> function(err, localFilePath)
      me.downloadFile = function(parameters) {

          me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

              // Parameters passed to getFile create a new file or return the file if it already exists.
              directoryEntry.getFile(parameters.fileName, { create: true, exclusive: false }, function (fileEntry) {

                  var fileTransfer = new FileTransfer();
                  var fileURL = fileEntry.toURL();

                  fileTransfer.download(
                      parameters.url,
                      fileURL,

                      function (entry) {
                          parameters.onComplete(null, fileURL);
                      },

                      function (error) {
                          parameters.onComplete("[downloadFile] file download error: " + error.code, null);
                      },
                      true //trustAllHosts: Optional parameter, defaults to false. If set to true, it accepts all security certificates.

                  );

              }, function(err) {

                  parameters.onComplete("[downloadFile] file create error: " + JSON.stringify(err), null);

              });

          }, function(err) {

            parameters.onComplete("[downloadFile] directory creation error" + JSON.stringify(err), null);

          });


      };


      me.getLocalPath = function()
      {
        return me.fileSystem.toURL();
      }

      // parameters.url
      // parameters.directory
      // parameters.fileName
      // parameters.onComplete -> function(err, localFilePath)
      me.uploadFile = function(parameters) {

          console.log("Sto per uploadare " + parameters.fileName);
 
          var fileTransfer = new FileTransfer();
          var url = parameters.baseUrl + "/momentsFileUtils.php";

          console.log("url: " + url);

          var options = new FileUploadOptions();
          options.fileKey = "file";
          options.fileName = parameters.fileName.substr(parameters.fileName.lastIndexOf('/')+1);
          options.mimeType = "image/jpeg";
          
          var params = new Object();
          params.devID = parameters.devID;
          params.nick = parameters.nick;
          params.prjID = parameters.prjID;

          options.params = params;
          options.chunkedMode = false;


          console.log(JSON.stringify(options));

          var success = function (r) {
              console.log("Successful upload...");
              console.log("Code = " + r.responseCode);
              // displayFileData(fileEntry.fullPath + " (content uploaded to server)");
              parameters.onComplete(null);
          }

          var fail = function (error) {
              console.log("Error on upload...");
              console.log("Code = " + error.code);
              //alert("An error has occurred: Code = " + error.code);
              parameters.onComplete(error.code);
          }

          fileTransfer.onprogress = function(progressEvent) {
              /*
              if (progressEvent.lengthComputable) {
                  console.log(progressEvent.loaded, progressEvent.total);
              } else {
                  console.log("increment");
              }
          */
          };

          fileTransfer.upload(
              parameters.fileName,
              encodeURI(url),success,fail,options

          );




      };


});
